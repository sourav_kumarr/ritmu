<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 7/20/2017
 * Time: 6:50 PM
 */




class FOLDER
{
    public $link = null;
    public $response = array();
    function __construct(){
        $this->link = new \Classes\CONNECT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }

    function createFolder($table_name,$data) {

      $link = $this->link->connect();
      $response = array();
//      echo $path;
        $path = $data['folder_path'];
//        echo $data['folder_path'];
        if (!file_exists($data['folder_path'])) {
            $isCreated  = mkdir($path, 0777, true);
            if($isCreated) {
               $responsee = $this->isFolderExists($data['name']);
               if($responsee['count'] == 0 ) {
//                 $data['folder_path'] = $path;
                 $query = $this->link->insertQuery($table_name,$data);
                 $result = mysqli_query($link,$query);
                 if($result) {
                    $id_ = mysqli_insert_id($link);
                    if($id_>0) {
                        $response[STATUS] = Success;
                        $response[MESSAGE] = "Folder created successfully";
                    }
                 }
                 else{
                     $response[STATUS] = Error;
                     $response[MESSAGE] = $this->link->sqlError();
                 }
               }
               else{
                   $response[STATUS] = Error;
                   $response[MESSAGE] = "Folder name already exists";
               }
            }
            else{
                $response[STATUS] = Error;
                $response[MESSAGE] = "Unable to create folder";
            }
        }
        else{
            $response[STATUS] = Error;
            $response[MESSAGE] = "Folder already exists";

        }
        return $response;
    }

    function updateFolder($tableName,$data,$columnName) {

        $link = $this->link->connect();
        $response = array();
        $old_path = $data['folder_path'];
        if (file_exists($data['folder_path'])) {
          $name = $data['folder_name'];
          $responsee = $this->isFolderExists($data['name']);
            if($responsee['count'] == 0 ) {
                $base_path = explode("/",$old_path);
                array_pop($base_path);
                $base_path = implode("/",$base_path);
                $new_path = $base_path.'/'.$name;
//                $isRename = rename($old_path,$new_path);
//                if($isRename) {
                    if($link) {
                        $data['folder_path'] = $new_path;
                        unset($data['folder_path']);
                        $sql = $this->link->updateQuery($tableName,$data,$columnName,$data['folder_id']);
                        $result = mysqli_query($link,$sql);
                        if($result) {
                            $num = mysqli_affected_rows($link);
                            $id = $data['folder_id'];
//                            substr(strrchr($, "/"), 1);
//                            $ress = mysqli_query($link,"update folders set folder_path='$new_path' where folder_parent_id='$id'");

                            if($num>0) {
                                $response[STATUS] = Success;
                                $response[MESSAGE] = "Folder updated successfully";
                            }
                            else{
                                $response[STATUS] = Error;
                                $response[MESSAGE] = "No data set has been changed";
                            }
                        }
                        else{
                            $response[STATUS] = Error;
                            $response[MESSAGE] = $this->link->sqlError();
                        }
                    }
                    else{
                        $response[STATUS] = Error;
                        $response[MESSAGE] = $this->link->sqlError();
                    }
                /*}
                else{
                    $response[STATUS] = Error;
                    $response[MESSAGE] = "Unable to rename directory";
                }*/
            }
            else{

                    $response[STATUS] = Error;
                    $response[MESSAGE] = "Folder name already exists";
            }
        }
        else{
            $response[STATUS] = Error;
            $response[MESSAGE] = "Folder does not exists";
        }
        return $response;
    }

    function deleteFolder($id,$path) {
        $link = $this->link->connect();
        $response = array();
        if($link) {
          $query = "select * from videos where video_parent_id='$id'";
          $res = mysqli_query($link,$query);
          $num = mysqli_num_rows($res);
          if($num == 0) {
              $sql = "delete from folders where folder_id='$id'";
              $result = mysqli_query($link, $sql);
              $this->destroyDir($path);
              if ($result) {
                  $response[STATUS] = Success;
                  $response[MESSAGE] = "Folder deleted successfully";
              } else {
                  $response[STATUS] = Error;
                  $response[MESSAGE] = $this->link->sqlError();
              }
          }
          else{
              $response[STATUS] = Error;
              $response[MESSAGE] = "In this folder video exists..";
          }
        }
        else {
            $response[STATUS] = Error;
            $response[MESSAGE] = $this->link->sqlError();
        }
        return $response;
    }

    function getFolderData($parent_id) {
        $link = $this->link->connect();
        $response = array();
        $folders = array();
        $videos = array();
        $videoClass = new \Classes\CONNECT();
        if($link) {
            $sql = "select * from folders where folder_parent_id='$parent_id'";
            $result = mysqli_query($link,$sql);
            if($result) {
              while($rows=mysqli_fetch_array($result)) {
                  $folders[] = array("folder_id"=>$rows['folder_id'],
                      "folder_name"=>$rows['folder_name'],
                      "folder_created_at"=>$rows["folder_created_at"],
                      "folder_path"=>$rows['folder_path']);
              }
              $sql = "select * from videos where video_parent_id='$parent_id'";

              $res = mysqli_query($link,$sql);
//                echo $res;
                if($res) {
                    while ($videoArray = mysqli_fetch_array($res)) {
                        $videos[] =array(
                            "video_id"=>$videoArray['video_id'],
                            "video_name"=>$videoArray['video_name'],
                            "video_file"=>$videoArray['video_file'],
                            "added_on"=>$videoArray['added_on'],
                            "video_status"=>$videoArray['video_status'],
                            "video_duration"=>$videoArray['video_duration'],
                            "video_path"=>$videoArray['video_path'],
                            "VideoBaseURL"=>VideoBaseURL
                        );
                    }
                    if(count($videos)==0 && count($folders)==0) {
                        $response[STATUS] = Error;
                        $response[MESSAGE] = "Data not found";
                    }
                    else{
                        $response[STATUS] = Success;
                        $response[MESSAGE] = "Data found successfully";
                        $response["videos"] = $videos;
                        $response["folders"] = $folders;

                    }
                }
                else{
                    $response[STATUS] = Error;
                    $response[MESSAGE] = $this->link->sqlError();
                }
            }
            else{
                $response[STATUS] = Error;
                $response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $response[STATUS] = Error;
            $response[MESSAGE] = $this->link->sqlError();
        }
        return $response;
    }

    function isFolderExists($name){
        $link = $this->link->connect();
        $response = array();
        $num = 0;
        if($link) {
            $sql = "select * from folders where folder_name='$name'";
            $result = mysqli_query($link,$sql);
            $num = mysqli_num_rows($result);
            $response[STATUS] = Success;
            $response[MESSAGE] = "Folder name already exists";
            $response["count"] = $num;

        }
        else{
            $response[STATUS] = Error;
            $response[MESSAGE] = $this->link->sqlError();
            $response["count"] = $num;

        }
        return $response;
    }

    function destroyDir($dir, $virtual = false)
    {
        $ds = DIRECTORY_SEPARATOR;
        $dir = $virtual ? realpath($dir) : $dir;
        $dir = substr($dir, -1) == $ds ? substr($dir, 0, -1) : $dir;
        if (is_dir($dir) && $handle = opendir($dir))
        {
            while ($file = readdir($handle))
            {
                if ($file == '.' || $file == '..')
                {
                    continue;
                }
                elseif (is_dir($dir.$ds.$file))
                {
                    $this->destroyDir($dir.$ds.$file);
                }
                else
                {
                    unlink($dir.$ds.$file);
                }
            }
            closedir($handle);
            rmdir($dir);
            return true;
        }
        else
        {
            return false;
        }
    }

}