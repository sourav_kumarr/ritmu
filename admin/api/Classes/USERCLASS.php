<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 1/5/17
 * Time: 1:27 PM
 */
namespace Classes;
require_once('CONNECT.php');
class USERCLASS
{
    public $link = null;
    public $response = array();
    public $currentDate=null;
    public $currentDateStamp=null;
    public $currentDateTime=null;
    public $currentDateTimeStamp=null;
    function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDate = date('d M Y');
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateStamp = strtotime($this->currentDate);
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function registerUser($username,$email,$contactNumber,$address,$registersource,$file_name)
    {
        $link = $this->link->connect();
        if($link) {
            $emailResponse = $this->checkEmailExistence($email);
            if($emailResponse[STATUS] == Error){
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $emailResponse[MESSAGE];
                $this->response['UserData'] = $emailResponse['UserData'];
            }else{
                $token = $this->generateToken();
                $query = "insert into users (user_name,user_email,user_contact,user_status,user_token,user_address,
                register_source,email_verified,user_profile, subscribed) VALUES ('$username','$email','$contactNumber','1','$token',
                '$address','$registersource','No','$file_name', 'No')";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $last_id = mysqli_insert_id($link);
                    $userResponse = $this->getParticularUserData($last_id);
                    $UserData = $userResponse['UserData'];
                    $this->response[STATUS] = Success;
                    $this->sendWelcomeMail($last_id);
                    $this->response[MESSAGE] = "User Registered Successfully";
                    $this->response['UserData'] = $UserData;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            }
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function generateToken()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $charactersLength; $i++) {
            $randomString .= $characters[rand(0,15)];
        }
        return $randomString;
    }
    public function checkEmailExistence($email)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from users where user_email = '$email'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "E-Mail Address Already Registered";
                    $row = mysqli_fetch_assoc($result);
                    $this->response['UserData'] = $row;
                }
                else{
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Now Register";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularUserData($userId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from users where user_id='$userId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $userData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid User";
                    $this->response['UserData'] = $userData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid User";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function updateUser($userId,$username,$email,$contactNumber,$token,$address)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "update users set user_name='$username',user_email='$email',
            user_contact='$contactNumber',user_token = '$token',user_address = '$address' where user_id='$userId' ";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Success";
                $this->response['userId'] = $userId;
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function updatePlan($userId,$plan_id, $renewal_type, $auto_renewal)
    {
        $link = $this->link->connect();
        if($link) {
            $expiry_date = "";
            if ($renewal_type == "Monthly") {
                $days = $this->link->currentMonthDays();
                $expiry_date = ($this->currentDateTimeStamp + (86400 * $days)) - 86400;
            } elseif ($renewal_type == "Yearly") {
                $days = $this->link->YearDays();
                $expiry_date = ($this->currentDateTimeStamp + (86400 * $days)) - 86400;
            }
            $query = "update users set active_plan='$plan_id',activation_date='$this->currentDateTimeStamp',
            plan_expiry_date='$expiry_date',renewal_type = '$renewal_type',
            auto_renewal = '$auto_renewal' where user_id='$userId' ";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Success";
                $this->response['userId'] = $userId;
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    ///////////////////////////////////////////////////////
    public function getUserDataFromEmail($email)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from users where user_email='$email'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $userData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid User";
                    $this->response["UserImageBaseURL"] = UserImageBaseURL;
                    $this->response['UserData'] = $userData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid User";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function statusChange($user_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE users SET user_status='$value' WHERE user_id='$user_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function renewalStatusChange($user_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE users SET auto_renewal='$value' WHERE user_id='$user_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteUser($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from users WHERE user_id='$user_id'");
                    if ($update) {
                        $row = mysqli_fetch_array($result);
                        if($row['user_profile'] != "") {
                            unlink("Files/images/" . $row['user_profile']);
                        }
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Users Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getAllUsers(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $usersData[]=array(
                          "user_id"=>$rows["user_id"],
                          "user_name"=>$rows["user_name"],
                          "user_email"=>$rows["user_email"],
                          "user_contact"=>$rows["user_contact"],
                          "active_plan"=>$rows["active_plan"],
                          "activation_date"=>$rows["activation_date"],
                          "plan_expiry_date"=>$rows["plan_expiry_date"],
                          "user_status"=>$rows["user_status"],
                          "user_address"=>$rows["user_address"],
                          "renewal_type"=>$rows["renewal_type"],
                          "auto_renewal"=>$rows["auto_renewal"],
                          "email_verified"=>$rows["email_verified"],
                          "subscribed"=>$rows["subscribed"]
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['usersData'] = $usersData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }

    public function updateSubscribe($userId, $subscribed, $subscribed_for)
    {
        $link = $this->link->connect();
        if($link) {
            if($subscribed == "Yes"){
                $valid_upto ="";
                if ($subscribed_for == "7") {
                    $valid_upto = ($this->currentDateTimeStamp + (86400 * 7)) - 86400;
                } elseif ($subscribed_for == "30") {
                    $valid_upto = ($this->currentDateTimeStamp + (86400 * 30)) - 86400;
                }
                $query = "update users set subscribed='Yes', subscribed_for='$valid_upto' where user_id='$userId' ";
                $result = mysqli_query($link,$query);
                if($result)
                {
                    $this->response[STATUS] = "Success";
                    $this->response[MESSAGE] = "Successfully Subscribed";

                }
                else
                {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            }
            else{
                $query = "update users set subscribed='No', subscribed_for='' where user_id='$userId' ";
                $result = mysqli_query($link,$query);
                if($result)
                {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Successfully Unsubscribed";
                    $this->response['userId'] = $userId;
                }
                else
                {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function sendWelcomeMail($userId)
    {
        $userResponse = $this->getParticularUserData($userId);
        if($userResponse[STATUS] == Success){
            $userData = $userResponse['UserData'];
            $user_email = $userData['user_email'];
            $user_name = $userData['user_name'];
            $contact_mail ="Contcact@boltikitaab.com";
            $subject ="Thanks For your Registration";
            $body = "<body><center><table width='600' background='#FFFFFF' style='text-align:left;' cellpadding='0' cellspacing='0'><tr>
	<td height='18' width='31' style='border-bottom:1px solid #e4e4e4;'><div style='line-height:0; font-size: 1px; position: absolute;'>&nbsp;
	</div></td><td height='18' width='131'><div style='line-height:0; font-size: 1px; position: absolute;'>&nbsp;</div></td>
	<td height='18' width='466' style='border-bottom:1px solid #e4e4e4;'><div style='line-height:0; font-size:1px; position: absolute;'>&nbsp;
	</div></td></tr><tr><td height='2' width='31' style='border-bottom:1px solid #e4e4e4;'>
	<div style='line-height:0; font-size:1px; position:absolute;'>&nbsp;</div></td>
	<td height='2' width='131'><div style='line-height:0; font-size:1px; position:absolute;'>&nbsp;</div></td>
	<td height='2' width='466' style='border-bottom:1px solid #e4e4e4;'>
	<div style='line-height:0; font-size:1px; position: absolute;'>&nbsp;</div></td></tr><tr>
	<td background='cid:greenback' width='31' bgcolor='#45a853' style='border-top:1px solid #FFF; border-bottom:1px solid #FFF;' height='113'>
	<div style='line-height:0; font-size:1px; position: absolute;'>&nbsp;</div></td>
	<td width='131' bgcolor='#FFFFFF' style='border-top:1px solid #FFF; text-align:center;' height='113' valign='middle'>
	<span style='font-size:25px; font-family:Trebuchet MS, Verdana, Arial; color:#2e8a3b;'>Success!</span></td>
	<td background='cid:greenback' bgcolor='#45a853' style='border-top:1px solid #FFF; border-bottom:1px solid #FFF; padding-left:15px;' height='113'>
	<span style='color:#FFFFFF; font-size:18px; font-family:Trebuchet MS, Verdana, Arial;'>Thank you for Your Registration to BoltiKitaab.</span>
	</td></tr><tr><td height='3' width='31' style='border-top:1px solid #e4e4e4; border-bottom:1px solid #e4e4e4;'>
	<div style='line-height:0; font-size:1px; position: absolute;'>&nbsp;</div>
	</td><td height='3' width='131'><div style='line-height:0; font-size:1px; position: absolute;'>&nbsp;</div>
	</td><td height='3' style='border-top:1px solid #e4e4e4; border-bottom:1px solid #e4e4e4;'>
	<div style='line-height:0; font-size:1px; position: absolute;'>&nbsp;</div></td></tr><tr><td colspan='3'><br/><br>
	<table cellpadding='0' cellspacing='0'><tr><td width='15'><div style='line-height:0; font-size:1px; position: absolute;'>&nbsp;</div>
	</td><td width='325' style='padding-right:10px; font-family:Trebuchet MS, Verdana, Arial; font-size:12px;' valign='top'>
	<span style='font-family:Trebuchet MS, Verdana, Arial; font-size:17px; font-weight:bold;'>Welcome $user_name!</span>
	<br /><p>You are successfully Registered to BoltiKitaab and You can look forward to receiving After <b>Subscription</b> to our newsletter:</p><br />
	<div style='padding-left:20px; padding-bottom:10px;'><img src='cid:spade' alt=''>&nbsp;&nbsp;&nbsp;
	New items added to categories based on their purchases</div>
	<div style='padding-left:20px; padding-bottom:10px;'><img src='cid:spade' alt=''>&nbsp;&nbsp;&nbsp;
	New items added to categories based on their Recently Viewed Items</div>
	<div style='padding-left:20px; padding-bottom:10px;'><img src='cid:spade' alt=''>&nbsp;&nbsp;&nbsp;
	Discount offers</div><br/><br/><br/>Best Regards,<br/>Admin<br/>Bolti Kitaab<br/><br/>
	This welcome email was sent to you because you recently signed up at BoltiKitaab.</td>
	<td style='border-left:1px solid #e4e4e4; padding-left:15px;' valign='top'>
	<table width='100%' cellpadding='0' cellspacing='0' style='border-bottom:1px solid #e4e4e4; font-family:Trebuchet MS, Verdana, Arial; font-size:12px;'>
	<tr><td><div style='font-family:Trebuchet MS, Verdana, Arial; font-size:17px; font-weight:bold; padding-bottom:10px;'>
	Add Us To Your Address Book</div><img src='cid:addressbook' align='right' style='padding-left:10px; padding-top:10px; padding-bottom:10px;' alt=''>
	<p>To help ensure that you receive all email messages consistently in your inbox with images displayed, please add this address to your address book or contacts list: <strong> $contact_mail </strong>.</p>
	<br></td></tr></table><br />
	<table width='100%' cellpadding='0' cellspacing='0' style='border-bottom:1px solid #e4e4e4; font-family:Trebuchet MS, Verdana, Arial; font-size:12px;'>
	<tr><td><div style='font-family:Trebuchet MS, Verdana, Arial; font-size:17px; font-weight:bold; padding-bottom:10px;'>
	Have Any Questions?</div><img src='cid:penpaper' align='right' style='padding-left:10px; padding-top:10px; padding-bottom:10px;' alt=''>
	<p>Do not hesitate to hit the reply button to any of the messages you receive.</p><br /></td></tr></table><br />
	<table cellpadding='0' width='100%' cellspacing='0' style='font-family:Trebuchet MS, Verdana, Arial; font-size:12px;'><tr><td>
	<div style='font-family:Trebuchet MS, Verdana, Arial; font-size:17px; font-weight:bold; padding-bottom:10px;'>Have A Topic Idea?</div>
	<img src='cid:lightbulb' align='right' style='padding-left:10px; padding-top:10px; padding-bottom:10px;' alt=''>
	<p>I would love to hear it! Just reply any time and let me know what topics you would like to know more about.</p><br /></td></tr></table>
	</td></tr></table></td></tr></table><br />
	<table cellpadding='0' style='border-top:1px solid #e4e4e4; text-align:center; font-family:Trebuchet MS, Verdana, Arial; font-size:12px;' cellspacing='0' width='600'>
	<tr><td height='2' style='border-bottom:1px solid #e4e4e4;'><div style='line-height: 0px; font-size: 1px; position: absolute;'>&nbsp;</div>
	</td></tr><tr><td style='font-family:Trebuchet MS, Verdana, Arial; font-size:12px;'><br/><a href='mailTo:$contact_mail'>$contact_mail</a>
	</td></tr></table></center></body>";

            $username = "no-reply@yagooface.com";
            $password = "yagooface@123";
            $mail_from = "no-reply@yagooface.com";
            $mail_from_name = "BoltiKitaab";
            $mail_to= "soniboy27@gmail.com";
            require 'SMTP/PHPMailerAutoload.php';
            $mail = new \PHPMailer();
            $mail->isSMTP();
            $mail->Host = 'md-in-68.webhostbox.net';
            $mail->SMTPAuth = true;
            $mail->Username = $username;
            $mail->Password = $password;
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;
            $mail->addAddress($mail_to);
            $mail->setFrom($mail_from, $mail_from_name);
            $mail->AddEmbeddedImage(MainServer.'admin/api/Classes/SMTP/images/spade.gif', 'spade');
            $mail->AddEmbeddedImage(MainServer.'admin/api/Classes/SMTP/images/addressbook.gif', 'addressbook', 'addressbook.gif');
            $mail->AddEmbeddedImage(MainServer.'SMTP/images/greenback.gif', 'greenback', 'greenback.gif');
            $mail->AddEmbeddedImage(MainServer.'SMTP/images/lightbulb.gif', 'lightbulb', 'lightbulb.gif');
            $mail->AddEmbeddedImage(MainServer.'SMTP/images/penpaper.gif', 'penpaper', 'penpaper.gif');
            $mail->Subject = $subject;
            $mail->Body    = $body;
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->AltBody = 'Thank you for subscribing to our email newsletter.';

            if(!$mail->send()) {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] ="Message could not be sent.Mailer Error: " . $mail->ErrorInfo;
            } else {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Message has been sent";
            }

        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Error Sending Email";
        }

        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}