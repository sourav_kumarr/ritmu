<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 7/20/2017
 * Time: 6:49 PM
 */
session_start();
require_once('Constants/DbConfig.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Classes/VIDEO.php');
require_once('Classes/FOLDER.php');

$videoClass = new VIDEO();
$folderClass = new FOLDER();

$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $videoClass->apiResponse($response);
    return false;
}
//$timeZone = date_default_timezone_get();
date_default_timezone_set($_SESSION['timezone']);
//error_reporting(0);
$type = $_POST['type'];

if($type == 'createFolder') {

    $data = array();
    $requiredfields = array('name','user_id','parent_id','timestamp');

    ($response = RequiredFields($_POST, $requiredfields));
    if($response['Status'] == 'Failure'){
        $videoClass->apiResponse($response);
        return false;
    }
    $name = trim($_REQUEST['name']);
    $user_id = trim($_REQUEST['user_id']);
    $parent_id = trim($_REQUEST['parent_id']);
    $timestamp = trim($_REQUEST['timestamp']);
    $data['folder_name'] = $name;
    $data['folder_user_id'] = $user_id;
    $data['folder_parent_id'] =  $parent_id;
    $data['folder_created_at'] =  $timestamp;

    if(!isset($_REQUEST['folder_path'])) {
        $data['folder_path'] = VideoBaseURL1.$name;
    }
    else{
        $data['folder_path'] = $_REQUEST['folder_path'].$name;
    }
    $response = $folderClass->createFolder('folders',$data);
    $videoClass->apiResponse($response);

}

else if($type == 'updateFolder') {
    $data = array();
    $requiredfields = array('folder_name','user_id','parent_id','folder_path','folder_id');

    ($response = RequiredFields($_POST, $requiredfields));
    if($response['Status'] == 'Failure') {
        $videoClass->apiResponse($response);
        return false;
    }

    $folder_name = trim($_REQUEST['folder_name']);
    $user_id = trim($_REQUEST['user_id']);
    $parent_id = trim($_REQUEST['parent_id']);
    $folder_path = trim($_REQUEST['folder_path']);
    $folder_id = trim($_REQUEST['folder_id']);

    $data['folder_name'] = $folder_name;
    $data['folder_user_id'] = $user_id;
    $data['folder_parent_id'] = $parent_id;
    $data['folder_path'] = $folder_path;
    $data['folder_id'] = $folder_id;

    $response = $folderClass->updateFolder('folders',$data,'folder_id');
    $videoClass->apiResponse($response);

}

else if($type == 'deleteFolder') {
    $requiredfields = array('folder_id','folder_path');

    ($response = RequiredFields($_POST, $requiredfields));
    if($response['Status'] == 'Failure') {
        $videoClass->apiResponse($response);
        return false;
    }
    $folder_id = trim($_REQUEST['folder_id']);
    $folder_path = trim($_REQUEST['folder_path']);
    $response = $folderClass->deleteFolder($folder_id,$folder_path);
    $videoClass->apiResponse($response);

}

else if($type == 'getFolderData') {
    $requiredfields = array('parent_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure') {
        $videoClass->apiResponse($response);
        return false;
    }
    $parent_id = trim($_REQUEST['parent_id']);
    $response = $folderClass->getFolderData($parent_id);
    $videoClass->apiResponse($response);
}