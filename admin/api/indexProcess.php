<?php
require_once('Classes/DISCOUNT.php');
require_once('Classes/BOOKS.php');
require_once('Classes/USERCLASS.php');
require_once('Classes/CATEGORY.php');
require_once('Classes/MEMBERSHIP.php');
require_once('Classes/ORDERS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$couponClass = new \Classes\DISCOUNT();
$booksClass = new \Classes\BOOKS();
$usersClass = new \Classes\USERCLASS();
$catClass = new \Classes\CATEGORY();
$memClass = new \Classes\MEMBERSHIP();
$orderClass = new \Classes\ORDERS();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $couponClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "getFrontPageData")
{
    $coupons = $couponClass->getAllCoupons();
    $coupons = $coupons['data'];
    $totalCoupons = sizeof($coupons);
    $activeCoupons=0;
    for($i=0;$i<sizeof($coupons);$i++){
        if($coupons[$i]['coupon_status'] == "1"){
            $activeCoupons++;
        }
    }
    //////////////////////////////////
    $books_data = $booksClass->totalBooks();
    $Bookscount=$books_data['Count'];
    ////////////////////////////////////////
    $userData = $usersClass->getAllUsers();
    $Userscount=$userData['Count'];
    ////////////////////////////////////////
    $catData = $catClass->getAllCategories();
    $Catcount=sizeof($catData['data']);
    ////////////////////////////////////////
    $memData = $memClass->getAllPlans();
    $Plancount=sizeof($memData['data']);
    ///////////////////////////////////////
    $orderData = $orderClass->getAllOrders();
    $Ordercount=sizeof($orderData['order']);
    ////////////////////////////////////////

    $frontPageData=array(
        "total_coupons"=>$totalCoupons,
        "active_coupons"=>$activeCoupons,
        "Bookscount"=>$Bookscount,
        "Userscount"=>$Userscount,
        "Catcount"=>$Catcount,
        "Plancount"=>$Plancount,
        "Ordercount"=>$Ordercount
    );
    $response[STATUS]=Success;
    $response[MESSAGE]="Data Found";
    $response['frontPageData']=$frontPageData;
    $couponClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $couponClass->apiResponse($response);
}
?>