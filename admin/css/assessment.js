
/**
 * Created by vistasoft on 9/2/16.
 */
var numberofStages = 0,actualWidth= 0,actualHeight=0;
var numberofQuestion = 0;
var step_number = 0;
var templates = [];
var jQuery = $.noConflict(true);
var scrollTopp = 0;
var validMatrixPoints = true;


var baseFonts = ['monospace', 'sans-serif', 'serif','sans-serif','fantasy','default','Arial','Arial Black',
    'Arial Narrow','Arial Rounded MT Bold','Bookman Old Style','Bradley Hand ITC','Century','Century Gothic','Comic Sans MS',
    'Courier','Courier New','Georgia','Gentium','Impact','King','Lucida Console','Lalit','Modena','Monotype Corsiva',
    'Papyrus','Tahoma','TeX','Times','Times New Roman','Trebuchet MS','Verdana','Verona'];

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function() {
    FB.init({
        appId      : '184894498676736',
        xfbml      : true,
        version    : 'v2.2'
    });
    FB.AppEvents.logPageView();
};

(function($) {

    $(document).ready(function () {

    CKEDITOR.replace( 'editor1' );

//    $('#editor').CustomEditor({flat:true});


        $('#colorpickerHolder').ColorPicker({
            flat:true,
            onBeforeShow:function(colpkr){
//            $(colpkr).fadeIn(500);

            },
            onChange: function (hsb, hex, rgb) {
                //alert('i m here');


                $('.assessment-page-title').css('backgroundColor', '#' + hex);
                setTimeout(function(){
                    assessmentModel.title.title_background = '#'+hex;
                    //saveAssessment(true,'none');
                },1000);



            }});


        $('#seekbar-container-vertical-blue').on('click',function(event){
            event.stopPropagation();
        });

        $('#footer-seekbar').on('click',function(event){
            event.stopPropagation();
        });

        $('#font-color').ColorPicker({

            color: '#0000ff',
            onShow: function (colpkr) {
                updateScrollHeight();
                $(colpkr).fadeIn(500);
                $('.colorpicker').css("z-index","10001");

                $(colpkr).mouseleave(function(event){
//                alert('hello');
                    $(colpkr).fadeOut(500);
                    saveAssessment(true,'none');

                    event.stopPropagation();
                });
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
//            event.stopPropagation();
//           $('#font-color').click();
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#font-color').css('backgroundColor', '#' + hex);
                $('#ass-title').css('color', '#' + hex);

                assessmentModel.title.title_color ='#'+hex;


            }
        });

        $('#font-color-desc').ColorPicker({

            color: '#0000ff',
            onShow: function (colpkr) {
                updateScrollHeight();
                $(colpkr).fadeIn(500);
                $('.colorpicker').css("z-index","10001");

                $(colpkr).mouseleave(function(event){
//                alert('hello');
                    $(colpkr).fadeOut(500);
                    saveAssessment(true,'none');

                });

                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#font-color-desc').css('backgroundColor', '#' + hex);
                $('#description').css('color', '#' + hex);
                assessmentModel.setting.setting_color ='#'+hex;


            }
        });

        $('#font-colorr').ColorPicker({

            color: '#0000ff',
            onShow: function (colpkr) {
//            updateScrollHeight();
                $(colpkr).fadeIn(500);
                $('.colorpicker').css("z-index","10001");

                $(colpkr).mouseleave(function(event) {
//                alert('hello');
                    $(colpkr).fadeOut(500);
//                $('.colorpicker').css("z-index","10001");
                    saveAssessment(true,'none');
                    event.stopPropagation();
                })
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#font-colorr').css('backgroundColor', '#' + hex);
                $('#page_footer').css('color', '#' + hex);
                assessmentModel.footer.footer_text_color = '#'+hex;
            }
        });

        $('#footer-colorpicker').ColorPicker({flat: true,
            onShow: function (colpkr) {
//            updateScrollHeight();
                $('.colorpicker').css("z-index","10001");

                $(colpkr).mouseleave(function(event){
//                alert('hello');
                    $(colpkr).fadeOut(500);
//                $('.colorpicker').css("z-index","10001");

                    event.stopPropagation();
                })
            },
            onChange: function (hsb, hex, rgb) {
//            $('.page-area').css('backgroundColor', '#' + hex);
                $('.assessment-footer').css('backgroundColor', '#' + hex);
                assessmentModel.footer.footer_back_color = '#'+hex;

                //saveAssessment(true);
            }});

        $('#page-colorpicker').ColorPicker({flat: true,
            onShow: function (colpkr) {
//            updateScrollHeight();
                $('.colorpicker').css("z-index","10001");

                $(colpkr).mouseleave(function(event){
//                alert('hello');
                    $(colpkr).fadeOut(500);
//                $('.colorpicker').css("z-index","10001");

                    event.stopPropagation();
                })
            },
            onChange: function (hsb, hex, rgb) {
//            $('.page-area').css('backgroundColor', '#' + hex);
                if($("#page_font").is(":checked")){
                    $('#page-text').css('color', '#' + hex);
//                console.log(hex);
                    assessmentModel.page.page_text_color = '#'+hex;
                    $(".question-text").css('color','#'+hex);
                    $(".answer-text").css('color','#'+hex);


                }
                if($("#page_background").is(":checked")) {
                    $('.page-area').css('backgroundColor', '#' + hex);
                    $("#description_d").css("background-color",'#' + hex);
                    //$(".question-header").css('background-color','#'+hex);
                    assessmentModel.page.page_back_color = '#'+hex;
                }
                //saveAssessment(true);
            }});

        $('#colorpickerBack').ColorPicker({flat: true,
            onShow: function (colpkr) {
//            updateScrollHeight();
                $('.colorpicker').css("z-index","10001");

                $(colpkr).mouseleave(function(event){
//                alert('hello');
                    $(colpkr).fadeOut(500);
//                $('.colorpicker').css("z-index","10001");

                    event.stopPropagation();
                })
            },
            onChange: function (hsb, hex, rgb) {
                $('#design-inner').css('backgroundColor', '#' + hex);
//            $(".page-area").css("backgroundColor",'#'+hex);
                assessmentModel.background.color = '#'+hex;
                /*setTimeout(function(){
                 saveAssessment(true);
                 },3000);*/

            }
        });

        $('#colorpickerBack').mouseleave(function(event){
//                alert('hello');
//        alert('finished');
            saveAssessment(true,'none');
        });

        $('#page-colorpicker').mouseleave(function(event){
//                alert('hello');
//        alert('finished');
            saveAssessment(true,'none');
        });

        $('#footer-colorpicker').mouseleave(function(event){
//                alert('hello');
//        alert('finished');
            saveAssessment(true,'none');
        });

        $('#colorpickerHolder').mouseleave(function(event){
//                alert('hello');
//        alert('finished');
            saveAssessment(true,'none');
        });



        var range = document.getElementById('seekbar-container-vertical-blue');

        var footer_seekbar = document.getElementById('footer-seekbar');

        footer_seekbar.style.height = '200px';
        footer_seekbar.style.margin = '0 auto 30px';

        noUiSlider.create(range, {
            start: 150,
//        direction:"rtl",
            orientation: "vertical",
            range: {
                'min': 170,
                'max': 250
            },
            format: wNumb({
                decimals: 0
            })
        });

        noUiSlider.create(footer_seekbar, {
            start: 130,
//        direction:"rtl",
            orientation: "vertical",
            range: {
                'min': 100,
                'max': 130
            },
            format: wNumb({
                decimals: 0
            })
        });
        range.noUiSlider.on('update', function( values, handle ){
            console.log(values[handle]);
            $(".assessment-page-title").css("height",values[handle]+"px");
            setTimeout(function(){
                assessmentModel.title.title_height = values[handle];
                saveAssessment(true,'none');
            },1000);
        });

        footer_seekbar.noUiSlider.on('update', function( values, handle ){
//        console.log(values[handle]);
            $(".assessment-footer").css("height",values[handle]+"px");
            assessmentModel.footer.footer_height = ''+values[handle];
            saveAssessment(true,'none');
//        evt.preventDefault();
//        return false;

        });
//    $(".assessment-page-title").css("height","130px");
        var font_style='';

        for(var i=0;i<baseFonts.length;i++) {
            font_style = font_style+"<option value='"+baseFonts[i]+"'>"+baseFonts[i]+"</option>";
        }

        $("#font-style").html(font_style);
        $("#font-stylee").html(font_style);
        $("#font-style-desc").html(font_style);

        $("#design-ul").on('scroll',function(){
//       console.log('scroll top -- '+$(this).scrollTop());
            scrollTopp = $(this).scrollTop();
        });

        $("#question-range").on('click',function(e){
            console.log('font stylee clicked');
            e.preventDefault();
            return false;
        });
        $("#font-stylee").on('click',function(e){
            console.log('font stylee clicked');
            e.preventDefault();
            return false;
        });
        $("#font-sizee").on('click',function(e){
            console.log('font sizee clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.preventDefault();
            return false;
        });

        $("#font-alignn").on('click',function(e){
            console.log('font sizee clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.preventDefault();
            return false;
        });

        $("#font-style").on('click',function(e){
            console.log('font style clicked');
            e.preventDefault();
            return false;
        });
        $("#font-size").on('click',function(e){
            console.log('font size clicked');
            e.preventDefault();
            return false;
        });

        $("#font-align").on('click',function(e){
            console.log('font align clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.preventDefault();
            return false;
        });
        $("#font-style-desc").on('click',function(e){
            console.log('font style desc clicked');
            e.preventDefault();
            return false;
        });
        $("#font-size-desc").on('click',function(e){
            console.log('font size clicked');
            e.preventDefault();
            return false;
        });

        $("#font-align-desc").on('click',function(e){
            console.log('font align clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.preventDefault();
            return false;
        });
        $("#progress").on('click',function(e){
            console.log('progress clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.stopPropagation();
//        return false;
        });
        $("#back").on('click',function(e){
            console.log('progress clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.stopPropagation();
//        return false;
        });
        $("#progress_top").on('click',function(e){
            console.log('progress_top clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.stopPropagation();
//        return false;
        });
        $("#progress_bottom").on('click',function(e){
            console.log('progress_bottom clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.stopPropagation();
//        return false;
        });
        $("#page_font").on('click',function(e){
            console.log('page_font clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.stopPropagation();
//        return false;
        });

        $("#page_background").on('click',function(e){
            console.log('page_background clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.stopPropagation();
//        return false;
        });


        $("#logo-position").on('click',function(e){
            console.log('logo_position clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.preventDefault();
            return false;
        });


        $("#logo-size").on('click',function(e){
            console.log('logo_size clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.preventDefault();
            return false;
        });

        $("#s_question").on('click',function(e){
            console.log('s_question clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.stopPropagation();
//        return false;
        });
        $("#s_title").on('click',function(e){
            console.log('s_title clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.stopPropagation();
//        return false;
        });

        $("#s_desc").on('click',function(e){
            console.log('s_desc clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.stopPropagation();
//        return false;
        });

        $("#s_back").on('click',function(e){
            console.log('s_back clicked');
//        $("#design-ul").scrollTop(scrollTop);
            e.stopPropagation();
//        return false;
        });

        onBackPressed = function(ref){
            if($(ref).is(':checked')) {
                $('#page_back').css('display','block');
                assessmentModel.setting.setting_is_back = 'A';
            }
            else{
                $('#page_back').css('display','none');
                assessmentModel.setting.setting_is_back = 'D';
            }
            //consoleAssessmentModel();
            saveAssessment(true,'none');
        }

        onBackType = function(ref) {
//        alert($(ref).val());
            $('#page_back').html($(ref).val());

            setTimeout(function(){
                assessmentModel.setting.setting_back_text = $(ref).val();
                saveAssessment(true,'none');
            },1000);
        }

        onNextType = function(ref) {
            $('#page_next').html($(ref).val());

            setTimeout(function(){
                assessmentModel.setting.setting_next_text = $(ref).val();
                saveAssessment(true,'none');
            },1000);
        }

        show_title = function(ref) {
            if($(ref).is(":checked")){
                $(".assessment-page-title").css("display","block");
                assessmentModel.setting.setting_is_header = 'A';
            }
            else{
                $(".assessment-page-title").css("display","none");
                assessmentModel.setting.setting_is_header = 'D';

            }
            saveAssessment(true,'none');
        }

        show_description = function(ref) {
            if($(ref).is(":checked")){
                $("#description").css("display","block");
                assessmentModel.setting.setting_is_desc = 'A';
            }
            else{
                $("#description").css("display","none");
                assessmentModel.setting.setting_is_desc = 'D';
            }
            saveAssessment(true,'none');
        }

        footerChecked = function(ref) {
            if($(ref).attr('id') == 'font'){
                $("#background").prop('checked',false);
                assessmentModel.footer.footer_is_font = 'A';
            }
            else{
                $("#font").prop('checked',false);
                assessmentModel.footer.footer_is_font = 'D';
            }
            saveAssessment(true,'none');
            $("#design-ul").scrollTop(scrollTopp);
//        $('#design-ul').animate({ scrollTop: scrollTopp }, 'fast');
        }

        $("#desktop").on('click',function(e){
            e.stopPropagation();
//        e.preventDefault();
//        return false;
        });
        $("#tab").on('click',function(e){
            e.stopPropagation();
//        e.preventDefault();
//        return false;
        });
        $("#smartphone").on('click',function(e){
            e.stopPropagation();
//        e.preventDefault();
//        return false;
        });
        $("#page_width").on('click',function(e){
            e.stopPropagation();
//        e.preventDefault();
//        return false;
        });
        $("#page_content_width").on('click',function(e){
            e.stopPropagation();
//        e.preventDefault();
//        return false;
        });

        $("#preview").on('click',function(e){
            e.stopPropagation();
//        e.preventDefault();
//        return false;
            var selected = '';
            $.each($("input[name='layout_1']:checked"), function(){

//            favorite.push($(this).val());
                selected = $(this).val();
            });

            var page_width = parseInt($("#page_width").val())+20;
            var page_content_width = parseInt($("#page_content_width").val());

            if(selected =="desktop" && page_width>100) {
                alert("Desktop width should not cross 80%");
                return;
            }
            if(selected =="tab" && page_width>95) {
                alert("Tab width should not cross 75%");
                return;
            }
            if(selected =="smartphone" && page_width>78) {
                alert("Smartphone width should not cross 58%");
                return;
            }
            if(page_content_width>parseInt($("#page_width").val())) {
                alert('Page content width should not greater than page width');
                return;
            }

            $("#main-contentt").css("width",page_width+"%");
            console.log("preview clicked -- "+selected);
            if(assessmentModel.setting.setting_question_limit =='')
                insertLayoutQuestions1(0,5,0);
            else
                insertLayoutQuestions1(0,parseInt(assessmentModel.setting.setting_question_limit),0);

            //$('.question-header').css('background-color',assessmentModel.page.page_back_color);
            $('.question-text').css('color',assessmentModel.page.page_text_color);
            $('.answer-text').css('color',assessmentModel.page.page_text_color);


        });
        layout_checked = function(ref,event){
            console.log('id here -- '+ref.id);
            $(".layout_").prop("checked",false);
            $("#"+ref.id).prop("checked",true);
//        $("#smartphone").prop("checked",false);
            $('#main-contentt').css("width","");
            if(ref.id == "desktop") {
                $('#main-contentt').removeClass('col-xs-12');
                $('#main-contentt').removeClass('col-sm-12');
                $('#main-contentt').removeClass('col-md-12');
                $('#main-contentt').removeClass('col-lg-12');
                $('#main-contentt').removeClass('col-md-9');
                $('#main-contentt').removeClass('col-lg-9');
                $('#main-contentt').removeClass('col-md-7');
                $('#main-contentt').removeClass('col-lg-7');
                $('#main-contentt').addClass('col-md-12');
                $('#main-contentt').addClass('col-lg-12');
                $("#page_width").val("80");
            }

            if(ref.id == "tab") {
                $('#main-contentt').removeClass('col-xs-12');
                $('#main-contentt').removeClass('col-sm-12');
                $('#main-contentt').removeClass('col-md-12');
                $('#main-contentt').removeClass('col-lg-12');
                $('#main-contentt').removeClass('col-md-7');
                $('#main-contentt').removeClass('col-lg-7');
                $('#main-contentt').addClass('col-md-9');
                $('#main-contentt').addClass('col-lg-9');
                $("#page_width").val("75");
            }
            if(ref.id == "smartphone") {
                $('#main-contentt').removeClass('col-xs-12');
                $('#main-contentt').removeClass('col-sm-12');
                $('#main-contentt').removeClass('col-md-12');
                $('#main-contentt').removeClass('col-lg-12');
                $('#main-contentt').removeClass('col-md-9');
                $('#main-contentt').removeClass('col-lg-9');
                $('#main-contentt').addClass('col-md-7');
                $('#main-contentt').addClass('col-lg-7');
                $("#page_width").val("58");
            }
//        $('#'+ref.id).prop("checked",true);
            assessmentModel.layout.layout_selected = ''+ref.id;
            saveAssessment(true,'none');

        }
        updateFontSize = function(id,font_size){
            var font_sizee = $("#"+font_size).val();
            $('#'+id).css('font-size',font_sizee);
//        updateScrollHeight();
//        $('#design-ul').animate({ scrollTop: scrollTopp }, 'slow');
//        $("#design-ul").scrollTop(scrollTopp);
            if(font_size =='font-size-desc') {
                assessmentModel.setting.setting_text_size = font_sizee;
            }
            else if(font_size == 'font-size') {
                assessmentModel.title.title_size = font_sizee;
            }
            else if(font_size == 'font-sizee') {
                assessmentModel.footer.footer_text_size = font_sizee;
            }

            saveAssessment(true,'none');

        }

        updateFontStyle = function(id,font_style){
            var font_stylee = $("#"+font_style).val();
            $('#'+id).css('font-family',font_stylee);
            if(font_style =='font-style-desc'){
                assessmentModel.setting.setting_font = font_stylee;
            }
            else if(font_style == 'font-style') {
                //alert('hi');
                assessmentModel.title.title_font = font_stylee;
            }
            else if(font_style == 'font-stylee') {
                assessmentModel.footer.footer_font = font_stylee;
            }
            saveAssessment(true,'none');
//        $("#design-ul").scrollTop(scrollTopp);

//        $('#design-ul').animate({ scrollTop: scrollTopp }, 'slow');
////        updateScrollHeight();
        }

        updateTextStyle = function(id,font_align) {
            var text_align = $("#"+font_align).val();
//        updateScrollHeight();
            $('#design-ul').animate({ scrollTop: scrollTopp }, 'slow');

            if(id == 'ass-title') {
                if(text_align =='center') {
                    var total_width = parseInt($('.assessment-page-title').width())+10;
                    var logo_width = $('#ass-logo').width();
                    var margin_area = (total_width-logo_width)/2;
//                    alert(margin_area+'total '+total_width+' logo '+logo_width);
                    /*if($('#'+id).css('float') =='left') {
                        //$('#'+id).css('text-align','right');
                        $('#'+id).css('margin-left','0px');
                        $('#ass-title').removeClass('pull-right');
                        $('#ass-title').removeClass('pull-left');


                    }
                    else if($('#'+id).css('float') =='right'){
                        //$('#'+id).css('text-align','left');
                        $('#'+id).css('margin-right',margin_area);
                        $('#ass-title').removeClass('pull-right');
                        $('#ass-title').removeClass('pull-left');

                    }*/

                    $('#'+id).css('margin-left','0px');
                    $('#'+id).css('margin-left','0px');
                    $('#ass-title').removeClass('pull-right');
                    $('#ass-title').removeClass('pull-left');
                    $('#ass-title').css('text-align','center');

                }
                else if(text_align == 'left') {

                    //      $(".assessment-logo").addClass("pull-left");
                    /*
                     $('.assessment-page-title').css('flex-direction','row-reverse');
                     $('.assessment-page-title').css('-webkit-flex-direction','row-reverse');
                     $('#ass-logo-container').css('text-align','right');
                     $('#ass-title').css('text-align','left');
                     */
                    $('#ass-title').removeClass('pull-right');
                    $('#ass-title').removeClass('pull-left');
                    $('#ass-title').addClass('pull-left');
                    $('#ass-title').css('margin-left','0px');
                    $('#ass-title').css('margin-right','0px');

                }
                else if(text_align == 'right') {

//      $(".assessment-logo").addClass("pull-right");
                    /*    $('.assessment-page-title').css('flex-direction','row');
                     $('.assessment-page-title').css('-webkit-flex-direction','row');
                     $('#ass-logo-container').css('text-align','left');
                     $('#ass-title').css('text-align','right');
                     */
                    $('#ass-title').removeClass('pull-right');
                    $('#ass-title').removeClass('pull-left');
                    $('#ass-title').addClass('pull-right');
                    $('#ass-title').css('margin-left','0px');
                    $('#ass-title').css('margin-right','0px');

                }
            }
            else if(id == 'assessment-footer') {
                if(text_align == 'right') {
//                    $("."+id).css("text-align","right");
                    $("#page_footer").removeClass("pull-right");
                    $("#page_footer").removeClass("pull-left");
                    $("#page_footer").addClass("pull-right");
                    $("#page_footer").css("text-align","");
                }
                else  if(text_align == 'left') {
                    $("#page_footer").removeClass("pull-right");
                    $("#page_footer").removeClass("pull-left");
                    $("#page_footer").addClass("pull-left");
                    $("#page_footer").css("text-align","");
                }
                if(text_align == 'center') {
                    $("#page_footer").removeClass("pull-right");
                    $("#page_footer").removeClass("pull-left");
                    $("#page_footer").css("text-align","center");
                }
            }

            else if(id == 'description') {
                if(text_align == 'right') {
                    $("#"+id).css("text-align","right");
                }
                else  if(text_align == 'left') {
                    $("#"+id).css("text-align","left");
                }
                if(text_align == 'center') {
                    $("#"+id).css("text-align","center");
                }
            }

            if(font_align =='font-align-desc') {
                assessmentModel.setting.setting_alignment = text_align;
            }
            else if(font_align =='font-align') {
                assessmentModel.title.title_alignment = text_align;
            }
            else if(font_align == 'font-alignn') {
                assessmentModel.footer.footer_text_align = text_align;
            }
            saveAssessment(true,'none');
        }

        pagewidth_typed = function(ref){
            var value = $(ref).val();

            if(!isNaN(value) ){
                assessmentModel.layout.layout_page_width = value;
                saveAssessment(true,'none');
            }
            else{
                alert('Please enter valid input');
            }
        }

        page_content_typed = function(ref){
            var value = $(ref).val();
            if(!isNaN(value) ){
                assessmentModel.layout.layout_content_width = value;
                saveAssessment(true,'none');
            }
            else{
                alert('Please enter valid input');
            }
        }

        typeDescription = function() {
//     console.log($("#desc").val());
            $("#description").html($("#desc").val());

            assessmentModel.setting.setting_desc_text = ''+$("#desc").val();
            saveAssessment(true,'none');

        }



        pageChecked = function(ref,e)  {
//        updateScrollHeight();
            console.log(ref.id);
            if($(ref).attr('id') == 'page_font'){
                $("#page_background").prop('checked',false);
                assessmentModel.page.page_is_font = 'A';
            }
            else{
                $("#page_font").prop('checked',false);
                assessmentModel.page.page_is_font = 'D';
            }
            saveAssessment(true,'none');
        }



        progressChecked = function(id)  {
//        updateScrollHeight();
            var page = $("html, body");
            if(id == 'progress_top'){
                $("#progress_bottom").prop('checked',false);
                $(".page-area").css('flex-direction','column-reverse');
                $(".page-area").css('-webkit-flex-direction','column-reverse');
                assessmentModel.page.page_is_top = 'A';
            }
            else{
                $("#progress_top").prop('checked',false);
                $(".page-area").css('flex-direction','column');
                $(".page-area").css('-webkit-flex-direction','column');
                assessmentModel.page.page_is_top = 'D';
            }
            saveAssessment(true,'none');
//        page.stop();

        }

        showProgress = function(ref,evt){
//      updateScrollHeight();
            if($(ref).is(":checked")) {
                $(".progress").css("display","block");
                assessmentModel.page.page_is_progress = 'A';
            }
            else{
                $(".progress").css("display","none");
                assessmentModel.page.page_is_progress = 'D';
            }
            saveAssessment(true,'none');
        }
//    alert($(window).height());
        var height =   $(window).height()+70;
        $("#design-content").css("height",height+"px");
        $("#design-ul").css("height",height+"px");
//    $("#design-inner").css("height",height+"px");
//    $("#main-contentt").css("height",(height-20)+"px");

        updateScrollHeight = function()
        {
//      $("#design-ul").scrollTop(scrollTopp);
//        $('#design-ul').animate({ scrollTop: scrollTopp }, 'slow');

            console.log('scroll height -- '+scrollTopp);
        }
        $.get("../../services/Transaction.php", {fn: "getSelectedTransactionModel"}, function (response, status) {

            response = JSON.parse( response );
//        alert('account type -- '+$("#account_type").val());
            console.log(JSON.stringify(assessmentModel));
            if (response.success_flag) {

                transactionModel = response.transaction_model;

                setTimeout(function () {

                    $(".as-builds .as-build-2 .as-scoring-3 .as-axis-scores .as-score-questions .as-score-question .as-points input, .as-builds .as-build-2 .as-scoring-4 .as-question-editor-advanced .as-content .as-foot .as-points input, .as-builds .as-build-6 .as-matrixes .as-matrix-3 .as-table input").attr("readonly", systemWorkMode !== "Development" && !transactionModel.weighting);
                }, 3000);

            }

        });

        $.get("../../services/TemplateService.php", {"service_function": "getTemplateData"}, function (response, status) {

            response = JSON.parse( response );
//        alert('account type -- '+$("#account_type").val());
//        console.log(JSON.stringify(assessmentModel));

            if (response.success_flag) {
                var temp_data = response.templateData;
                var temp_txt = "";
                for(var i = 0;i<temp_data.length;i++) {
                    templates.push(temp_data[i]);
                    if(i == 0){
                        /*temp_txt = temp_txt +'<div class="theme-item " onclick="themeSelected('+i+','+true+')"><div class="selected-item active" id="selected-'+i+'"><i class="fa fa-check"></i></div><div class="theme-body" style="background-color: '+temp_data[i].temp_pageback_color+'"><div class="theme-body-content"></div>'+
                         '<div class="theme-body-contentt " ></div><div class="theme-body-contentt theme-body-title" style="background-color: '+temp_data[i].temp_back_color+'"></div>' +
                         '<div class="theme-body-contentt theme-body-page" style="color: '+temp_data[i].temp_pagetext_color+'">Question Answer</div>'+
                         '<div class="theme-body-contentt theme-body-page" style="color: '+temp_data[i].temp_pagetext_color+'">Question Answer</div><div class="theme-body-contentt theme-body-title" style="background-color: '+temp_data[i].temp_footerback_color+'"></div></div>'+
                         '<div class="theme-footer"><p style="padding-bottom:5px;padding-left: 5px">'+temp_data[i].temp_name+'</p><p style="margin-left: auto;padding-bottom:5px;padding-right:5px"></p>'+
                         '</div></div></div>';*/
                        temp_txt = temp_txt +'<div class="theme-item " onclick="themeSelected('+i+','+true+')"><div class="selected-item active" id="selected-'+i+'"><i class="fa fa-check"></i></div>'+
                            '<img src="images/'+temp_data[i].temp_path+'" style="width:90px;height:122px;margin-top:10px"><div class="theme-footer"><p style="padding-bottom:5px;padding-left: 5px">'+temp_data[i].temp_name+'</p><p style="margin-left: auto;padding-bottom:5px;padding-right:5px"></p>'+
                            '</div></div>';

                    }
                    else{
                        /*temp_txt = temp_txt +'<div class="theme-item " onclick="themeSelected('+i+','+true+')"><div class="selected-item" id="selected-'+i+'"><i class="fa fa-check"></i></div><div class="theme-body"><div class="theme-body-content"></div>'+
                         '<div class="theme-body-contentt " style="background-color: '+temp_data[i].temp_titleback_color+'"></div><div class="theme-body-contentt theme-body-title" style="background-color: '+temp_data[i].temp_back_color+'"></div>' +
                         '<div class="theme-body-contentt theme-body-page" style="background-color: '+temp_data[i].temp_back_color+'"></div>'+
                         '<div class="theme-body-contentt theme-body-page" style="background-color: '+temp_data[i].temp_back_color+'"></div><div class="theme-body-contentt theme-body-title" style="background-color: '+temp_data[i].temp_footerback_color+'"></div></div>'+
                         '<div class="theme-footer"><p style="padding-bottom:5px;padding-left: 5px">'+temp_data[i].temp_name+'</p><p style="margin-left: auto;padding-bottom:5px;padding-right:5px"></p>'+
                         '</div></div></div>';*/

                        temp_txt = temp_txt +'<div class="theme-item " onclick="themeSelected('+i+','+true+')"><div class="selected-item" id="selected-'+i+'"><i class="fa fa-check"></i></div>'+
                            '<img src="images/'+temp_data[i].temp_path+'" style="width:90px;height:122px;margin-top:10px"><div class="theme-footer"><p style="padding-bottom:5px;padding-left: 5px">'+temp_data[i].temp_name+'</p><p style="margin-left: auto;padding-bottom:5px;padding-right:5px"></p>'+
                            '</div></div>';

                    }
                }
                $('.theme-box').html(temp_txt);
            }

        });

        /*$('#popovercloseid').on('click',function(){
         alert('closed');
         });*/

        $.get("../../services/AccountService.php", {'service_function': "getUserData"}, function (response, status) {

            response = JSON.parse(response);

            if (response.success_flag) {

                userModel = response.userData;
//            alert(userModel.account_video_view);
            }


        });


    });




    updateMatrixPoint = function(ref,axis,index,pointType) {
//    console.log($(ref).val());
        if($(ref).val().trim()<0 || isNaN($(ref).val().trim())) {
            $(ref).css('border-color','red');
            validMatrixPoints = false;
            return;
        }
        else{
            validMatrixPoints = true;
            $(ref).css("border-color", "#bbb");
        }

        //alert("index --- "+index+" axis-- "+axis+' length here -- '+assessmentModel.matrix_points[axis].length);

        var max = 0,min = 0,prev_value = 0,diff_value = 0;

        var matrix_value = parseInt($(ref).val());
        //alert(index);
        var asTable = $(".as-builds .as-build-6 .as-matrixes .as-matrix-3 .as-table");

        if(pointType == 'start') {
            if (index == 0 && matrix_value > 0 && axis == 'X') {              // first case to check 0
                $(ref).css('border-color', 'red');
                //alert("Inital score should be 0");
                validMatrixPoints = false;
                return;
            }
            else {
                $(ref).css("border-color", "#bbb");
                validMatrixPoints = true;
            }
        }

        if(index>0 && axis =='X' ) {
            if(pointType == 'start') {
                var index_x = index - 1;

                prev_value = parseInt(assessmentModel.matrix_points[axis][index_x].end);
                diff_value = matrix_value - prev_value;

                if (diff_value == 1) {
                    $(ref).css("border-color", "#bbb");
                    validMatrixPoints = true;
                }
                else if (diff_value > 1) {
                    $(ref).css('border-color', 'red');
                    //alert("You have a gap in your scoring.Please fix before preceeding");
                    validMatrixPoints = false;
                    return;
                }
                else if (diff_value < 0) {
                    $(ref).css('border-color', 'red');
                    //alert("You have an overlap in your scoring.Please fix before preceeding");
                    validMatrixPoints = false;
                    return;
                }
            }

        }

        if(axis =='Y' ) {
            var index_y = parseInt(assessmentModel.matrix_points[axis].length-1);

            if(parseInt(assessmentModel.matrix_points[axis][index_y].end)>100) {
                index_y = index_y-1;
            }
            console.log('index -- '+index+' index y -- '+index_y);
            //alert(index_y);

            if(index<index_y  && pointType =='start') {

                var prev_index = 0;
                if(index>0) {
                    if (!(index == assessmentModel.matrix_points[axis].length - 1)) {
                        prev_index = parseInt(index) - 1;
                    }
                    else {
                        prev_index = assessmentModel.matrix_points[axis].length - 1;
                    }
                }
                //alert(prev_index);
                console.log('prev index -- '+prev_index);

                prev_value = parseInt(assessmentModel.matrix_points[axis][prev_index].end);

                diff_value = matrix_value-prev_value;
                console.log('matrix value -- '+matrix_value+' prev value '+prev_value);

                if(diff_value == 1 ) {
                    $(ref).css("border-color", "#bbb");
                    validMatrixPoints = true;
                }
                else if(diff_value>1) {
                    $(ref).css('border-color','red');
                    //alert("You have a gap in your scoring.Please fix before preceeding");
                    validMatrixPoints = false;
                    return;
                }
                else if(diff_value<0 ) {
                    $(ref).css('border-color','red');
                    validMatrixPoints = false;
                    //alert("You have an overlap in your scoring.Please fix before preceeding");
                    return;
                }
            }
            else if( index == index_y && pointType == 'end') {

                var next_index = parseInt(index);
                var next_value = parseInt(assessmentModel.matrix_points[axis][next_index].start);
                diff_value = parseInt(matrix_value-next_value);
                console.log('difference in value --- '+diff_value);

                if(diff_value == 1) {
                    $('#'+ref.id).css("border-color", "#bbb");
                    validMatrixPoints = true;
                }
                else if(diff_value<0) {
                    $('#'+ref.id).css("border-color", "red");
                    validMatrixPoints = false;
                    return;
                }
                else{
                    console.log('in else case-- '+ref.id);
                    $('#'+ref.id).css("border-color", "red");
                    validMatrixPoints = false;
                    return;
                }

            }
            else if(index < index_y && pointType == 'end') {
                next_index = 0;
                var indexx_y = parseInt((assessmentModel.matrix_points[axis].length-1)-index);

                if (!(indexx_y == assessmentModel.matrix_points[axis].length - 1)) {
                    next_index = parseInt(index) + 1;
                    next_value = parseInt(assessmentModel.matrix_points[axis][next_index].start);

                    diff_value = parseInt(matrix_value-next_value);
                    console.log('difference in value --- '+diff_value);

                    if(diff_value == 1) {
                        $('#'+ref.id).css("border-color", "#bbb");
                        validMatrixPoints = true;
                    }
                    else if(diff_value<0) {
                        $('#'+ref.id).css("border-color", "red");
                        validMatrixPoints = false;
                        return;
                    }
                    else{
                        console.log('in else case-- '+ref.id);
                        $('#'+ref.id).css("border-color", "red");
                        validMatrixPoints = false;
                        return;
                    }
                }

                console.log('next index -- '+next_index);
                console.log('next value -- '+next_value);
                console.log('next indexx_y -- '+indexx_y);


            }
            else if(index_y == index && pointType == 'start'){
                if( matrix_value>0) {
                    $(ref).css('border-color','red');
                    alert("Intial value must be 0");
                    validMatrixPoints = false;
                    return;
                }
                else{
                    $(ref).css("border-color", "#bbb");
                    validMatrixPoints = true;
                }
            }
        }

        for( var i=0;i<asTable.children().length;i++) {
            max = parseInt(asTable.eq(i).find(".as-"+axis.toLowerCase()+"-"+index+" .as-"+axis+"-1").val().trim());
            min = parseInt(asTable.eq(i).find(".as-"+axis.toLowerCase()+"-"+index+" .as-"+axis+"-0").val().trim());
        }

        if(pointType == 'start') {
//            console.log('max value -- '+max);
//            console.log('min value -- '+min);

            /*if(min>max || isNaN(min) || min<0) {

                $(ref).css('border-color','red');
                return;
            }
            else{
                $(ref).css("border-color", "#bbb");
            }*/

            if(axis == 'X')
                assessmentModel.matrix_points[axis][index].start = $(ref).val();
            if (axis == 'Y')
            {
                var index_value = parseInt(index)+1;
                //alert(index_value);
                var indexx = parseInt((assessmentModel.matrix_points[axis].length-1)-index_value);

                assessmentModel.matrix_points[axis][indexx].start = $(ref).val();
                console.log('end index -- '+indexx);
            }
        }


        else if(pointType == 'end') {

            /*if(max<min || max>100 || isNaN(max)) {
                $(ref).css('border-color','red');
                return;
            }
            else{
                $(ref).css("border-color", "#bbb");
            }*/

            index_value = parseInt(index)+1;
            //alert(index_value);

            indexx = parseInt((assessmentModel.matrix_points[axis].length-1)-index_value);



            if(axis == 'X')
             assessmentModel.matrix_points[axis][index].end = $(ref).val();
            if (axis == 'Y')
            {

                assessmentModel.matrix_points[axis][indexx].end = $(ref).val();
                console.log('end index -- '+indexx);
            }

        }
//        consoleAssessmentModel();
    }

    addNewQuestion = function (axis, type) {
        var  account_type = $("#account_type").val();
        var isAddNew = true;
        switch(account_type)
        {
            case "free":
                if(numberofQuestion>=5) {
                    isAddNew = false;
                }
                else{
                    numberofQuestion++;
                }
                break;
            case "Koalafy Joey":
                if(numberofQuestion>=10) {
                    isAddNew = false;
                }
                else {
                    numberofQuestion++;
                }
                break;
        }
        if(isAddNew)
        {

            insertNewQuestionEditor(axis, type, assessmentModel.questions[axis].length);
        }
        else
        {
            var message = escape("To Add More Questions You Need to Upgrade Your Plan");
            showpop(message);
        }
        console.log('num of questions ---- '+numberofQuestion+" account type "+account_type);
    }

    consoleAssessmentModel = function () {

        console.log(JSON.stringify(assessmentModel));
    }

    deleteQuestion = function (axis, q_index) {

        assessmentModel.questions[axis].splice(q_index, 1);

        if (axis === "X") {

            $(".as-builds .as-build-3 .as-questions .as-question-builder-" + q_index).remove();
            $(".as-builds .as-build-2 .as-scoring-3 .as-axis-scores-1 .as-score-questions .as-score-question-" + q_index).remove();
        } else if (axis === "Y") {

            $(".as-builds .as-build-4 .as-questions .as-question-builder-" + q_index).remove();
            $(".as-builds .as-build-2 .as-scoring-3 .as-axis-scores-2 .as-score-questions .as-score-question-" + q_index).remove();
        }
        redrawQuestionIndexes(axis);
        if(axis === "X" && assessmentModel.matrix.length > 0 && assessmentModel.matrix[0].length > assessmentModel.questions["X"].length) {

            if(assessmentModel.matrix[0].length <= 1 && assessmentModel.questions["Y"].length > 0) {

            } else {

                for(var j = 0;j < assessmentModel.matrix.length;j++) {

                    assessmentModel.matrix[j].splice(q_index, 1);
                }

                if(assessmentModel.matrix[0].length < 1) {

                    assessmentModel.matrix.length = 0;
                }

                assessmentModel.matrix_points[axis].splice(q_index, 1);
            }
        } else if(axis === "Y" && assessmentModel.matrix.length > assessmentModel.questions["Y"].length) {

            if(assessmentModel.matrix.length <= 1 && assessmentModel.questions["X"].length > 0) {

            } else {

                assessmentModel.matrix.splice(q_index, 1);

                assessmentModel.matrix_points[axis].splice(q_index, 1);
            }
        }

        redrawMatrix();
        redrawScores(axis);
        numberofQuestion--;
    }

    drawAxisMinMax = function(axis) {

        var min = 0;
        var max = 0;

        for(var i in assessmentModel.questions[axis]) {

            min += assessmentModel.questions[axis][i].min;

            max += assessmentModel.questions[axis][i].max;
        }

        if(axis === "X") {

            $(".as-builds .as-build-2 .as-scoring-2 .as-table .vs-x .vs-min, .as-builds .as-build-2 .as-scoring-3 .as-axis-scores-1 .as-head .as-points .as-min, .as-builds .as-build-6 .as-matrixes .as-matrix .as-points .vs-x .vs-min").html(min);
            $(".as-builds .as-build-2 .as-scoring-2 .as-table .vs-x .vs-max, .as-builds .as-build-2 .as-scoring-3 .as-axis-scores-1 .as-head .as-points .as-max, .as-builds .as-build-6 .as-matrixes .as-matrix .as-points .vs-x .vs-max").html(max);
        } else if(axis === "Y") {

            $(".as-builds .as-build-2 .as-scoring-2 .as-table .vs-y .vs-min, .as-builds .as-build-2 .as-scoring-3 .as-axis-scores-2 .as-head .as-points .as-min, .as-builds .as-build-6 .as-matrixes .as-matrix .as-points .vs-y .vs-min").html(min);
            $(".as-builds .as-build-2 .as-scoring-2 .as-table .vs-y .vs-max, .as-builds .as-build-2 .as-scoring-3 .as-axis-scores-2 .as-head .as-points .as-max, .as-builds .as-build-6 .as-matrixes .as-matrix .as-points .vs-y .vs-max").html(max);
        }
    }

    drawAxisPotentialResults = function(axis) {
        console.log('i m in draw -- '+axis);
        var results = findAxisPotentialResults(axis);

        var selectAxis = axis === "X" ? "y" : "x";

        $(".as-builds .as-build-6 .as-matrixes .as-matrix-2 .vs-" + axis.toLowerCase() + " .as-results").html("");
        $(".as-builds .as-build-6 .as-matrixes .as-matrix-2 .vs-" + selectAxis + " select").html("");

        for(var i in results) {

            $(".as-builds .as-build-6 .as-matrixes .as-matrix-2 .vs-" + axis.toLowerCase() + " .as-results").append("<span style='margin: 10px'>" + results[i] + "</span>");
            $(".as-builds .as-build-6 .as-matrixes .-matrix-2 .vs-" + selectAxis + " select").append("<option>" + results[i] + "</option>");
        }
    }

    insertNewStage = function (index) {
        var stagesCount = $("#stagescount").val();
        var  account_type = $("#account_type").val();
        switch(account_type)
        {
            case "free":
                numberofStages = 2;
                break;
            case "Koalafy Joey":
                numberofStages = 3;
                break;
            case "Koalafy Eucalyptus":
                numberofStages = 5;
                break;
            case "Koalafy King":
                numberofStages = 6;
                break;
        }
        if (stagesCount == numberofStages)
        {
            var message = escape("To Add More Stages you Need to Upgrade your Account.");
            showpop(message);
            return;
        }
        else
        {
            stagesCount++;
            $("#stagescount").val(stagesCount);
        }
        assessmentModel.stages.splice(index, 0, {
            id: "",
            abbreviation: "",
            description: "",
            jsToken: saveJSToken(25)
        });

        insertStage(index);
    }

    insertStage = function (index) {

        var html = '<div class="col-xs-12 as-abbreviation as-abbreviation-' + index + '">' +
            '<div class="col-md-3 vs-padding-left as-abr">' +
            '<input class="col-xs-12" placeholder="1-4 Letters" value="' + assessmentModel.stages[index].abbreviation + '" onkeyup="updateStageAbbreviation(' + index + ', this.value);" />' +
            '</div>' +
            '<div class="col-md-8 vs-padding-left as-description">' +
            '<input class="col-xs-12" placeholder="Description Text" value="' + assessmentModel.stages[index].description + '" onkeyup="assessmentModel.stages[' + index + '].description = this.value;" />' +
            '</div>' +
            '<div class="col-md-1 as-control">' +
            '<span class="glyphicon glyphicon-plus" onclick="insertNewStage(' + ( index + 1 ) + ');redrawStageIndexes();" aria-hidden="true"></span>' +
            '<span class="glyphicon glyphicon-remove" onclick="removeStage(' + index + ');redrawStageIndexes();" aria-hidden="true"></span>' +
            '</div>' +
            '</div>';

        insertHtmlElement($(".as-builds .as-build-6 .as-matrixes .as-matrix-1 .as-abbreviations"), index, html);

        html = "<div class='col-xs-12 as-abbreviation as-abbreviation-" + index + "'>" +
            "<span class='col-xs-12' draggable='true' ondragstart='event.dataTransfer.setData(\"id\", \"" +
            assessmentModel.stages[index].id + "\");event.dataTransfer.setData(\"abbr\", \"" +
            assessmentModel.stages[index].abbreviation + "\");'>" + assessmentModel.stages[index].abbreviation + "</span></div>";
        insertHtmlElement($(".as-builds .as-build-6 .as-matrixes .as-matrix-3 .as-abbreviations"), index, html);
    }

    logoutFromSystem = function () {

        $.post("../../services/AccountService.php", {service_function: "logoutFromSystem"}, function (response, status) {

            var response = JSON.parse(response);

            if (response.success_flag) {

                window.location = "../login/login.php";
            }
        });
    }

    redrawAdvancedQuestionEditorAnswerIndexes = function(axis, e_index) {

        $(".as-builds .as-build-2 .as-scoring-4 .as-question-editor-advanced-" + axis + "-" + e_index + " .as-content .as-answers .as-answer-edit").each(function(index, asAnswerEditor) {

            $(asAnswerEditor).attr("class", "col-xs-12 as-answer-edit as-answer-edit-" + index);
        });
    }

    updateAssessmentDetails = function () {

        assessmentModel.details.typeID = jQuery(".assessment-details .type select").val().trim();
        assessmentModel.details.resultsType = jQuery(".assessment-details .results-type .type-table").prop("checked") ? "Table" : "Chart";
    }

    redrawStageIndexes = function () {

        var asAbbreviation = $(".as-builds .as-build-6 .as-matrixes .as-matrix-1 .as-abbreviations .as-abbreviation");

        for (var i = 0; i < asAbbreviation.length; i++) {

            asAbbreviation.eq(i).attr("class", "col-xs-12 as-abbreviation as-abbreviation-" + i);
            asAbbreviation.eq(i).find(".as-abr input").attr("onkeyup", "updateStageAbbreviation(" + i + ", this.value);");
            asAbbreviation.eq(i).find(".as-description input").attr("onkeyup", "assessmentModel.stages[" + i + "].description = this.value;");
            asAbbreviation.eq(i).find(".as-control .glyphicon-plus").attr("onclick", "insertNewStage(" + ( i + 1 ) + ");redrawStageIndexes();");
            asAbbreviation.eq(i).find(".as-control .glyphicon-remove").attr("onclick", "removeStage(" + i + ");redrawStageIndexes();");
        }

        asAbbreviation = $(".as-builds .as-build-6 .as-matrixes .as-matrix-3 .as-abbreviations .as-abbreviation");

        for (var i = 0; i < asAbbreviation.length; i++) {

            asAbbreviation.eq(i).attr("class", "col-xs-12 as-abbreviation as-abbreviation-" + i);
        }
    }

    repaintAdvancedQuestionEditor = function(axis, e_index)
    {

        var advancedQuestionEditor = $(".as-builds .as-build-2 .as-scoring-4 .as-question-editor-advanced-" + axis + "-" + e_index);
        if(advancedQuestionEditor.length > 0) {

            advancedQuestionEditor.find(".as-content .as-question-edit .as-text").val(assessmentModel.questions[axis][e_index].text);
            advancedQuestionEditor.find(".as-content .as-foot .as-points .as-min").val(assessmentModel.questions[axis][e_index].min);
            advancedQuestionEditor.find(".as-content .as-foot .as-points .as-max").val(assessmentModel.questions[axis][e_index].max);

            $(".as-builds .as-build-2 .as-scoring-4 .as-question-editor-advanced-" + axis + "-" + e_index + " .as-content .as-answers .as-answer-edit").each(function(index, asAnswerEditor) {

                $(asAnswerEditor).find(".as-score input").val(assessmentModel.questions[axis][e_index].answers[index].score);
                $(asAnswerEditor).find(".as-text input").val(assessmentModel.questions[axis][e_index].answers[index].text);
            });
        }
    }

    removeStage = function (index) {

        assessmentModel.stages.splice(index, 1);

        removeHtmlElement($(".as-builds .as-build-6 .as-matrixes .as-matrix-1 .as-abbreviations"), index);
        removeHtmlElement($(".as-builds .as-build-6 .as-matrixes .as-matrix-3 .as-abbreviations"), index);

        if (assessmentModel.stages.length <= 0) {

            insertNewStage(0);
        }
        var stagesCount2 = $("#stagescount").val();
        stagesCount2--;
        $("#stagescount").val(stagesCount2);
    }

    updateStageAbbreviation = function (index, abr) {

        if (abr.length > 4) {

            abr = abr.substr(0, 4);

            $(".as-builds .as-build-6 .as-matrixes .as-matrix-1 .as-abbreviations .as-abbreviation").eq(index).find(".as-abr input").val(abr);
        }

        assessmentModel.stages[index].abbreviation = abr;

        $(".as-builds .as-build-6 .as-matrixes .as-matrix-3 .as-abbreviations .as-abbreviation").eq(index).find("span").attr("ondragstart", "event.dataTransfer.setData('id', '" + assessmentModel.stages[index].id + "');event.dataTransfer.setData('abbr', '" + assessmentModel.stages[index].abbreviation + "');").html(abr);
    }

    onContinueDetailsClicked = function () {

        if (validateAssessmentDetails()) {
            selectASStep(2);
        }
    }

    drawAnswers = function (qindex) {
        console.log('add answer is called');
        var noAnswer = $(".assessment-questions .question:nth-child(" + (qindex + 2) + ") .no-answer").val().trim();

        if (noAnswer.length == 0 || isNaN(noAnswer)) {

        } else {

            $(".assessment-questions .question:nth-child(" + (qindex + 2) + ") .answers").html("");

            noAnswer = parseInt(noAnswer);

            for (var i = 0; i < noAnswer; i++) {

                $(".assessment-questions .question:nth-child(" + (qindex + 2) + ") .answers").append(
                    '<div class="form-group" style="height: 34px;">' +
                        '<label for="4399526821' + qindex + '-' + i + '" class="col-sm-3 control-label" style="line-height: 34px;">Answer ' + (i + 1) + ':</label>' +
                        '<div class="col-sm-9">' +
                        '<input class="form-control author-name" id="4399526821' + qindex + '-' + i + '" type="text" onkeyup="drawAnswers(' + 59 + ');">' +
                        '</div>' +
                        '</div>'
                );
            }
        }
    }

    onContinueQuestionsClicked = function () {


    }

    closeHelp = function () {

        $(".help-area").css("display", "none");
        $(".as-builds").toggleClass("col-md-10");
        $(".as-builds").toggleClass("col-md-11");
        $(".open-help").css("display", "inline");
    }

    openHelp = function () {

        $(".open-help").css("display", "none");
        $(".as-builds").toggleClass("col-md-10");
        $(".as-builds").toggleClass("col-md-11");
        $(".help-area").css("display", "block");
    }

    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });

    $(document).ready(function () {

        $("input:checkbox").on('click', function () {
            // in the handler, 'this' refers to the box clicked on
            var $box = $(this);
            if ($box.is(":checked")) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                // the checked state of the group/box on the other hand will change
                // and the current value is retrieved using .prop() method
                $(group).prop("checked", false);
                $box.prop("checked", true);
            } else {
                $box.prop("checked", false);
            }
        });
    });

    switchCategoryControl = function (source) {
        console.log('switchCategory control -- '+$(source).prop("checked"));
        if ($(source).prop("checked")) {

            $(".x-category select, .x-category input, .y-category select, .y-category input").toggleClass("vs-hidden");
            updateXYCategory();
        } else {

            $(source).prop("checked", true);
        }
    }

    $(document).ready(function () {

        $(".assessment-details .as-helper input[type='text']").combotree();
        $(".assessment-details .as-helper .textbox.combo").css("width", "100%");
        $(".assessment-details .as-helper .textbox.combo").css("border-radius", "0");
        $(".assessment-details .as-helper .textbox.combo").css("border-color", "#bbb");
        $(".assessment-details .as-helper .textbox.combo").css("height", "34px");
        $(".assessment-details .as-helper .combo-arrow").css("height", "34px");
        $(".assessment-details .as-helper .textbox.combo .textbox-text").css("height", "34px");
        $(".assessment-details .as-helper .textbox.combo .textbox-text").css("font-size", "14px");

        $(".assessment-details .as-helper input[type='text']").combotree('tree').tree({

            onSelect: function (node) {

                $(".x-category select, .y-category select").html("");

                $.get("../../services/AssessmentService.php", {service_function: "getHelperNodes", parent_id: node.id, type_name: "Item"}, function (response, status) {

                    response = JSON.parse(response);

                    if (response.success_flag) {

                        for (var i = 0; i < response.helper_data.length; i++) {

                            $(".x-category select, .y-category select")
                                .append($("<option></option>")
                                    .attr("value", response.helper_data[i].id)
                                    .text(response.helper_data[i].name));
                        }

                        updateXYCategory();
                    }
                });

                updateXYCategory();
            }
        });
    });

    $(document).ready(function () {

        $.get("../../services/AssessmentService.php", {service_function: "getHelperNodes", type_name: "Category"}, function (response, status) {

            response = JSON.parse(response);

            if (response.success_flag) {

                for (var i = 0; i < response.helper_data.length; i++) {

                    if (response.helper_data[i].parent_id === null) {
                        $(".assessment-details .as-helper input[type='text']").combotree('tree').tree("append", {data: [
                            {id: response.helper_data[i].id, data: {parent_id: response.helper_data[i].parent_id, type_name: response.helper_data[i].type}, text: response.helper_data[i].name}
                        ]});
                        response.helper_data.splice(i, 1);
                        i = -1;
                    } else if ($(".assessment-details .as-helper input[type='text']").combotree('tree').tree("find", response.helper_data[i].parent_id) !== null) {

                        $(".assessment-details .as-helper input[type='text']").combotree('tree').tree("append", {parent: $(".assessment-details .as-helper input[type='text']").combotree('tree').tree("find", response.helper_data[i].parent_id).target, data: [
                            {id: response.helper_data[i].id, data: {parent_id: response.helper_data[i].parent_id, type_name: response.helper_data[i].type}, text: response.helper_data[i].name, icon: (response.helper_data[i].type === "Item" ? "../../resources/development/icons/stop-red.png" : true)}
                        ]});
                        response.helper_data.splice(i, 1);
                        i = -1;
                    }
                }
            }
        });
    });

    selectASStep = function (step_no) {
//    alert($("#account_type").val());
        console.log('step number ---- '+step_no);

        /*if (systemWorkMode === "Development") {

         $(".as-steps .as-step").removeClass("selected");
         $(".as-steps .as-step-" + step_no).addClass("selected");
         $(".as-builds .as-build").removeClass("active");
         $(".as-builds .as-build-" + step_no).addClass("active");
         return;
         }*/
        if(!validMatrixPoints) {
            $('.as-steps.as-main .as-step-6 .title').attr("data-original-title", "Error! <button id='popovercloseid' type='button' onclick=onPopUpClosed1(this) class='close' >&times;</button>");
            $('.as-steps.as-main .as-step-6 .title').attr("data-content", "Overlap or gap consist in matrix");

            $(".as-steps.as-main .as-step-6 .title").popover("show");
            return;
        }
        else{
            $(".as-steps.as-main .as-step-6 .title").popover("hide");
        }

        if (step_no == 1) {

//        alert(userModel.account_id);

            showVideo(1);

            $(".as-steps .as-step").removeClass("selected");
            $(".as-steps .as-step-" + step_no).addClass("selected");
            $(".as-builds .as-build").removeClass("active");
            $(".as-builds .as-build-" + step_no).addClass("active");
            return;

        }
        else if(step_no == 2) {
            var account_type = $('#account_type').val();
            var x_questions = '';
            var y_questions = '';

            if(account_type == 'free' || account_type == 'Koalafy Joey' || account_type =='Koalafy Eucalyptus' || account_type == 'Koalafy King') {
                x_questions = assessmentModel.questions["X"].length;
                y_questions = assessmentModel.questions["Y"].length;
                console.log('x questions -- '+x_questions);
                console.log('y questions -- '+y_questions);
                if(y_questions < 2 && x_questions < 2 ) {
                    alert('User should have number of questions 2 on X-axis or Y-axis');
                    return;
                }
                showVideo(4);




            }


        }

        else if(step_no == 5) {

            $(".assessment-save ul li").attr("onclick","showCollapsed(this)");
//            updateQuestionList();
//            getAssessmentData(false);
            updateQuestionList();
            loadDesignUL();
            loadResultData();

            //$('.page-area-child').css('overflow','auto');
            $('#ass-title').html(assessmentModel.details.name);
            showVideo(5);
        }
       else if(step_no == 7) {
            $(".as-steps .as-step").removeClass("selected");
            $(".as-steps .as-step-" + step_no).addClass("selected");
            $(".as-builds .as-build").removeClass("active");
            $(".as-builds .as-build-" + step_no).addClass("active");

        }
        /*else if(step_no == 7) {

         $(".as-steps .as-step").removeClass("selected");
         $(".as-steps .as-step-" + step_no).addClass("selected");
         $(".as-builds .as-build").removeClass("active");
         $(".as-builds .as-build-" + step_no).addClass("active");

         }*/

        validateAssessmentDetails(function (valid) {

            if (valid) {

                if(step_no == 6) {

                    var temp = true;

                    if(assessmentModel.questions["X"].length <= 0) {

                        $(".as-steps.as-main .as-step-3 .as-title").popover("show");
                        temp = false;
                    }

                    if(assessmentModel.questions["Y"].length <= 0) {

                        $(".as-steps.as-main .as-step-4 .as-title").popover("show");
                        temp = false;
                    }
                    /*if(!validateQuestionsScored("X")) {
                     //                    alert('hello');
                     $('.as-steps.as-main .as-step-2 .title').attr("data-original-title", "Error!<button id='popovercloseid' type='button' onclick=onPopUpClosed('title','') class='close' >&times;</button>");
                     $('.as-steps.as-main .as-step-2 .title').attr("data-content", "X axis scoring needs to be complete");
                     $(".as-steps.as-main .as-step-2 .title").popover("show");
                     return;

                     }

                     if(!validateQuestionsScored("Y")) {
                     $('.as-steps.as-main .as-step-2 .title').attr("data-original-title", "Error!<button id='popovercloseid' type='button' onclick=onPopUpClosed('title','') class='close' >&times;</button>");
                     $('.as-steps.as-main .as-step-2 .title').attr("data-content", "Y axis scoring needs to be complete");
                     $(".as-steps.as-main .as-step-2 .title").popover("show");
                     return;
                     }*/

                    if(!temp) {
                        return;
                    }
                    showVideo(6);


                } else if(step_no == 3) {

                    $(".as-steps.as-main .as-step-3 .as-title").popover("hide");
                    showVideo(2);



                } else if(step_no == 4) {

                    $(".as-steps.as-main .as-step-4 .as-title").popover("hide");
                    showVideo(3);

                }


                $(".as-steps .as-step").removeClass("selected");
                $(".as-steps .as-step-" + step_no).addClass("selected");
                $(".as-builds .as-build").removeClass("active");
                $(".as-builds .as-build-" + step_no).addClass("active");

            }
        });

    }

    wNumb = function(dec){

    }

    share_selected = function(id){
//        alert(ref.id);
        $('.share').prop('checked',false);
        $('#'+id).prop('checked',true);
    }

    share_submit = function() {
        var selected = $('input[name=share]:checked').val();
        //alert(selected);
        if(selected =='web_link') {
            selectASDelivery(2);
            saveAssessment(true,'none');
        }
        else if(selected =='social') {
            selectASDelivery(3);
        }
    }

    onSocialShare = function(id){
        $(".social_share").attr("checked",false);
        $("#"+id).attr("checked",true);
    }

    onSocialSubmit = function() {
        var id = $("input[name='social_share']:checked").val();

        if(id == 'facebook') {
            //alert('facebook');
            onSocialPopup("Post To Facebook","facebook");
        }
        else if(id == 'linkedin') {
            //alert('linkedin');
            onSocialPopup("Post To LinkedIn","linkedin");

        }
        else if(id == 'twitter') {
            //alert('twitter');
            onSocialPopup("Post To Twitter","twitter");
        }
    }

    onSocialPopup = function(title,type) {
        $(".modal-title").html("<span style='color:black;'>"+title+"</span>");
        $(".modal-header").css("border","none");
        $(".modal-title").css("border-bottom","solid 1px lightgrey");
        $(".modal-body").html('<div class="col-md-12 col-lg-12"><div class="col-md-3 col-lg-3" style="margin: 0px;padding:0px"><p>Headline </p></div>' +
            '<div class="col-md-9 col-lg-9" style="margin: 0px;padding:0px"><input type="text" id="share-title" style="width:100%"/></div></div>'+
        '<div class="col-md-12 col-lg-12"><div class="col-md-3 col-lg-3" style="margin: 0px;padding:0px"><p>Message </p></div>' +
            '<div class="col-md-9 col-lg-9" style="margin: 0px;padding:0px"><textarea type="text" id="share-message" style="width:100%;height: 100px;resize: none" /></div></div>');
        $(".modal-body").css("height","160px");
        $(".modal-footer").css("display","block");
        $(".modal-footer").html('<div class="col-md-12 col-lg-12" style="margin-top: 10px"><button class="btn btn-default" onclick="onSharePopupCancel()">Cancel</button><button class="btn btn-primary" onclick= onShareNext("'+type+'") >Next</button></div>');
        $("#myModal").modal("show");

    }

    onSharePopupCancel = function() {
        $("#myModal").modal("hide");

    }

    onShareNext = function(typee) {
        var title = $("#share-title").val();
        var message = $("#share-message").val();
        if(title == '' || message == '') {
            alert ('Please enter all fields');
            return;
        }
        //console.log(typee);
        if(typee == 'facebook') {
            onSharePopupCancel();
          var fb_value = $("#fb_val").val();
          console.log('fb value here -- '+fb_value);
            if(fb_value!="") {
                /*FB.ui({
                    method: 'feed',
                    name: 'Koalafy.me | Assessment',
                    link: 'http://koalafy.me',
                    picture: 'http://koalafy.me/resources/development/site/site-logo.png',
                    caption: "Share Assessment",
                    description: message,
                    message: title
                }, function (response) {
                    console.log(JSON.stringify(response));
                });*/
                window.location = '../my-account/fbconfig.php?post=1&state=assessment&title='+title+"&message="+message;
            }
            else{
                window.location = '../my-account/fbconfig.php?state=assessment';
            }


      }
      else if(typee == 'twitter') {
            onSharePopupCancel();
            window.open("https://twitter.com/share?url=http%3A%2F%2Fwww.koalafy.me" +
                "&related=twitterapi%2Ctwitter&hashtags="+title +
                "&text="+message, "MsgWindow", "width=800,height=600");

        }
      else if(typee == 'linkedin') {
            onSharePopupCancel();
            window.open("https://www.linkedin.com/shareArticle?mini=true&url=http://koalafy.me&title="+title+"&summary="+message+"&source=http://koalafy.me", "MsgWindow", "width=800,height=600");
      }
    }
    loadDesignUL = function() {

        var template_id = assessmentModel.template.temp_id;
        if(template_id == '')
            themeSelected(parseInt(0),false);
        else
            themeSelected(parseInt(template_id-1),false);
        // set data for setting & description

        if(assessmentModel.setting.setting_is_back =='A') {
            $("#back").attr("checked",true);
            $('#page_back').css('display','block');
        }
        else{
            $("#back").attr("checked",false);
            $('#page_back').css('display','none');
        }
        if(assessmentModel.setting.setting_is_question == 'A') {
            $("#s_question").attr("checked",true);
        }
        else{
            $("#s_question").attr("checked",false);
        }

        if(assessmentModel.setting.setting_is_header == 'A') {
            $("#s_title").attr("checked",true);
            $('.assessment-page-title').css('display','block');
        }
        else{
            $("#s_title").attr("checked",false);
            $('.assessment-page-title').css('display','none');
        }

        if(assessmentModel.setting.setting_is_desc == 'A') {
            $("#s_desc").attr("checked",true);
            $('#description').css('display','block');

        }
        else{
            $("#s_desc").attr("checked",false);
            $('#description').css('display','none');
        }

        $('#back_text').val(assessmentModel.setting.setting_back_text);
        $('#next_text').val(assessmentModel.setting.setting_next_text);
        $('#desc').val(assessmentModel.setting.setting_desc_text);
        $('#font-style-desc').val(assessmentModel.setting.setting_font);
        $('#font-size-desc').val(''+assessmentModel.setting.setting_text_size);
        $('#font-align-desc').val(assessmentModel.setting.setting_alignment);
        $('#page_next').val(assessmentModel.setting.setting_next_text);

        $('#description').html(assessmentModel.setting.setting_desc_text);
        $('#description').css('font-family',assessmentModel.setting.setting_font);
        $('#description').css('font-size',assessmentModel.setting.setting_text_size+'px');
        $('#description').css('color',assessmentModel.setting.setting_color);

        if(assessmentModel.setting.setting_question_limit ==''){
            $("#question-range").val('5');
        }
        else{
            $("#question-range").val(assessmentModel.setting.setting_question_limit);
        }
        updateTextStyle('description','font-align-desc');

        // set data for title area

        $('#font-style').val(assessmentModel.title.title_font);
        $('#font-size').val(assessmentModel.title.title_size);
        $('#font-align').val(assessmentModel.title.title_alignment);
        //alert(assessmentModel.title.title_alignment);
        if(assessmentModel.title.title_background!='')
            $('.assessment-page-title').css('background',assessmentModel.title.title_background);
        $('.assessment-page-title').css('height',assessmentModel.title.title_height+'px');
        $('#ass-title').css('font-family',assessmentModel.title.title_font);
        $('#ass-title').css('font-size',assessmentModel.title.title_size+'px');
        $('#ass-title').css('color',assessmentModel.title.title_color);

        //alert(assessmentModel.title.title_background);
        setTimeout(function(){

            updateTextStyle('ass-title','font-align');
        },500);

        // set data for logo

        if(assessmentModel.logo.logo_path === 'profile-pic.png' || assessmentModel.logo.logo_path === '') {
            document.getElementById("logo_img").src = "images/profile-pic.png";
            document.getElementById("ass-logo").src = "images/profile-pic.png";
        }
        else{
            document.getElementById("logo_img").src = "data:image/png;base64,"+assessmentModel.logo.logo_path;
            document.getElementById("ass-logo").src = "data:image/png;base64,"+assessmentModel.logo.logo_path;
        }
        $('#ass-logo').css('height',assessmentModel.logo.logo_height);
        $('#ass-logo').css('width',assessmentModel.logo.logo_width);
        $('#logo-position').val(assessmentModel.logo.logo_position);
        $('#logo-size').val(assessmentModel.logo.logo_size);

        logo_position('logo-position');
        logo_scale('logo-size');


        if(parseInt(assessmentModel.logo.logo_height)>170) {
            $('.assessment-page-title').css('height',assessmentModel.title.logo_height);
        }

        // set data for background
        $('.design-innerr').css('background',assessmentModel.background.color);

        // set data for page

        if(assessmentModel.page.page_is_font) {
            $('#page-font').attr('checked',true);
        }
        else{
            $('#page-background').attr('checked',true);
        }
        if(assessmentModel.page.page_is_progress === 'A') {
            $('.progress').css('display','block');
            $('#progress').attr('checked',true);
        }
        else{
            $('.progress').css('display','none');
            $('#progress').attr('checked',false);
        }

        if(assessmentModel.page.page_is_top === 'A') {
            $('#progress_top').attr('checked',true);
            progressChecked('progress_top');
        }
        else{
            $('#progress_top').attr('checked',false);
            progressChecked('progress_bottom');
        }
        if(assessmentModel.page.page_back_color!=''){
            $('.page-area-child').css('background',assessmentModel.page.page_back_color);
            $('#description_d').css('background',assessmentModel.page.page_back_color);
        }

        $('#page-text').css('color',assessmentModel.page.page_text_color);
        // set data for footer
        $('#font-stylee').val(assessmentModel.footer.footer_font);
        $('#font-sizee').val(assessmentModel.footer.footer_text_size);
        $('#font-alignn').val(assessmentModel.footer.footer_text_align);
        updateTextStyle('assessment-footer','font-alignn');
        if(assessmentModel.footer.footer_back_color!='')
            $('.assessment-footer').css('background-color',''+assessmentModel.footer.footer_back_color);
        $('.assessment-footer').css('color',''+assessmentModel.footer.footer_text_color);
        $('.assessment-footer').css('height',assessmentModel.footer.footer_height);
        $('#page_footer').css('font-family',''+assessmentModel.footer.footer_font);
        $('#page_footer').css('font-size',''+assessmentModel.footer.footer_text_size+'px');

        // set data for footer
        if(assessmentModel.layout.layout_selected!='')
            $('#'+assessmentModel.layout.layout_selected).attr('checked',true);
        $('#page_width').val(''+assessmentModel.layout.layout_page_width);
        $('#page_content_width').val(''+assessmentModel.layout.layout_content_width);

        var width = (parseInt($("#page_width").val())-22);
        var page_content_width = (parseInt($("#page_content_width").val())+20);

        $('#design-innerr').css('width',width+'%');
        $('#main-contentt').css('width',page_content_width+'%');

        if(assessmentModel.popup.popup_title!="") {
            if(assessmentModel.popup.popup_display == 'A') {
                $('.confirm').attr('checked',false);
                $('#yes').attr('checked',true);
            }
            else{
                $('.confirm').attr('checked',false);
                $('#no').attr('checked',true);

            }
            $('#popup-title').val(assessmentModel.popup.popup_title);
            $('#first_name').val(assessmentModel.popup.popup_firstname_label);
            $('#last_name').val(assessmentModel.popup.popup_lastname_label);
            $('#full_name').val(assessmentModel.popup.popup_fullname_label);
            $('#email').val(assessmentModel.popup.popup_email_label);
            $('#company').val(assessmentModel.popup.popup_company_label);
            $('#phone').val(assessmentModel.popup.popup_phone_label);

            if(assessmentModel.popup.popup_firstname_display =='A') {
                $('#first_name_chk').attr("checked",true);
            }
            else{
                $('#first_name_chk').attr("checked",false);
            }

            if(assessmentModel.popup.popup_lastname_display =='A') {
                $('#last_name_chk').attr("checked",true);
            }
            else{
                $('#last_name_chk').attr("checked",false);
            }

            if(assessmentModel.popup.popup_fullname_display =='A') {
                $('#full_name_chk').attr("checked",true);
            }
            else{
                $('#full_name_chk').attr("checked",false);
            }

            if(assessmentModel.popup.popup_email_display =='A') {
                $('#email_chk').attr("checked",true);
            }
            else{
                $('#email_chk').attr("checked",false);
            }

            if(assessmentModel.popup.popup_company_display =='A') {
                $('#company_chk').attr("checked",true);
            }
            else{
                $('#company_chk').attr("checked",false);
            }




            if(assessmentModel.popup.popup_firstname_required =='A') {
                $('#first_name_req').attr("checked",true);
            }
            else{
                $('#first_name_req').attr("checked",false);
            }
            if(assessmentModel.popup.popup_lastname_required =='A') {
                $('#last_name_req').attr("checked",true);
            }
            else{
                $('#last_name_req').attr("checked",false);
            }

            if(assessmentModel.popup.popup_fullname_required =='A') {
                $('#full_name_req').attr("checked",true);
            }
            else{
                $('#full_name_req').attr("checked",false);
            }

            if(assessmentModel.popup.popup_email_required =='A') {
                $('#email_req').attr("checked",true);
            }
            else{
                $('#email_req').attr("checked",false);
            }

            if(assessmentModel.popup.popup_company_required =='A') {
                $('#company_req').attr("checked",true);
            }
            else{
                $('#company_req').attr("checked",false);
            }
            //alert(assessmentModel.footer.footer_text);
            if(assessmentModel.footer.footer_text!="")
            $("#page_footer").html(assessmentModel.footer.footer_text);




        }
    }

    showCollapsed = function(ref){
//    alert('function called');
//    $('.assessment-save ul #first li ul').toggle();
//    $('.assessment-save ul li').children('ul.collapse').collapse({toggle: false}).collapse('hide');
        var idd = $(ref).children("ul.collapse").attr("id");

        $('.assessment-save ul li ul').each(function() {

            if($(this).is(":visible") && $(this).attr("id") != idd ){
                $(this).toggle();
            }
            else{
//            console.log('i m here');
                $('.assessment-save ul li').children('label').html('+');
            }
        });
        $(ref).children('ul.collapse').toggle();

        $('.assessment-save ul li ul').each(function() {

            if($(this).attr("id") === idd){
                if($(this).is(":visible") ) {
                    $(ref).children('label').html('-');
                }
                else{
                    $(ref).children('label').html('+');
                }

            }
//      alert(idd);


        });

        if(idd == "nineth") {
            console.log('i m in if');
//        insertLayoutQuestions('X',0,5,0);
        }

    }

    onPopupVisible = function(id) {
        //alert(id);
        $('.confirm').prop('checked',false);
        $('#'+id).prop('checked',true);
        if($('#yes').is(':checked')) {
            assessmentModel.popup.popup_display = 'A';
        }
        else{
            assessmentModel.popup.popup_display = 'D';
        }
        saveAssessment(true,'none');
    }

    onFieldUpdate = function(id) {

        switch(id) {
            case 'first_name_chk':
                if($('#'+id).is(':checked')) {
                    assessmentModel.popup.popup_firstname_display ='A';
                }
                else{
                    assessmentModel.popup.popup_firstname_display ='D';
                }
                break;
            case 'last_name_chk':
                if($('#'+id).is(':checked')) {
                    assessmentModel.popup.popup_lastname_display ='A';
                }
                else{
                    assessmentModel.popup.popup_lastname_display ='D';
                }
                break;
            case 'email_chk':
                if($('#'+id).is(':checked')) {
                    assessmentModel.popup.popup_email_display ='A';
                }
                else{
                    assessmentModel.popup.popup_email_display ='D';
                }
                break;
            case 'full_name_chk':
                if($('#'+id).is(':checked')) {
                    assessmentModel.popup.popup_fullname_display ='A';
                }
                else{
                    assessmentModel.popup.popup_fullname_display ='D';
                }
                break;
            case 'phone_chk':
                if($('#'+id).is(':checked')) {
                    assessmentModel.popup.popup_phone_display ='A';
                }
                else{
                    assessmentModel.popup.popup_phone_display ='D';
                }
                break;
            case 'company_chk':
                if($('#'+id).is(':checked')) {
                    assessmentModel.popup.popup_company_display ='A';
                }
                else{
                    assessmentModel.popup.popup_company_display ='D';
                }
                break;
            default:
                console.log('No case found');
        }
        saveAssessment(true,'none');
    }

    onRequiredUpdate = function(id) {
        alert(id);
        switch(id) {
            case 'first_name_req':
                if($('#'+id).is(':checked')) {
                    assessmentModel.popup.popup_firstname_required ='A';
                }
                else{
                    assessmentModel.popup.popup_firstname_required ='D';
                }
                break;
            case 'last_name_req':
                if($('#'+id).is(':checked')) {
                    assessmentModel.popup.popup_lastname_required ='A';
                }
                else{
                    assessmentModel.popup.popup_lastname_required ='D';
                }
                break;
            case 'email_req':
                if($('#'+id).is(':checked')) {
                    assessmentModel.popup.popup_email_required ='A';
                }
                else{
                    assessmentModel.popup.popup_email_required ='D';
                }
                break;
            case 'full_name_req':
                if($('#'+id).is(':checked')) {
                    assessmentModel.popup.popup_fullname_required ='A';
                }
                else{
                    assessmentModel.popup.popup_fullname_required ='D';
                }
                break;
            case 'phone_req':
                if($('#'+id).is(':checked')) {
                    assessmentModel.popup.popup_phone_required ='A';
                }
                else{
                    assessmentModel.popup.popup_phone_required ='D';
                }
                break;
            case 'company_req':
                if($('#'+id).is(':checked')) {
                    assessmentModel.popup.popup_company_required ='A';
                }
                else{
                    assessmentModel.popup.popup_company_required ='D';
                }
                break;
            default:
                console.log('No case found');
        }
        saveAssessment(true,'none');
    }

    onAssessmentTyped = function(id) {
        var value = $('#'+id).val();
        switch(id) {
            case 'popup-title':
                assessmentModel.popup.popup_title = ''+value;
                break;
            case 'first_name':
                assessmentModel.popup.popup_firstname_label = ''+value;
                break;
            case 'last_name':
                assessmentModel.popup.popup_lastname_label = ''+value;
                break;
            case 'email':
                assessmentModel.popup.popup_email_label = ''+value;
                break;
            case 'full_name':
                assessmentModel.popup.popup_fullname_label = ''+value;
                break;
            case 'phone':
                assessmentModel.popup.popup_phone_label = ''+value;
                break;
            case 'company':
                assessmentModel.popup.popup_company_label = ''+value;
                break;
            default:
                console.log('invalid value found');

        }
        assessmentModel.popup.popup_title = ''+value;
        saveAssessment(true,'none');

    }

    insertLayoutQuestions1 = function (index,limit,loaded) {
        var total_questions = assessmentModel.questions["X"].concat(assessmentModel.questions["Y"]);
        total_questions.sort(function(a, b) {
            return parseInt(a.order) - parseInt(b.order);
        });
        var questionHtml = "";
        var width = (parseInt($("#page_width").val())-22);
        var page_content_width = (parseInt($("#page_content_width").val())+20);

        var items = updateHtmlData1(total_questions,index,limit);
        index = items.index;
        loaded = items.loaded;
        questionHtml = questionHtml+items.html;

        if( questionHtml!="</div>" ) {
            $('.page-area-child').html(questionHtml+'<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 " style="margin-top: 5px;">'+
                '<div class="col-md-6 col-xs-6 col-sm-6 col-lg-6" style="margin:0px;padding:0px;text-align:left"><button class="btn btn-default btn-primary" style="color:white"  id="page_back">Back</button>'+
                '</div><div class="col-md-6 col-xs-6 col-sm-6 col-lg-6" style="margin:0px;padding:0px;">'+
                '<button class="btn btn-default btn-primary pull-right " style="color:white" id="page_next">Next</button> </div></div>');
//              $(".assessment-desc").css("width",width);
        }
        else{
            console.log('i m in else case');
        }
        //alert(page_widthh);
        $("#design-inner").css("width",width+'%');
        $("#main-contentt").css("width",page_content_width+'%');

        //$('.page-area-child').css('overflow','scroll');
        //$('.page-area-child').css('overflow-x','hidden');

        //$('.question-header').css('background-color',assessmentModel.page.page_back_color);
        $('.question-text').css('color',assessmentModel.page.page_text_color);
        $('.answer-text').css('color',assessmentModel.page.page_text_color);
        //$(".answer-text").css('background-color',assessmentModel.page.page_back_color);


//     alert(questionHtml);


        $('#page_next').attr("onclick","insertLayoutQuestions1('"+index+"','"+limit+"','"+loaded+"')");
        var back_index = 0;

        console.log('loaded here -- '+loaded);
        if(loaded<limit && index>loaded) {
            var limit_t = parseInt(loaded)+parseInt(limit);
            back_index = index-limit_t;
            console.log('back index0 '+back_index +" index:"+index+" loaded:"+loaded +" limit "+limit+" limit_t "+limit_t);
        }
        else if(loaded == limit ) {
            back_index = parseInt(index)-parseInt(loaded);
            console.log('back index1 '+back_index);
        }
        else if(loaded<limit && index<loaded) {
            back_index = parseInt(index)-parseInt(loaded);
            console.log('back index2 '+back_index);
        }
        if(back_index>=0){
            if( index<=limit) {
                $('#page_back').attr("onclick","insertLayoutQuestions1(,'0','"+limit+"','"+loaded+"')");
            }
            else{
                $('#page_back').attr("onclick","insertLayoutQuestions1('"+back_index+"','"+limit+"','"+loaded+"')");
            }

        }
        else{
            $('#page_back').attr("onclick","insertLayoutQuestions('0','"+limit+"','"+loaded+"')");
        }
        if($("#s_question").is(":checked")) {
            assessmentModel.setting.setting_is_question = 'A';
        }
        else{
            assessmentModel.setting.setting_is_question = 'D';
        }

        if(total_questions.length == index) {
            $('#page_next').text("Submit");
            $('#page_next').attr("onclick","onSubmitAssessment()");

        }

    }

    onSubmitAssessment = function() {
        //alert('assessment submited');
        if(assessmentModel.popup.popup_display == 'A'){
        if(assessmentModel.popup.popup_title!="") {
            $("#modal_title").html("<p style='color:"+assessmentModel.footer.footer_text_color+"'>"+assessmentModel.popup.popup_title+"</p>");
            $("#modal_title").css("border","none");
            $("#modal_header").css("background-color",assessmentModel.footer.footer_back_color);
            $("#modal_body").css('background-color',assessmentModel.title.title_background);
            $("#modal_body").css('display','flex');
            $("#modal_body").css('display','-webkit-flex');
            $("#modal_body").css('flex-direction','column');
            $("#modal_body").css('-webkit-flex-direction','column');
            $("#modal_footer").css("display","none");

            $("#modal_body").html(getModalBody());
//            alert(assessmentModel.footer.footer_back_color);

          }
          $("#myModal").modal("show");
        }
        else{

            assessmentUserModel.assessment_id = assessmentModel.id;
            assessmentUserModel.popup_display = assessmentModel.popup.popup_display;
            saveAssessmentUsers();

//            alert('Assessment uploaded successfully');
        }


    }

    getModalBody = function(){
        var modal_body = '';

        if(assessmentModel.popup.popup_firstname_display =='A')
        {
          if(assessmentModel.popup.popup_firstname_required =='A') {
              modal_body = modal_body+'<div class="col-md-12"><div class="col-md-4"><p style="color:'+assessmentModel.page.page_text_color+'">First Name *</p></div><div class="col-md-8"><input type="text" id="first_namee" style="width:100%"/></div></div>';
          }
          else{
              modal_body = modal_body+'<div class="col-md-12"><div class="col-md-4"><p style="color:'+assessmentModel.page.page_text_color+'">First Name</p></div><div class="col-md-8"><input type="text" id="first_namee" style="width:100%"/></div></div>';

          }
        }

        if(assessmentModel.popup.popup_lastname_display =='A')
        {
            if(assessmentModel.popup.popup_lastname_required =='A') {
                modal_body = modal_body+'<div class="col-md-12"><div class="col-md-4"><p style="color:'+assessmentModel.page.page_text_color+'">Last Name *</p></div><div class="col-md-8"><input type="text" id="last_namee" style="width:100%"/></div></div>';
            }
            else{
                modal_body = modal_body+'<div class="col-md-12"><div class="col-md-4"><p style="color:'+assessmentModel.page.page_text_color+'">Last Name</p></div><div class="col-md-8"><input type="text" id="last_namee" style="width:100%"/></div></div>';

            }
        }

        if(assessmentModel.popup.popup_fullname_display =='A')
        {
            if(assessmentModel.popup.popup_fullname_required =='A') {
                modal_body = modal_body+'<div class="col-md-12"><div class="col-md-4"><p style="color:'+assessmentModel.page.page_text_color+'">Full Name *</p></div><div class="col-md-8"><input type="text" id="full_namee" style="width:100%"/></div></div>';
            }
            else{
                modal_body = modal_body+'<div class="col-md-12"><div class="col-md-4"><p style="color:'+assessmentModel.page.page_text_color+'">Full Name</p></div><div class="col-md-8"><input type="text" id="full_namee" style="width:100%"/></div></div>';
            }
        }

        if(assessmentModel.popup.popup_email_display =='A')
        {
            if(assessmentModel.popup.popup_email_required =='A') {
                modal_body = modal_body+'<div class="col-md-12"><div class="col-md-4"><p style="color:'+assessmentModel.page.page_text_color+'">Email *</p></div><div class="col-md-8"><input type="text" id="emaill" style="width:100%"/></div></div>';
            }
            else{
                modal_body = modal_body+'<div class="col-md-12"><div class="col-md-4"><p style="color:'+assessmentModel.page.page_text_color+'">Email</p></div><div class="col-md-8"><input type="text" id="emaill" style="width:100%"/></div></div>';
            }
        }

        if(assessmentModel.popup.popup_company_display =='A')
        {
            if(assessmentModel.popup.popup_company_required =='A') {
                modal_body = modal_body+'<div class="col-md-12"><div class="col-md-4"><p style="color:'+assessmentModel.page.page_text_color+'">Company *</p></div><div class="col-md-8"><input type="text" id="companyy" style="width:100%"/></div></div>';
            }
            else{
                modal_body = modal_body+'<div class="col-md-12"><div class="col-md-4"><p style="color:'+assessmentModel.page.page_text_color+'">Company</p></div><div class="col-md-8"><input type="text" id="companyy" style="width:100%"/></div></div>';
            }
        }

        if(assessmentModel.popup.popup_phone_display =='A')
        {
            if(assessmentModel.popup.popup_phone_required =='A') {
                modal_body = modal_body+'<div class="col-md-12"><div class="col-md-4"><p style="color:'+assessmentModel.page.page_text_color+'">Phone *</p></div><div class="col-md-8"><input type="text" id="phonee" style="width:100%"/></div></div>';
            }
            else{
                modal_body = modal_body+'<div class="col-md-12"><div class="col-md-4"><p style="color:'+assessmentModel.page.page_text_color+'">Phone</p></div><div class="col-md-8"><input type="text" id="phonee" style="width:100%"/></div></div>';
            }
        }
        modal_body = modal_body+'<div class="col-md-12"><div class="col-md-4"></div><div class="col-md-8"><p style="color:'+assessmentModel.page.page_text_color+'">*= Required Field</p></div></div>';

        modal_body = modal_body+'<div class="col-md-12 " style="text-align:center;margin-top:5%"><Button class="btn btn-default" onclick="onAssessmentUpload()">Submit</Button><span id="result" style="margin-left:1%;color:'+assessmentModel.page.page_text_color+';font-weight:bold"></span></div>';

        return modal_body;
    }

     onAssessmentUpload = function() {

//     alert('assessment uploaded succesfully');
         var first_name = $("#first_namee").val();
         var last_name = $("#last_namee").val();
         var full_name = $("#full_namee").val();
         var email = $("#emaill").val();
         var phone = $("#phonee").val();
         var company = $("#companyy").val();
         console.log(first_name);
         console.log(last_name);
         console.log(email);
         console.log(phone);
         console.log(company);


         if(assessmentModel.popup.popup_firstname_display =='A' && assessmentModel.popup.popup_firstname_required =='A' && first_name == '') {
           $('#result').html('Please enter first name');

            return;
         }
         if(assessmentModel.popup.popup_lastname_display =='A' && assessmentModel.popup.popup_lastname_required =='A' && last_name == '') {
             $('#result').html('Please enter last name');
             return;
         }
         if(assessmentModel.popup.popup_email_display =='A' && assessmentModel.popup.popup_email_required =='A' && email == '') {
             $('#result').html('Please enter email-id');
             return;
         }
         if(assessmentModel.popup.popup_phone_display =='A' && assessmentModel.popup.popup_phone_required =='A' && phone == '') {
             $('#result').html('Please enter mobile number');
             return;
         }
         if(assessmentModel.popup.popup_company_display =='A' && assessmentModel.popup.popup_company_required =='A' && company == '') {
             $('#result').html('Please enter company name');
             return;
         }

         if(assessmentModel.popup.popup_email_display =='A' && assessmentModel.popup.popup_email_required =='A' && !isEmailAddress(email)) {
             $('#result').html('Please enter valid email-id');
              return;
         }

         if(assessmentModel.popup.popup_phone_display =='A' && assessmentModel.popup.popup_phone_required =='A' &&!validatePhone(phone)) {
             $('#result').html('Please enter valid mobile number');
             return;
         }
         assessmentUserModel.assessment_id = assessmentModel.id;
         assessmentUserModel.popup_display = assessmentModel.popup.popup_display;
         if(first_name)
           assessmentUserModel.first_name = first_name;
         if(last_name)
           assessmentUserModel.last_name = last_name;
         if(full_name){
             assessmentUserModel.full_name = full_name;
         }
         if(email)
           assessmentUserModel.email = email;
         if(phone)
           assessmentUserModel.phone = phone;
         if(company)
           assessmentUserModel.company = company;

         assessmentUserModel.first_name_required = assessmentModel.popup.popup_firstname_required;
         assessmentUserModel.last_name_required = assessmentModel.popup.popup_lastname_required;
         assessmentUserModel.full_name_required = assessmentModel.popup.popup_fullname_required;
         assessmentUserModel.email_required = assessmentModel.popup.popup_email_required;
         assessmentUserModel.company_required = assessmentModel.popup.popup_company_required;
         assessmentUserModel.phone_required = assessmentModel.popup.popup_phone_required;


         assessmentUserModel.first_name_display = assessmentModel.popup.popup_firstname_display;
         assessmentUserModel.last_name_display = assessmentModel.popup.popup_lastname_display;
         assessmentUserModel.full_name_display = assessmentModel.popup.popup_fullname_display;
         assessmentUserModel.email_display = assessmentModel.popup.popup_email_display;
         assessmentUserModel.company_display = assessmentModel.popup.popup_company_display;
         assessmentUserModel.phone_display = assessmentModel.popup.popup_phone_display;

         saveAssessmentUsers();
         $('#myModal').modal('hide');


     }

    function isEmailAddress(str) {
        var pattern =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return pattern.test(str);  // returns a boolean
    }

    function validatePhone(phone) {
        if (phone.match(/^\d{10}/)) {
            return true;
        }
        return false;
    }

    onResultTypeChecked = function(id) {
        console.log('id result type '+id);
        if(id =='redirect' || id === 'redirectt') {
            selectASResults(1);
            $("#eleven").css("height","32%");
            console.log('i am in if');
            assessmentModel.result.result_isRedirect = 'A';

        }
        else if(id =='message' || id === 'messagee') {
            selectASResults(2);
            $("#eleven").css("height","55%");

            assessmentModel.result.result_isRedirect = 'D';
        }
        saveAssessment(true,'none');
    }

    onFooterTyped = function() {
      var footer_txt = $("#page_footerr").val();
      $("#page_footer").html(footer_txt);
      assessmentModel.footer.footer_text = footer_txt;
      saveAssessment(true,'none');

    }

    selectASResults = function(step_no) {
     $("#eleven >li> .as-resultss ").removeClass("active");
     $("#eleven >li> .as-resultss.as-resultss-"+step_no).addClass("active");

     if(step_no == 1) {
         $("#messagee").prop("checked",true);
         $("#redirectt").prop("checked",false);
         $("#redirect").prop("checked",true);
     }
      else{
         $("#redirect").prop("checked",false);
         $("#message").prop("checked",false);
       }
   }

    updateHtmlData1 = function(questionModel,index,limit) {
        var scoreHtml = "";
        var counter = 0,counter_1= 0;
        var isFound = true;

        for(var i = index;i<questionModel.length;i++ ) {
            counter = i;
//            counter_index =i;
            console.log("counter_1  here -- "+counter_1);

            if( counter_1>=limit ) {
//            axis = "X";
                break;
            }



            if (questionModel[i].type === "Dropdown") {

                var optionsHtml = "<select style='width:100%' class='answer-text'>";

                for (var j = 0; j < questionModel[i].answers.length; j++) {

                    optionsHtml += "<option class='as-score-answer as-score-answer-" + i + "' value='" + questionModel[i].answers[j].id + "'>" + questionModel[i].answers[j].text + "</option>";
                }
//        console.log('min score -- '+questionModel.min);
                /*for(var i = 0;i<assessmentModel.questions['X'].length;i++) {
                 console.log('scores point-- '+assessmentModel.questions['X'][i].text);
                 console.log('min -- '+assessmentModel.questions['X'][i].min);
                 console.log('max -- '+assessmentModel.questions['X'][i].max);

                 }*/
                var q_number = '';
                if($("#s_question").is(":checked")) {
                    var q_index = parseInt(i)+1;
                    q_number = q_index+' . ';

                }
                scoreHtml = scoreHtml+'<div class="col-xs-12  as-score-question as-score-question-' + index + '" style="margin-bottom: 9px; border-radius: 5px;padding-bottom:5px">' +
                    '<p class="col-xs-12 question-text" style="padding:0px;text-align: left;word-wrap: break-word">' + q_number  + questionModel[i].text +'</p>' +
                    '<div class="col-xs-12 as-score-answers" style="padding: 0;margin-top: 5px" >' + optionsHtml + '</select></div>' +
                    '</div>';
            } else if (questionModel[i].type === "Button") {

                var radiosHtml = "";

                for (var j = 0; j < questionModel[i].answers.length; j++) {

                    radiosHtml += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 as-score-answer as-score-answer-" + i + "' style='padding: 0;margin-top: 5px;text-align:left'>" +
                        "<input type='radio' name='answers-" + i + "' />" +
                        "<span class='answer-text'>&nbsp;&nbsp;" + questionModel[i].answers[j].text + "</span>" +
                        "</div>";
                }
                var q_number = '';
                if($("#s_question").is(":checked")) {
                    q_index = parseInt(i)+1;
                    q_number = q_index+' . ';
                }
                scoreHtml = scoreHtml+'<div class="col-xs-12 as-score-question  as-score-question-' + index + '" style="margin-bottom: 9px; border-radius: 5px;padding-bottom:5px">' +
                    '<p class="col-xs-12 question-text" style="padding:0px;text-align: left;word-wrap: break-word">' + q_number  + questionModel[i].text +'</p>' +
                    '<div class="col-xs-12 as-score-answers" style="padding: 0;">' + radiosHtml + '</div>' +
                    '</div>';
            }
            counter++;
            counter_1++;

        }
        console.log('counter here -- '+counter+' length here -- '+questionModel.length);
        console.log('score html -- '+scoreHtml);
        scoreHtml = scoreHtml+'</div>';

//    $('.page-area-child').append(scoreHtml+'</div><div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 " style="margin-top: 5px;">'+
//        '<div class="col-md-6 col-xs-6 col-sm-6 col-lg-6" style="margin:0px;padding:0px;text-align:left"><button class="btn btn-default btn-primary" style="color:white"  id="page_back">Back</button>'+
//        '</div><div class="col-md-6 col-xs-6 col-sm-6 col-lg-6" style="margin:0px;padding:0px;">'+
//        '<button class="btn btn-default btn-primary pull-right " style="color:white" id="page_next">Next</button> </div></div>');


        var data = {};
        data.index = counter;
        data.html = scoreHtml;
        data.loaded = counter_1;
        return data;
    }

    onTextCopy = function(){
        //$("#web-link").focus();
        $('#web-link').attr('disabled',false);
        $("#web-link").select();
        var successful = document.execCommand('copy');
        $('#web-link').attr('disabled',true);
        //alert(successful);
    }

    onQuestionRange = function(id){
        assessmentModel.setting.setting_question_limit = $('#'+id).val();
        saveAssessment(true,'none');
    }

    onQuestionChecked = function() {
        if($("#s_question").is(":checked")) {
            assessmentModel.setting.setting_is_question = 'A';
        }
        else{
            assessmentModel.setting.setting_is_question = 'D';
        }
        saveAssessment(true,'none');

    }

    insertLayoutQuestions = function(axis,index,limit,loaded,counter_index) {
        var questionModel = assessmentModel.questions[axis];
//   var questionHtml = "<div class='col-xs-12 col-md-12 col-lg-12 col-sm-12 as-content scored' style='text-align: left; margin: 2px; padding: 8px;height:320px'>";
        var questionHtml = "";
        var items = {};
        var isFound = true;
        var page_widthh = parseInt($('#design-inner').width());
        var width = (parseInt($("#page_width").val())-22);
        var page_content_width = (parseInt($("#page_content_width").val())+20);
//        alert(counter_index);

        if(axis == "X" ) {   // step 1
            console.log("question if X < case here");
            if(index == questionModel.length) {
                axis = "Y";
                index = 0;
                loaded = 0;
                isFound = false;
            }
            else{
                items = updateHtmlData(questionModel,index,limit,axis,counter_index);
                axis = items.axis;
                index = items.index;
                loaded = items.loaded;
                counter_index = parseInt(items.counter_index);
                questionHtml = questionHtml+items.html;
            }
            //alert(page_widthh);
            if( questionHtml!="</div>" ) {
                $('.page-area-child').html(questionHtml+'<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 " style="margin-top: 5px;">'+
                    '<div class="col-md-6 col-xs-6 col-sm-6 col-lg-6" style="margin:0px;padding:0px;text-align:left"><button class="btn btn-default btn-primary" style="color:white"  id="page_back">Back</button>'+
                    '</div><div class="col-md-6 col-xs-6 col-sm-6 col-lg-6" style="margin:0px;padding:0px;">'+
                    '<button class="btn btn-default btn-primary pull-right" style="color:white" id="page_next">Next</button> </div></div>');
//              $(".assessment-desc").css("width",width);
            }
            else{
                console.log('i m in else case');
            }
            //alert(page_widthh);
            $("#design-inner").css("width",width+'%');
            $("#main-contentt").css("width",page_content_width+'%');

//         questionHtml = items.html;
//       console.log(questionHtml);

        }

        if(axis == "Y" ) {   // step 3
            console.log('question if Y < case here' );
            items = updateHtmlData(questionModel,index,limit,axis,counter_index);

            axis = items.axis;
            index = items.index;
            loaded = items.loaded;
            counter_index = parseInt(items.counter_index);
            questionHtml = items.html;

//         questionHtml = questionHtml + items.html;
//       console.log(questionHtml);
//         $('.page-area-child').html(questionHtml);
            if( questionHtml!="</div>" ) {
                $('.page-area-child').html(questionHtml+'<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 " style="margin-top: 5px;">'+
                    '<div class="col-md-6 col-xs-6 col-sm-6 col-lg-6" style="margin:0px;padding:0px;text-align:left"><button class="btn btn-default btn-primary" style="color:white"  id="page_back">Back</button>'+
                    '</div><div class="col-md-6 col-xs-6 col-sm-6 col-lg-6" style="margin:0px;padding:0px;">'+
                    '<button class="btn btn-default btn-primary pull-right " style="color:white" id="page_next">Next</button> </div></div>');

            }
            else{
                console.log('i m in else case');
            }

            //alert(page_widthh);

            $(".design-innerr").css("width",width+'%');
            $("#main-contentt").css("width",page_content_width+'%');

            //$("#main-contentt").css("width",width);
        }

//     alert('hello');
//        $('.page-area-child').css('overflow','scroll');
//        $('.page-area-child').css('overflow-x','hidden');

        //$('.question-header').css('background-color',assessmentModel.page.page_back_color);
        $('.question-text').css('color',assessmentModel.page.page_text_color);
        $('.answer-text').css('color',assessmentModel.page.page_text_color);

//     alert(questionHtml);


        $('#page_next').attr("onclick","insertLayoutQuestions('"+axis+"','"+index+"','"+limit+"','"+counter_index+"')");
        var back_index = 0;
        console.log('loaded here -- '+loaded);
        if(loaded<limit && index>loaded) {
            var limit_t = parseInt(loaded)+parseInt(limit);
            back_index = index-limit_t;
            console.log('back index0 '+back_index +" index:"+index+" loaded:"+loaded +" limit "+limit+" limit_t "+limit_t);
        }
        else if(loaded == limit ) {
            back_index = parseInt(index)-parseInt(loaded);
            console.log('back index1 '+back_index);
        }
        else if(loaded<limit && index<loaded) {
            back_index = parseInt(index)-parseInt(loaded);
            console.log('back index2 '+back_index);
        }
        if(back_index>=0){
            if(axis == "Y" && index<=limit){
                $('#page_back').attr("onclick","insertLayoutQuestions('X','0','"+limit+"','"+counter_index+"')");
            }
            else{
                $('#page_back').attr("onclick","insertLayoutQuestions('"+axis+"','"+back_index+"','"+limit+"','"+counter_index+"')");
            }

        }
        else{
            $('#page_back').attr("onclick","insertLayoutQuestions('"+axis+"','0','"+limit+"','"+counter_index+"')");
        }

        if(total_questions.length == index) {
            $('#page_next').text("Submit");
            $('#page_next').attr("onclick","onSubmitAssessment()");

        }
    }

    updateHtmlData = function(questionModel,index,limit,axis,counter_index) {
        var scoreHtml = "";
        var counter = 0,counter_1= 0;
        var isFound = true;
        counter_index = parseInt(counter_index);
        for(var i = index;i<questionModel.length;i++ ) {
            counter = i;
//            counter_index =i;
            console.log("counter_1  here -- "+counter_1);

            if(axis =="X" && (questionModel.length <= limit || i == questionModel.length-1)){
                axis = "Y";

            }
            else if(axis == "X" && (questionModel.length > limit || i == questionModel.length-1)) {

                axis = "X";
            }
            if( counter_1>=limit ){
//            axis = "X";
                break;
            }



            if (questionModel[i].type === "Dropdown") {

                var optionsHtml = "<select style='width:100%' class='answer-text'>";

                for (var j = 0; j < questionModel[i].answers.length; j++) {

                    optionsHtml += "<option class='as-score-answer as-score-answer-" + i + "' value='" + questionModel[i].answers[j].id + "'>" + questionModel[i].answers[j].text + "</option>";
                }
//        console.log('min score -- '+questionModel.min);
                /*for(var i = 0;i<assessmentModel.questions['X'].length;i++) {
                 console.log('scores point-- '+assessmentModel.questions['X'][i].text);
                 console.log('min -- '+assessmentModel.questions['X'][i].min);
                 console.log('max -- '+assessmentModel.questions['X'][i].max);

                 }*/
                var q_number = '';
                if($("#s_question").is(":checked")) {
                    q_number = parseInt(i+1)+' . ';

                }
                scoreHtml = scoreHtml+'<div class="col-xs-12 question-header as-score-question as-score-question-' + index + '" style="background:'+assessmentModel.page.page_back_color+';margin-bottom: 9px; border-radius: 5px;padding-bottom:5px">' +
                    '<p class="col-xs-12 question-text" style="padding:0px;text-align: left;word-wrap: break-word">' + q_number  + questionModel[i].text +'</p>' +
                    '<div class="col-xs-12 as-score-answers" style="padding: 0;margin-top: 5px" >' + optionsHtml + '</select></div>' +
                    '</div>';
            } else if (questionModel[i].type === "Button") {

                var radiosHtml = "";

                for (var j = 0; j < questionModel[i].answers.length; j++) {

                    radiosHtml += "<div class='col-md-3 as-score-answer as-score-answer-" + i + "' style='padding: 0;margin-top: 5px'>" +
                        "<input type='radio' name='answers-" + i + "' />" +
                        "<span class='answer-text'>" + questionModel[i].answers[j].text + "</span>" +
                        "</div>";
                }
                var q_number = '';
                if($("#s_question").is(":checked")) {
                    q_number = parseInt(i+1)+' . ';
                }
                scoreHtml = scoreHtml+'<div class="col-xs-12 as-score-question question-header as-score-question-' + index + '" style="background:'+assessmentModel.page.page_back_color+';margin-bottom: 9px; border-radius: 5px;padding-bottom:5px">' +
                    '<p class="col-xs-12 question-text" style="padding:0px;text-align: left;word-wrap: break-word">' + q_number  + questionModel[i].text +'</p>' +
                    '<div class="col-xs-12 as-score-answers" style="padding: 0;">' + radiosHtml + '</div>' +
                    '</div>';
            }
            counter++;
            counter_1++;
            counter_index++;
        }
        console.log('counter here -- '+counter+' length here -- '+questionModel.length);
        console.log('score html -- '+scoreHtml);
        scoreHtml = scoreHtml+'</div>';

//    $('.page-area-child').append(scoreHtml+'</div><div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 " style="margin-top: 5px;">'+
//        '<div class="col-md-6 col-xs-6 col-sm-6 col-lg-6" style="margin:0px;padding:0px;text-align:left"><button class="btn btn-default btn-primary" style="color:white"  id="page_back">Back</button>'+
//        '</div><div class="col-md-6 col-xs-6 col-sm-6 col-lg-6" style="margin:0px;padding:0px;">'+
//        '<button class="btn btn-default btn-primary pull-right " style="color:white" id="page_next">Next</button> </div></div>');


        var data = {};
        data.axis = axis;
        data.index = counter;
        data.html = scoreHtml;
        data.loaded = counter_1;
        data.counter_index = counter_index;
        return data;
    }

    updateQuestionList = function() {
        var x_question = assessmentModel.questions["X"];
        var y_question = assessmentModel.questions["Y"];
        var total_questions = x_question.concat(y_question);
        total_questions.sort(function(a, b) {
            return parseInt(a.order) - parseInt(b.order);
        });

        var counter = 0;
        var question_txt = '<li ><span class="col-md-5" style="padding: 0px;color:black">Order</span> <span class="col-md-7" style="padding: 0px;color:black">Question</span></li>';
        for(var i = 0;i<total_questions.length;i++) {
            counter = i+1;
            question_txt = question_txt+'<li id='+total_questions[i].id+' style="overflow:hidden;word-wrap: break-word;"><span class="col-md-5" style="padding: 0px;color:black;overflow:hidden;word-wrap: break-word;">'+counter+' </span> <span class="col-md-7" style="padding: 0px;color:black">'+total_questions[i].text+'</span></li>';
        }

//   counter++;
        /*for(var i = 0;i<y_question.length;i++) {
         counter = counter+1;
         question_txt = question_txt+'<li id='+y_question[i].id+' style="overflow:hidden;word-wrap: break-word;"><span class="col-md-5" style="padding: 0px;color:black">'+counter+' </span> <span class="col-md-7" style="padding: 0px;color:black">Y('+counter+') '+y_question[i].text+'</span></li>';
         }*/

        $('.assessment-save ul li ul#first').html(question_txt);
        $( "#first" ).sortable({
            update:function(event,ui){
                setTimeout(function(){

                    var questions_ids = []
                    $("ul#first li").each(function(index) {
                        if(index>0)
                            questions_ids.push($(this).attr('id'));
                    });
                    //alert(questions_ids.join(','));
                    assessmentModel.orders = questions_ids.join(',');
                    saveAssessment(true,'getAssessmentModel');
                    //consoleAssessmentModel();
                },1000);
            }
        });

        /*
         *
         * */
    }

    loadResultData = function() {

            var stage_model = assessmentModel.stages;
            var stage_data = '';
            var stage_options = '';
            for(var i=0;i<stage_model.length;i++) {
                stage_data = stage_data+'<div class="assessment-layout-header col-md-12 col-lg-12 col-sm-12 col-xs-12">'+
                 '<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3"><p >'+stage_model[i].abbreviation+'</p></div><div class="col-md-9 col-lg-9 col-sm-9 col-xs-9">'+
                  '<input type="text" style="width:100%" id="abbr_'+i+'"/></div></div>';
                stage_options = stage_options+'<option value="'+stage_model[i].id+"_"+i+'">'+stage_model[i].abbreviation+"</option>";
            }
            stage_data = stage_data+'<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="text-align: right;margin-top:2%;margin-bottom: 2%"><button class="btn btn-default" onclick="onSaveResults()" style="margin-right:15px">Save</button>'+
            '</div>';

            $(".result_row").html(stage_data);
            $("#stage_options").html(stage_options);

            if(assessmentModel.result.result_stages) {

                var result_model = assessmentModel.result.result_stages;
                for(var i=0;i<result_model.length;i++) {
                    var id = '#abbr_'+i;
                    $(id).val(result_model[i].stage_url);
                }
            }
            $("#results_s").val(assessmentModel.result.result_msg_title);
            onStageChanged(); // load value in textarea here .. . ..


    }

    onSaveResults = function() {

        var stage_model = assessmentModel.stages;
//        alert('clicked');
        if($("#redirect").is(":checked") || $("#redirectt").is(":checked")) {
          var url_txt = [];
          var isValid = false;

          for(var i =0;i<stage_model.length;i++){
             var abbr_id = 'abbr_'+i;
             var abbr_val =  $('#'+abbr_id).val();

             if(abbr_val != ''){
                if(!validateURL(abbr_val)) {
                    alert("please enter valid url");
                    isValid = false;
                    break;
                }
                else{

                    var items = {};
                    items.stage_id = stage_model[i].id;
                    items.stage_url = abbr_val;
                    //items.stage_msg = '';
                    if(assessmentModel.result.result_stages.length != stage_model.length) {
                        assessmentModel.result.result_stages.push(items);
                    }
                    else{
                        assessmentModel.result.result_stages[i].stage_id = stage_model[i].id;
                        assessmentModel.result.result_stages[i].stage_url = abbr_val;


                    }
                    isValid = true;
                }
              }
             else{
                 alert("Please fill all urls");
                 break;
             }
          }
          if(isValid) {

              assessmentModel.result.result_urls = url_txt.join(',');
              saveAssessment(false,'none');
          }
          consoleAssessmentModel();
        }
        else{
           var title = $("#results_s").val();
           var desc = CKEDITOR.instances.editor1.getData();
           if(title == '' || desc =='') {
               alert("Please enter all fields");
               return;
           }
            assessmentModel.result.result_msg_title = title;

            for(var i =0;i<stage_model.length;i++) {

                var items = {};
                items.stage_id = stage_model[i].id;
                //items.stage_url = '';
                items.stage_message = '';
                if(assessmentModel.result.result_stages.length!=stage_model.length)
                  assessmentModel.result.result_stages.push(items);
            }
            var index = parseInt($("#stage_options").val().split("_")[1]);
            assessmentModel.result.result_stage_id = $("#stage_options").val().split("_")[0];
            assessmentModel.result.result_stages[index].stage_message = desc;

            saveAssessment(false,'none');
        }
    }

    onStageSelected = function(event) {
      event.preventDefault();
      event.stopPropagation();
      return false;
    }

    onStageChanged = function() {
        var result_stage = assessmentModel.result.result_stages;
        if(result_stage) {
            var stage_options = $("#stage_options").val().split("_")[0];
            console.log('option val -- ' + stage_options);
            for (var i = 0; i < result_stage.length; i++) {
                //$("#abbr_"+i).val(result_stage[i].stage_message);
                if (result_stage[i].stage_id == stage_options) {
                    CKEDITOR.instances.editor1.setData(result_stage[i].stage_message);
                }
            }
        }
    }

    onPreviewClicked = function() {
      //alert('preview clicked')
        $("#modal_title").html("<p style='color:"+assessmentModel.footer.footer_text_color+"'>"+assessmentModel.result.result_msg_title+"</p>");
        $("#modal_title").css("border","none");
        $("#modal_header").css("background-color",assessmentModel.footer.footer_back_color);
        $("#modal_body").css('background-color',assessmentModel.title.title_background);
        $("#modal_body").css('display','flex');
        $("#modal_body").css('display','-webkit-flex');
        $("#modal_body").css('flex-direction','column');
        $("#modal_body").css('-webkit-flex-direction','column');
        $("#modal_footer").css("display","none");
        var body_txt = '';
        var result_model = assessmentModel.result.result_stages;
        for(var i=0;i<result_model.length;i++) {
            if($("#stage_options").val().split("_")[0] === result_model[i].stage_id) {
                body_txt = body_txt+result_model[i].stage_message;
            }
        }
        $("#modal_body").html(body_txt);
        $("#myModal").modal("show");
//            alert(assessmentModel.footer.footer_back_color);

    }

    validateURL = function(textval) {
        var urlregex = /^(http|https):\/\/(([a-zA-Z0-9$\-_.+!*'(),;:&=]|%[0-9a-fA-F]{2})+@)?(((25[0-5]|2[0-4][0-9]|[0-1][0-9][0-9]|[1-9][0-9]|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9][0-9]|[1-9][0-9]|[0-9])){3})|localhost|([a-zA-Z0-9\-\u00C0-\u017F]+\.)+([a-zA-Z]{2,}))(:[0-9]+)?(\/(([a-zA-Z0-9$\-_.+!*'(),;:@&=]|%[0-9a-fA-F]{2})*(\/([a-zA-Z0-9$\-_.+!*'(),;:@&=]|%[0-9a-fA-F]{2})*)*)?(\?([a-zA-Z0-9$\-_.+!*'(),;:@&=\/?]|%[0-9a-fA-F]{2})*)?(\#([a-zA-Z0-9$\-_.+!*'(),;:@&=\/?]|%[0-9a-fA-F]{2})*)?)?$/;
        return urlregex.test(textval);
    }
    addAssessmentLogo = function(){
        $("#file1").click();
    }

    removeAssessmentLogo = function(){
        $('#logo_img').attr('src', 'images/profile-pic.png');
        $('#ass-logo').attr('src', 'images/profile-pic.png');

    }

    selectedFiles = function(input) {
        var logo_size = $('#logo-size').val();
        var url = input.value;
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();

            reader.onload = function (e) {
//                $('#logo_img_hide').attr('src', e.target.result);
//                var myImage = document.getElementById('logo_img_hide');
                var file, img;
                var _URL = window.URL || window.webkitURL;
                if ((file = input.files[0])) {
                    img = new Image();
                    img.onload = function () {
//                        alert(this.width + " " + this.height);
                        var dimension = {};
                        actualWidth = this.width;
                        actualHeight = this.height;
                        if(logo_size =='small' ) {
                            dimension = calculateImageSize(actualWidth,actualHeight,75,75);
                        }
                        else if(logo_size =='medium' ) {
                            dimension = calculateImageSize(actualWidth,actualHeight,150,150);
                        }
                        else if(logo_size =='large' ){
                            dimension = calculateImageSize(actualWidth,actualHeight,250,250);
                        }
                        console.log('actual width -- '+actualWidth);
                        console.log('actual height -- '+actualHeight);
                        console.log('width -- '+dimension.width);
                        console.log('height -- '+dimension.height);

                        if(this.height>250 || this.width>250) {
                            alert("Selected image size should not be greater than 250*250 dimention");
                            return;
                        }
                        $('#logo_img').attr('src', e.target.result);
                        $('#ass-logo').attr('src', e.target.result);
                        //$('#logo_img_hide').attr('src', '');
                        $('.assessment-logo').css('height',dimension.height);
                        $('.assessment-logo').css('width',dimension.width);
                        var elem = document.getElementById('logo_img');
                        var canvas = document.createElement('canvas');

                        canvas.width = this.width; // or 'width' if you want a special/scaled size
                        canvas.height = this.height; // or 'height' if you want a special/scaled size

                        canvas.getContext('2d').drawImage(this, 0, 0);
                        assessmentModel.logo.logo_height = dimension.height;
                        assessmentModel.logo.logo_width = dimension.width;
                        assessmentModel.logo.logo_path = canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, '');
                        assessmentModel.logo.logo_size = logo_size;

//                        $('.assessment-page-title').css('height',dimension.height);
                        if(dimension.height>170) {
                            $(".assessment-page-title").css("height",(dimension.height+5));
                        }
                        saveAssessment(true,'none');
                    };
                    img.src = _URL.createObjectURL(file);
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
        else{
            alert("Image should be .gif,.jpg,.png and .jpeg extention");
        }
    }

    logo_scale = function(id) {
        var logo_size = $('#'+id).val();

        var dimension = {};
        if(logo_size =='small' ){
            dimension = calculateImageSize(actualWidth,actualHeight,75,75);

        }
        else if(logo_size =='medium' ){
            dimension = calculateImageSize(actualWidth,actualHeight,150,150);
        }
        else if(logo_size =='large' ){
            dimension = calculateImageSize(actualWidth,actualHeight,250,250);
        }
        console.log('actual width -- '+actualWidth);
        console.log('actual height -- '+actualHeight);
        console.log('width -- '+dimension.width);
        console.log('height -- '+dimension.height);
        if(dimension.height>170) {
            $(".assessment-page-title").css("height",(dimension.height+5));
        }
        $('.assessment-logo').css('height',dimension.height);
        $('.assessment-logo').css('width',dimension.width);

        assessmentModel.logo.logo_height = dimension.height;
        assessmentModel.logo.logo_width = dimension.width;
        assessmentModel.logo.logo_size = logo_size;
        saveAssessment(true,'none');


    }
    getBase64Image = function (imgElem){
        // imgElem must be on the same server otherwise a cross-origin error will be thrown "SECURITY_ERR: DOM Exception 18"
        var canvas = document.createElement("canvas");
        canvas.width = imgElem.width;
        canvas.height = imgElem.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(imgElem, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        return dataURL.replace(/^data:image\/(png|jpg|jpeg|gif);base64,/, "");
    }

    calculateImageSize = function(actualWidth,actualHeight,maxWidth,maxHeight) {
        var width = 0,height= 0,ratio=0;
        if(actualWidth >= actualHeight){
            ratio = maxWidth / actualWidth;   // get ratio for scaling image
            width = maxWidth;
            height = actualHeight*ratio;
        }

        // Check if current height is larger than max
        if(actualHeight >= actualWidth){
            ratio = maxHeight / actualHeight; // get ratio for scaling image
            height = maxHeight;
            width = actualWidth*ratio;
        }
        var dimesion = {};
        dimesion.width = width;
        dimesion.height = height;
        return dimesion;
    }
    realImgDimension=function(ref) {
        return {
            naturalWidth: ref.width,
            naturalHeight: ref.height
        };
    }

    logo_position = function(id){
        console.log($('#'+id).val());
        if($('#'+id).val() == 'left') {

//      $(".assessment-logo").addClass("pull-left");
            /*
             $('.assessment-page-title').css('flex-direction','row');
             $('.assessment-page-title').css('-webkit-flex-direction','row');
             $('#ass-logo-container').css('text-align','left');
             $('#ass-title').css('text-align','right');
             */
            $('#ass-logo').removeClass('pull-right');
            $('#ass-logo').removeClass('pull-left');
            $('#ass-logo').addClass('pull-left');
            $('#ass-logo').removeAttr('margin-left');
            $('#ass-logo').removeAttr('margin-right');


        }
        else if($('#'+id).val() == 'right') {

//      $(".assessment-logo").addClass("pull-right");
            $('#ass-logo').removeClass('pull-right');
            $('#ass-logo').removeClass('pull-left');
            $('#ass-logo').addClass('pull-right');
            $('#ass-logo').removeAttr('margin-left');
            $('#ass-logo').removeAttr('margin-right');


        }
        else if($('#'+id).val() == 'center') {
//      $('#ass-logo-container').css('text-align','center');
//            console.log($('#ass-logo-container').css('text-align'));

            /*if($('#ass-logo-container').css('text-align') == 'left'){
             $('#ass-logo-container').css('text-align','right');
             return;
             }
             if($('#ass-logo-container').css('text-align') == 'right'){
             $('#ass-logo-container').css('text-align','left');
             return;
             }*/
            var total_width = parseInt($('.assessment-page-title').width());
            var title_width = parseInt($('#ass-title').width())+10;
            var margin_area = (total_width-title_width)/2;
//                    alert(margin_area+'total '+total_width+' logo '+logo_width);
            if($('#ass-logo').css('float') =='left'){
                //$('#'+id).css('text-align','right');
                $('#ass-logo').removeClass('pull-right');
                $('#ass-logo').removeClass('pull-left');
                $('#ass-logo').css('margin-left',margin_area+'px');

            }
            else if($('#'+id).css('float') =='right'){
                //$('#'+id).css('text-align','left');
                $('#ass-logo').removeClass('pull-right');
                $('#ass-logo').removeClass('pull-left');
                $('#ass-logo').css('margin-right',margin_area+'px');
            }
        }

        assessmentModel.logo.logo_position = $('#'+id).val();
        saveAssessment(true,'none');
    }

    selectASScoring = function (number) {

        $(".as-builds .as-build.active .as-scoring").removeClass("active");
        $(".as-builds .as-build.active .as-scoring.as-scoring-" + number).addClass("active");

//    alert('hello -- ');
    }

    selectASDelivery = function(number) {
        var childeren = $(".as-builds .as-build.active").children().length;
//        alert(childeren);
        $(".as-builds .as-build.active .as-delivery").removeClass("active");
        $(".as-builds .as-build.active .as-delivery.as-delivery-" + number).addClass("active");


    }

    selectASAxisScores = function (number,type) {

//    alert('select as axis is called');
//    selectASScoring(3);
        if(type =='review') {
            $(".as-builds .as-build.active .as-scoring.active .as-axis-scores").removeClass("active");
            $(".as-builds .as-build.active .as-scoring.active .as-axis-scores.as-axis-scores-" + step_number).addClass("active");
            $(".as-steps.as-main .as-step-2 .title").popover("hide");

            return;
        }
        if(type =='next' || type =='back'){
            step_number = number;
            $(".as-builds .as-build.active .as-scoring.active .as-axis-scores").removeClass("active");
            $(".as-builds .as-build.active .as-scoring.active .as-axis-scores.as-axis-scores-" + number).addClass("active");
            return;
        }

        $(".as-builds .as-build.active .as-scoring.active .as-axis-scores").removeClass("active");
        $(".as-builds .as-build.active .as-scoring.active .as-axis-scores.as-axis-scores-" + number).addClass("active");

        console.log(type);
    }

    insertNewQuestionEditor = function (axis, type, index) {

        if (systemWorkMode !== "Development" && transactionModel.max_questions_per_assessment != null && assessmentModel.questions[axis].length >= transactionModel.max_questions_per_assessment) {

            var message = escape("To Add More Questions you Need to Upgrade your Account.");
            showpop(message);
            return;
        }

        if(axis === "X") {

            $(".as-steps.as-main .as-step-3 .as-title").popover("hide");
        } else if(axis === "Y") {

            $(".as-steps.as-main .as-step-4 .as-title").popover("hide");
        }

        assessmentModel.questions[axis].splice(index, 0, {
            id: "",
            type: type,
            text: "",
            min: 0,
            max: 0,
            jsToken: saveJSToken(25),

            answers: []
        });

        insertQuestionEditor(axis, index);

        insertNewAnswer(axis, index, 0);

        insertScoreQuestion(axis, index);

        redrawScores(axis);

        if (assessmentModel.matrix.length <= 0) {

            assessmentModel.matrix[0] = [""];
        }

        for(var j = 0;j < assessmentModel.questions["Y"].length - 1 || (j == assessmentModel.questions["Y"].length - 1 && axis === "X");j++) {

            if(j >= assessmentModel.matrix.length) {

                assessmentModel.matrix.push([""]);
            }

            for(var i = 0;i < assessmentModel.questions["X"].length - 1 || (i == assessmentModel.questions["X"].length - 1 && axis === "Y");i++) {

                if(i >= assessmentModel.matrix[j].length) {

                    assessmentModel.matrix[j].push("");
                }
            }
        }

        if (axis === "X" && assessmentModel.questions["X"].length > assessmentModel.matrix[0].length) {

            for (var j = 0; j < assessmentModel.matrix.length; j++) {

                assessmentModel.matrix[j].splice(index, 0, "");
            }
        } else if (axis === "Y" && assessmentModel.questions["Y"].length > assessmentModel.matrix.length) {

            var temp = [];

            for (var i = 0; i < assessmentModel.matrix[0].length; i++) {

                temp.splice(i, 0, "");
            }

            assessmentModel.matrix.splice(index, 0, temp);
        }

        for(var key in assessmentModel.questions) {
            var start = 0,end = 0 ,total = 0;
            console.log('key -- ' +key);
            console.log('matrix  -- ' +assessmentModel.matrix_points[key].length);
            console.log('question -- ' +assessmentModel.questions[key].length);

            while((assessmentModel.questions[key].length - 1 > assessmentModel.matrix_points[key].length) && axis!==key ) {

                /*if( assessmentModel.questions[key].length == 1){
                 start = 0;
                 //               end = assessmentModel.questions[key].max;
                 }
                 else {
                 var divide = 100/assessmentModel.questions[key].length;
                 total = total+divide;
                 start = Math.ceil(total);
                 end = start+divide;
                 }*/

                assessmentModel.matrix_points[key].push({id: "", start: start, end: end, jsToken: saveJSToken(25)});
            }
        }

        if(assessmentModel.matrix_points["X"].length <= 0 && assessmentModel.matrix_points["Y"].length <= 0) {

            assessmentModel.matrix_points["X"].splice(0, 0, {id: "", start: 0, end: 0, jsToken: saveJSToken(25)});
            assessmentModel.matrix_points["Y"].splice(0, 0, {id: "", start: 0, end: 0, jsToken: saveJSToken(25)});
        } else {

            assessmentModel.matrix_points[axis].splice(index, 0, {id: "", start: 0, end: 0, jsToken: saveJSToken(25)});
        }

        consoleAssessmentModel();

        redrawMatrix();
    }

    insertQuestionEditor = function (axis, index) {

        var html = '<div class="col-xs-12 as-question-builder as-question-editor as-edit as-edit-' + index + ' as-question-builder-' + index + '">' +
            '<div class="col-xs-12 as-question-type"><div></div></div>' +
            '<div class="col-xs-12 as-question-edit" style="padding: 0px; margin-bottom: 5px;">' +
            '<span class="col-md-2 as-title" style="padding-left: 0px;">Question ' + (index + 1) + ':</span>' +
            '<input class="col-md-10 as-text" type="text" placeholder="Question Text" value="' + assessmentModel.questions[axis][index].text + '" />' +
            '</div>' +
            '<div class="col-xs-12 as-answers">' +
            '</div>' +
            '<div class="col-xs-12 as-control" style="padding: 0px; margin-top: 10px; text-align: right; border: none; margin-bottom: 0;">' +
            '<button class="btn btn-info as-save" style="line-height: 12px; margin-right: 3px;" onclick="onSaveQuestionClicked(\'' + axis + "\', " + index + ');">Save</button>' +
            '<button class="btn btn-danger as-delete" style="line-height: 12px; margin-right: 3px;" onclick="onDeleteQuestionEditorClicked(\'' + axis + "\', " + index + ');">Delete</button>' +
            '<button class="btn btn-default as-cancel" style="line-height: 12px;" onclick="onCancelQuestionEditorClicked(\'' + axis + "\', " + index + ');">Cancel</button>' +
            '</div>' +
            '</div>';

        insertHtmlElement($(".as-builds .as-build.active .as-questions"), index, html);

        var answers = assessmentModel.questions[axis][index].answers;

        for (var i = 0; i < answers.length; i++) {

            insertAnswer(axis, index, i);
        }
    }

    onCancelQuestionEditorClicked = function (axis, e_index) {
//    var question_txt =  asQuestionEditor.find(".as-question-edit .as-text").val().trim();
        var asQuestionEditor = $(".as-builds .as-build.active .as-questions .as-edit-" + e_index);
        var question_txt =  asQuestionEditor.find(".as-question-edit .as-text").val().trim();
        var isAnswerEmpty = true;
        var answerEditors = asQuestionEditor.find(".as-answers").children();

        for (var i = 0; i < answerEditors.length; i++) {
            var answerText = answerEditors.eq(i).find(".as-text").val().trim();
            if(answerText == '')
            {
                isAnswerEmpty = false;
                break;

            }
            console.log('answer text -- '+answerEditors.eq(i).find(".as-text").val().trim());
        }

        /*if(question_txt =='') {
         alert('Question fields must not be empty');
         }
         else if(answerText == '') {
         alert("Answer field should not be empty");
         }
         else{
         //        $(".as-builds .as-build.active .as-questions .as-edit-" + e_index).remove();

         //        insertQuestion(axis, e_index);
         deleteQuestion(axis,e_index);
         }*/

        deleteQuestion(axis,e_index);

    }

    onDeleteQuestionEditorClicked = function (axis, e_index) {

        if (confirm("Are you sure to delete this question?")) {

            deleteQuestion(axis, e_index);
        }
    }

    onSaveQuestionClicked = function (axis, index) {
        var isAnswerEmpty = true;
        var isQuestionEmpty = true;
        var asQuestionEditor = $(".as-builds .as-build.active .as-questions .as-edit-" + index);
        var question_txt =  asQuestionEditor.find(".as-question-edit .as-text").val().trim();
        assessmentModel.questions[axis][index].text = question_txt;
        if(question_txt == '') {
            isQuestionEmpty = false;
        }
        var answerEditors = asQuestionEditor.find(".as-answers").children();

        for (var i = 0; i < answerEditors.length; i++) {
            var answerText = answerEditors.eq(i).find(".as-text").val().trim();
            if(answerText != '')
                assessmentModel.questions[axis][index].answers[i].text = answerEditors.eq(i).find(".as-text").val().trim();
            else{
                isAnswerEmpty = false;
                break;
            }
            console.log('answer text -- '+answerEditors.eq(i).find(".as-text").val().trim());
        }

        if( !isQuestionEmpty ) {
            alert('Question fields must not be empty');
            return;

        }
        else if( !isAnswerEmpty ) {
            alert('All Answer fields must not be empty');
            return;
        }
        else if(assessmentModel.questions[axis][index].answers.length<=1) {
            alert('At least 2 answers must be added');
            return;
        }
        else {

            $(".as-builds .as-build.active .as-questions .as-edit-" + index).remove();

            var temp = null;

            if (axis === "X") {
                temp = $(".as-builds .as-build.as-build-2 .as-scoring.as-scoring-3 .as-axis-scores.as-axis-scores-1 .as-score-questions .as-score-question-" + index);
            } else if (axis === "Y") {
                temp = $(".as-builds .as-build.as-build-2 .as-scoring.as-scoring-3 .as-axis-scores.as-axis-scores-2 .as-score-questions .as-score-question-" + index);
            }

            temp.remove();

//        var questionModel = assessmentModel.questions[axis][index];
//        for(var i=0;assessmentModel.questions[axis].length;i++){
//        }
            insertQuestion(axis, index);
            insertScoreQuestion(axis, index);
            redrawScores(axis);
            repaintAdvancedQuestionEditor(axis, index);
            insertMatrixPoint(axis);
            console.log('len here--'+assessmentModel.questions[axis].length);

//        consoleAssessmentModel();


        }
    }

    isIneger = function(value){
        if (isNaN(value)) {
            return false;
        }
        var x = parseFloat(value);
        return (x | 0) === x;
    }
    insertMatrixPoint = function(axis){
        var matrixModel = assessmentModel.matrix_points[axis];
        var start= 0,end= 0,total = 0,divide=0;
        for(var i=0;i<matrixModel.length;i++) {

            if( assessmentModel.questions[axis].length == 1) {
                start = 0;
                end = assessmentModel.questions[axis][0].max;
            }
            else{
                if(i == 0) {

                    divide = 100/assessmentModel.questions[axis].length;
                    end = Math.floor(divide);

                }
                else {
                    total = total+Math.floor(divide);

//            start = total;
//            divide = 100/assessmentModel.questions[axis].length;
                    start = Math.floor(total);
                    end = Math.floor(start+divide);
                    if(i > 0) {
                        start++;
                    }
                    if(i == matrixModel.length-1 && end<100 ) {
                        end = end+1;
                    }
                    if(i == matrixModel.length-1 && end>100)
                        end--;

                }
            }
            assessmentModel.matrix_points[axis][i].start = start;
            assessmentModel.matrix_points[axis][i].end = end;


        }
        redrawMatrix();
    }

    editQuestion = function (axis, index) {

        $(".as-builds .as-build.active .as-questions .as-question-" + index).remove();

        insertQuestionEditor(axis, index);
    }

    insertAdvancedQuestionEditorAnswer = function(axis, e_index, a_index) {

        var advancedQuestionEditor = $(".as-builds .as-build-2 .as-scoring-4 .as-question-editor-advanced-" + axis + "-" + e_index);

        if(advancedQuestionEditor.length > 0) {

            var html = '<div class="as-answer-edit as-answer-edit-' + a_index + ' col-xs-12">' +
                '<div class="col-md-2 as-score" style="padding-right: 0px;">' +
                '<input class="col-xs-12" type="text" value="' + assessmentModel.questions[axis][e_index].answers[a_index].score + '" onkeyup="validateMinMax(\'' + axis + '\', ' + e_index + ', this);" />' +
                '</div>' +
                '<div class="col-md-9 as-text">' +
                '<input class="col-xs-12" type="text" value="' + assessmentModel.questions[axis][e_index].answers[a_index].text + '" />' +
                '</div>' +
                '<div class="col-md-1" style="text-align: center; height: 26px;">' +
                '<span class="glyphicon glyphicon-plus" style="vertical-align: middle; color: green; cursor: pointer; margin-right: 5px;"></span>' +
                '<span class="glyphicon glyphicon-remove-circle" style="vertical-align: middle; color: red; cursor: pointer;"></span>' +
                '</div>' +
                '</div>';

            insertHtmlElement(advancedQuestionEditor.find(".as-content .as-answers"), a_index, html);
        }
    }

    insertNewAnswer = function (axis, e_index, a_index) {

        /*
         if (systemWorkMode !== "Development" && transactionModel.max_answers_per_question != null && assessmentModel.questions[axis][e_index].answers.length >= transactionModel.max_answers_per_question) {
         var message = escape("To Add More Answers you Need to Upgrade your Account.");
         showpop(message);
         return;
         }
         */

        var total_answer = assessmentModel.questions[axis][e_index].answers.length;
        console.log('question index ---- '+e_index);
        console.log('answer here ---- '+JSON.stringify(assessmentModel.questions[axis][e_index].answers));
        console.log('total answers ---- '+total_answer);



        var  account_type = $("#account_type").val();
        var isAddNew = true;
        switch(account_type)
        {
            case "free":
                if(total_answer>2) {
                    isAddNew = false;
                }
                break;
            case "Koalafy Joey":
                if(total_answer>4) {
                    isAddNew = false;
                }
                break;

        }
        if( isAddNew ) {

            assessmentModel.questions[axis][e_index].answers.splice(a_index, 0, {
                id: "",
                score: 0,
                text: "",
                jsToken: saveJSToken(25)
            });

            insertAnswer(axis, e_index, a_index);

            insertScoreAnswer(axis, e_index, a_index);

            insertAdvancedQuestionEditorAnswer(axis, e_index, a_index);

        }
        else {
            var message = escape("To Add More Answers you Need to Upgrade your Account.");
            showpop(message);
        }
    }
    showpop = function(message)
    {
        message = unescape(message);
        $(".modal-title").html("<span style='color:red'>Warning</span>");
        $(".modal-body").html(message);
        $(".modal-body").css('background','#ffffff');
        $(".modal-footer").css("display","block");
        $("#modal-body").css("height","160px");
        $(".modal-footer").html("<input type='button' class='btn btn-default' value='Cancel' data-dismiss='modal'/>" +
            "<input type='button' class='btn btn-success' value='Upgrade' onclick='upgradeAccount()' />");
        $("#myModal").modal("show");
    }

    upgradeAccount =function(){
        $("#myModal").modal("hide");
        window.location="../pricing/pricing.php";
    }
    insertAnswer = function (axis, e_index, a_index) {

        var answerModel = assessmentModel.questions[axis][e_index].answers[a_index];

        var html = '<div class="col-xs-12 as-answer as-answer-' + a_index + '">' +
            '<div class="col-md-11" style="padding-left: 0;"><input class="col-xs-12 as-text" type="text" placeholder="Answer Text" value="' + answerModel.text + '" /></div>' +
            '<div class="col-md-1" style="padding: 0px; line-height: 26px; text-align: center;">' +
            '<span class="glyphicon glyphicon-plus as-insert" onclick="insertNewAnswer(\'' + axis + '\', ' + e_index + ', ' + (a_index + 1) + ');redrawAnswerIndexes(\'' + axis + '\', ' + e_index + ');redrawAdvancedQuestionEditorAnswerIndexes(\'' + axis + '\', ' + e_index + ');" aria-hidden="true" style="color: green; cursor: pointer; margin-right: 5px;"></span>' +
            '<span class="glyphicon glyphicon-remove-circle as-remove" onclick="removeAnswerEditor(\'' + axis + '\', ' + e_index + ', ' + a_index + ');" aria-hidden="true" style="color: red; cursor: pointer;"></span>' +
            '</div>' +
            '</div>';

        insertHtmlElement($(".as-builds .as-build.active .as-questions .as-edit-" + e_index + " .as-answers"), a_index, html);
    }

    insertQuestion = function (axis, index) {

        var questionModel = assessmentModel.questions[axis][index];

        var html = "";

        if (questionModel.type === "Dropdown") {

            var optionsHtml = "";

            for (var i = 0; i < questionModel.answers.length; i++) {

                optionsHtml += "<option value='" + questionModel.answers[i].id + "'>" + questionModel.answers[i].text + "</option>";
            }

            html = '<div class="col-xs-12 as-question as-question-' + index + '">' +
                '<div class="col-md-11" style="padding: 0;">' +
                '<p class="col-xs-12 as-text" style="padding: 0;">' + (index + 1) + '. ' + questionModel.text + '</p>' +
                '<select class="col-xs-12">' + optionsHtml + '</select>' +
                '</div>' +
                '<span onclick="editQuestion(\'' + axis + "\', " + index + ')" class="col-md-1 as-edit" style="line-height: 56px; font-weight: bold; color: cornflowerblue; text-decoration: underline; padding-left: 10px; padding-right: 0; cursor: pointer; padding-top: 0; padding-bottom: 0; margin: 0; border: none; text-align: center;">Edit</span>' +
                '</div>';
        } else if (questionModel.type === "Button") {

            var radiosHtml = "";

            for (var i = 0; i < questionModel.answers.length; i++) {

                radiosHtml += "<div class='col-md-3 vs-answer' style='padding: 0;'>" +
                    "<input type='radio' name='answers-" + index + "' />" +
                    "&nbsp&nbsp<span>" + questionModel.answers[i].text + "</span>" +
                    "</div>";
            }

            html = '<div class="col-xs-12 as-question as-question-' + index + '">' +
                '<div class="col-md-11" style="padding: 0;">' +
                '<p class="col-xs-12 as-text" style="padding: 0;">' + (index + 1) + '. ' + questionModel.text + '</p>' +
                '<div class="col-xs-12 vs-answers" style="padding: 0;">' + radiosHtml + '</div>' +
                '</div>' +
                '<span onclick="editQuestion(\'' + axis + "\', " + index + ')" class="col-md-1 as-edit" style="line-height: 56px; font-weight: bold; color: cornflowerblue; text-decoration: underline; padding-left: 10px; padding-right: 0; cursor: pointer; padding-top: 0; padding-bottom: 0; margin: 0; border: none; text-align: center;">Edit</span>' +
                '</div>';
        }

        var temp = null;

        if (axis === "X") {
            temp = $(".as-builds .as-build-3 .as-questions");
        } else if (axis === "Y") {
            temp = $(".as-builds .as-build-4 .as-questions");
        }

        insertHtmlElement(temp, index, html);
    }

    insertScoreQuestion = function (axis, index) {
//   consoleAssessmentModel();
        // commented
        var questionModel = assessmentModel.questions[axis][index];

        var scoreHtml = "";

        if (questionModel.type === "Dropdown") {

            var optionsHtml = "";

            for (var i = 0; i < questionModel.answers.length; i++) {

                optionsHtml += "<option class='as-score-answer as-score-answer-" + i + "' value='" + questionModel.answers[i].id + "'>" + questionModel.answers[i].text + "</option>";
            }
//        console.log('min score -- '+questionModel.min);
            /*for(var i = 0;i<assessmentModel.questions['X'].length;i++) {
             console.log('scores point-- '+assessmentModel.questions['X'][i].text);
             console.log('min -- '+assessmentModel.questions['X'][i].min);
             console.log('max -- '+assessmentModel.questions['X'][i].max);

             }*/

            scoreHtml = '<div class="col-xs-12 as-score-question as-score-question-' + index + '">' +
                '<div class="col-xs-2 as-points" style="text-align: center; margin-top: 21px;">' +
                '<input class="col-xs-5 as-min" style="padding: 0px; border: 1px solid; text-align: center;" value="' + questionModel.min + '" onkeyup="if(validateMin(\'' + axis + '\', ' + index + ', this)) { drawAxisMinMax(\'' + axis + '\'); };"' + (systemWorkMode !== "Development" && !transactionModel.weighting ? " readonly" : "") + ' />' +
                '<input class="col-xs-5 as-max" style="padding: 0px; border: 1px solid; text-align: center;" value="' + questionModel.max + '" onkeyup="if(validateMax(\'' + axis + '\', ' + index + ', this)) { drawAxisMinMax(\'' + axis + '\'); };"' + (systemWorkMode !== "Development" && !transactionModel.weighting ? " readonly" : "") + ' />' +
                '</div>' +
//            '<input class="col-xs-2" type="checkbox" style="margin-top: 26px;" onclick="if($(this).prop(\'checked\')) {showQuestionEditorAdvanced(\'' + axis + '\', ' + index + ');}" />' +
                '<div class="col-xs-2" style="margin-top:19px;"><button type="button" class="btn btn-sm btn-primary " onclick="showQuestionEditorAdvanced(\'' + axis + '\', ' + index + ');"><i class="fa fa-pencil"></i></button></div>'+

                '<div class="col-xs-8 as-content">' +
                '<p class="col-xs-12">' + (index + 1) + '. ' + questionModel.text + '</p>' +
                '<select class="col-xs-12 as-score-answers">' + optionsHtml + '</select>' +
                '</div>' +
                '</div>';
        } else if (questionModel.type === "Button") {

            var radiosHtml = "";

            for (var i = 0; i < questionModel.answers.length; i++) {

                radiosHtml += "<div class='col-md-3 as-score-answer as-score-answer-" + i + "' style='padding: 0;'>" +
                    "<input type='radio' name='answers-" + i + "' />" +
                    "<span>" + questionModel.answers[i].text + "</span>" +
                    "</div>";
            }

            scoreHtml = '<div class="col-xs-12 as-score-question as-score-question-' + index + '">' +
                '<div class="col-xs-2 as-points" style="text-align: center; margin-top: 21px;">' +
                '<input class="col-xs-5 as-min" style="padding: 0px; border: 1px solid; text-align: center;" value="' + questionModel.min + '" onkeyup="if(validateMin(\'' + axis + '\', ' + index + ', this)) { drawAxisMinMax(\'' + axis + '\'); };"' + (systemWorkMode !== "Development" && !transactionModel.weighting ? " readonly" : "") + ' />' +
                '<input class="col-xs-5 as-max" style="padding: 0px; border: 1px solid; text-align: center;" value="' + questionModel.max + '" onkeyup="if(validateMax(\'' + axis + '\', ' + index + ', this)) { drawAxisMinMax(\'' + axis + '\'); };"' + (systemWorkMode !== "Development" && !transactionModel.weighting ? " readonly" : "") + ' />' +
                '</div>' +
//            '<input class="col-xs-2" type="checkbox" style="margin-top: 26px;" onclick="if($(this).prop(\'checked\')) {showQuestionEditorAdvanced(\'' + axis + '\', ' + index + ');}" />' +
                '<div class="col-xs-2" style="margin-top:19px;"><button type="button" class="btn btn-sm btn-primary " onclick="showQuestionEditorAdvanced(\'' + axis + '\', ' + index + ');"><i class="fa fa-pencil"></i></button></div>'+

                '<div class="col-xs-8 as-content">' +
                '<p class="col-xs-12">' + (index + 1) + '. ' + questionModel.text + '</p>' +
                '<div class="col-xs-12 as-score-answers" style="padding: 0;">' + radiosHtml + '</div>' +
                '</div>' +
                '</div>';
        }

        var temp = [];

        if (axis === "X") {
            temp = $(".as-builds .as-build.as-build-2 .as-scoring.as-scoring-3 .as-axis-scores.as-axis-scores-1 .as-score-questions");
        } else if (axis === "Y") {
            temp = $(".as-builds .as-build.as-build-2 .as-scoring.as-scoring-3 .as-axis-scores.as-axis-scores-2 .as-score-questions");
        }

        insertHtmlElement(temp, index, scoreHtml);
    }

    insertScoreAnswer = function (axis, q_index, a_index) {

        var answerModel = assessmentModel.questions[axis][q_index].answers[a_index];

        var html = "";

        if (assessmentModel.questions[axis][q_index].type === "Dropdown") {

            html = "<option class='as-score-answer as-score-answer-" + a_index + "' value='" + assessmentModel.questions[axis][q_index].answers[a_index].id + "'>" + assessmentModel.questions[axis][q_index].answers[a_index].text + "</option>";
        } else if (assessmentModel.questions[axis][q_index].type === "Button") {

            html = "<div class='col-md-3 as-score-answer as-score-answer-" + a_index + "' style='padding: 0;'>" +
                "<input type='radio' name='answers-" + a_index + "' />" +
                "<span>" + assessmentModel.questions[axis][q_index].answers[a_index].text + "</span>" +
                "</div>";
        }

        var temp = [];

        if (axis === "X") {
            temp = $(".as-builds .as-build.as-build-2 .as-scoring.as-scoring-3 .as-axis-scores.as-axis-scores-1 .as-score-questions .as-score-question-" + q_index + " .as-content .as-score-answers");
        } else if (axis === "Y") {
            temp = $(".as-builds .as-build.as-build-2 .as-scoring.as-scoring-3 .as-axis-scores.as-axis-scores-2 .as-score-questions .as-score-question-" + q_index + " .as-content .as-score-answers");
        }

        insertHtmlElement(temp, a_index, html);
    }

    redrawAnswerIndexes = function (axis, e_index) {

        var answers = $(".as-builds .as-build.active .as-questions .as-edit-" + e_index + " .as-answers").children();

        for (var i = 0; i < answers.length; i++) {

            answers.eq(i).attr("class", "col-xs-12 as-answer as-answer-" + i).find(".as-insert").attr("onclick", "insertNewAnswer('" + axis + "', " + e_index + ", " + (i + 1) + ");redrawAnswerIndexes('" + axis + "', " + e_index + ");redrawAdvancedQuestionEditorAnswerIndexes('" + axis + "', " + e_index + ");");
            answers.eq(i).find(".as-remove").attr("onclick", "removeAnswerEditor('" + axis + "', " + e_index + ", " + i + ");");
        }

        if (axis === "X") {
            answers = $(".as-builds .as-build.as-build-2 .as-scoring.as-scoring-3 .as-axis-scores.as-axis-scores-1 .as-score-questions .as-score-question-" + e_index + " .as-content .as-score-answers").children();
        } else if (axis === "Y") {
            answers = $(".as-builds .as-build.as-build-2 .as-scoring.as-scoring-3 .as-axis-scores.as-axis-scores-2 .as-score-questions .as-score-question-" + e_index + " .as-content .as-score-answers").children();
        }

        if (assessmentModel.questions[axis][e_index].type === "Dropdown") {

            for (var i = 0; i < answers.length; i++) {

                answers.eq(i).attr("class", "as-score-answer as-score-answer-" + i);
            }
        } else if (assessmentModel.questions[axis][e_index].type === "Button") {

            for (var i = 0; i < answers.length; i++) {

                answers.eq(i).attr("class", "col-md-3 as-score-answer as-score-answer-" + i);
                answers.eq(i).find("input").attr("name", "answers-" + i);
            }
        }
    }

    removeAnswerEditor = function (axis, e_index, a_index) {

        assessmentModel.questions[axis][e_index].answers.splice(a_index, 1);
        console.log('answer length here ---- '+assessmentModel.questions[axis][e_index].answers.length);
        removeHtmlElement($(".as-builds .as-build-2 .as-scoring-4 .as-question-editor-advanced-" + axis + "-" + e_index + " .as-content .as-answers"), a_index);

        if (axis === "X") {
            removeHtmlElement($(".as-builds .as-build-3 .as-questions .as-edit-" + e_index + " .as-answers"), a_index);
            removeHtmlElement($(".as-builds .as-build-2 .as-scoring-3 .as-axis-scores-1 .as-score-questions .as-score-question-" + e_index + " .as-content .as-score-answers"), a_index);
        } else if (axis === "Y") {
            removeHtmlElement($(".as-builds .as-build-4 .as-questions .as-edit-" + e_index + " .as-answers"), a_index);
            removeHtmlElement($(".as-builds .as-build-2 .as-scoring-3 .as-axis-scores-2 .as-score-questions .as-score-question-" + e_index + " .as-content .as-score-answers"), a_index);
        }

        if (assessmentModel.questions[axis][e_index].answers.length <= 0) {
            insertNewAnswer(axis, e_index, 0);
        } else {
            redrawAnswerIndexes(axis, e_index);
            redrawAdvancedQuestionEditorAnswerIndexes(axis, e_index);
        }
    }

    repaintQuestion = function(axis, q_index, isQuestion, isQuestionEditor, isQuestionScorer) {

        if(isQuestion) {

            var asQuestion = [];

            if (axis === "X") {
                asQuestion = $(".as-builds .as-build-3 .as-questions .as-question-" + q_index);
            } else if (axis === "Y") {
                asQuestion = $(".as-builds .as-build-4 .as-questions .as-question-" + q_index);
            }

            if(asQuestion.length > 0) {

                asQuestion.find(".as-text").html((q_index + 1) + ". " + assessmentModel.questions[axis][q_index].text);

                if(assessmentModel.questions[axis][q_index].type === "Dropdown") {

                    asQuestion.find("select option").each(function(index, option) {

                        $(option).html(assessmentModel.questions[axis][q_index].answers[index].text);
                    });
                } else if(assessmentModel.questions[axis][q_index].type === "Button") {

                    asQuestion.find(".vs-answers .vs-answer").each(function(index, element) {

                        $(element).find("span").html(assessmentModel.questions[axis][q_index].answers[index].text);
                    });
                }
            }
        }

        if(isQuestionEditor) {

            var asQuestionEditor = [];

            if (axis === "X") {
                asQuestionEditor = $(".as-builds .as-build-3 .as-questions .as-edit-" + q_index);
            } else if (axis === "Y") {
                asQuestionEditor = $(".as-builds .as-build-4 .as-questions .as-edit-" + q_index);
            }

            if(asQuestionEditor.length > 0) {

                asQuestionEditor.find(".as-question-edit .as-title").html("Question " + (q_index + 1) + ":");
                asQuestionEditor.find(".as-question-edit .as-text").val(assessmentModel.questions[axis][q_index].text);

                asQuestionEditor.find(".as-answers .as-answer").each(function(index, asAnswerEditor) {

                    $(asAnswerEditor).find(".as-text").val(assessmentModel.questions[axis][q_index].answers[index].text);
                });
            }
        }

        if(isQuestionScorer) {

            var asQuestionScorer = [];

            if (axis === "X") {
                asQuestionScorer = $(".as-builds .as-build.as-build-2 .as-scoring.as-scoring-3 .as-axis-scores.as-axis-scores-1 .as-score-questions .as-score-question-" + q_index);
            } else if (axis === "Y") {
                asQuestionScorer = $(".as-builds .as-build.as-build-2 .as-scoring.as-scoring-3 .as-axis-scores.as-axis-scores-2 .as-score-questions .as-score-question-" + q_index);
            }

            if(asQuestionScorer.length > 0) {

                asQuestionScorer.find(".as-content").find("p").html((q_index + 1) + ". " + assessmentModel.questions[axis][q_index].text);

                if(assessmentModel.questions[axis][q_index].type === "Dropdown") {

                    asQuestionScorer.find(".as-content select option").each(function(index, option) {

                        $(option).html(assessmentModel.questions[axis][q_index].answers[index].text);
                    });
                } else if(assessmentModel.questions[axis][q_index].type === "Button") {

                    asQuestionScorer.find(".as-content .as-score-answers .as-score-answer").each(function(index, element) {

                        $(element).find("span").html(assessmentModel.questions[axis][q_index].answers[index].text);
                    });
                }
            }
        }
    }

    updateXYCategory = function () {

        console.log($(".as-build.active .as-helper input[name='a-helper']").prop("checked"));
        if ($(".as-build.active .as-helper input[name='a-helper']").prop("checked")) {

            if ($(".as-build.active .x-category select").children().length > 0) {
                assessmentModel.details.x_category = $(".as-build.active .x-category select").find("option[value='" + $(".as-build.active .x-category select").val().trim() + "']").html();
            } else {
                assessmentModel.details.x_category = "";
            }

            if ($(".as-build.active .y-category select").children().length > 0) {
                assessmentModel.details.y_category = $(".as-build.active .y-category select").find("option[value='" + $(".as-build.active .y-category select").val().trim() + "']").html();
            } else {
                assessmentModel.details.y_category = "";
            }
        } else {
            assessmentModel.details.x_category = $(".as-build.active .x-category input").val().trim();
            assessmentModel.details.y_category = $(".as-build.active .y-category input").val().trim();
        }

        $(".as-build.active .x-category select").css("border-color", "");
        $(".as-build.active .x-category select").popover("hide");
        $(".as-build.active .y-category select").css("border-color", "");
        $(".as-build.active .y-category select").popover("hide");
        $(".as-build.active .x-category input").css("border-color", "");
        $(".as-build.active .x-category input").popover("hide");
        $(".as-build.active .y-category input").css("border-color", "");
        $(".as-build.active .y-category input").popover("hide");
    }

    $(document).ready(function () {

        setInterval(function () {

            //saveAssessment(true);
        }, 300000);
    });

    saveAssessmentUsers = function () {
//        console.log('modell -- '+JSON.stringify(assessmentUserModel)) ;
        $.post("../../services/AssessmentService.php", {service_function: "saveAssessmentUsers", model: assessmentUserModel}, function (response, status) {
            console.log(response);
            var json = $.parseJSON(response);
            if(json.success_flag) {
                //alert("Assessment uploaded successfully");
                if(assessmentModel.result.result_isRedirect === 'A') {
                    if(assessmentModel.result.result_stages.length >0) {
                        window.location = assessmentModel.result.result_stages[0].stage_url;
                    }
                    else{
                        alert("Ooops url is not set up for stage yet");
                    }
                }
                else{
                    if(assessmentModel.result.result_stages.length >0) {
                        onPreviewClicked();
                    }
                    else{
                        alert("Ooops message is not set up for stage yet");
                    }
                }
            }
            else{
                alert(json.message);
            }
          });
        }

    saveAssessment = function (silent_flag,type, callback) {
        if(assessmentModel.details.name == ''){
            //alert('assessment id mustnot be empty -- ');
            return;
        }
        $.post("../../services/AssessmentService.php", {service_function: "saveAssessment", assessment_model: assessmentModel}, function (response, status) {

            console.log(response);
            //alert(response);
            response = JSON.parse(response);
            //consoleAssessmentModel();
//            console.log(response);
            if (response.success_flag) {

                assessmentModel.id = response.assessment_model.id;

                if (typeof response.assessment_model.questions === "object") {

                    var axis = Object.keys(response.assessment_model.questions);

                    for (var x = 0; x < axis.length; x++) {

                        var serverQuestions = response.assessment_model.questions[axis[x]];

                        for (var i = 0; i < serverQuestions.length; i++) {

                            var clientQuestions = assessmentModel.questions[axis[x]];

                            for (var j = 0; j < clientQuestions.length; j++) {

                                if (clientQuestions[j].jsToken === serverQuestions[i].jsToken) {

                                    clientQuestions[j].id = serverQuestions[i].id;

                                    var serverAnswers = serverQuestions[i].answers;

                                    for (var sa = 0; sa < serverAnswers.length; sa++) {

                                        var clientAnswers = clientQuestions[j].answers;

                                        for (var ca = 0; ca < clientAnswers.length; ca++) {

                                            if (clientAnswers[ca].jsToken === serverAnswers[sa].jsToken) {

                                                clientAnswers[ca].id = serverAnswers[sa].id;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (typeof response.assessment_model.stages === "object") {

                    for (var i = 0; i < response.assessment_model.stages.length; i++) {

                        for (var j = 0; j < assessmentModel.stages.length; j++) {

                            if (assessmentModel.stages[j].jsToken === response.assessment_model.stages[i].jsToken) {

                                assessmentModel.stages[j].id = response.assessment_model.stages[i].id;
                                break;
                            }
                        }
                    }
                }

                if (typeof response.assessment_model.matrix_points === "object") {

                    for (var axis in response.assessment_model.matrix_points) {

                        var serverPoints = response.assessment_model.matrix_points[axis];

                        for (var i = 0; i < serverPoints.length; i++) {

                            var clientPoints = assessmentModel.matrix_points[axis];

                            for (var j = 0; j < clientPoints.length; j++) {

                                if (clientPoints[j].jsToken === serverPoints[i].jsToken) {

                                    clientPoints[j].id = serverPoints[i].id;
                                }
                            }
                        }
                    }
                }
                //alert(response.assessment_model.template.id);

                if (typeof response.assessment_model.template === "object") {
                    assessmentModel.template.id = response.assessment_model.template.id;
                }

                if (typeof response.assessment_model.setting === "object") {
                    assessmentModel.setting.id = response.assessment_model.setting.id;
                }
                if (typeof response.assessment_model.title === "object") {
                    assessmentModel.title.id = response.assessment_model.title.id;
                }
                if (typeof response.assessment_model.background === "object") {
                    assessmentModel.background.id = response.assessment_model.background.id;
                }
                if (typeof response.assessment_model.logo === "object") {
                    assessmentModel.logo.id = response.assessment_model.logo.id;
                }
                if (typeof response.assessment_model.footer === "object") {
                    assessmentModel.footer.id = response.assessment_model.footer.id;
                }
                if (typeof response.assessment_model.layout === "object") {
                    assessmentModel.layout.id = response.assessment_model.layout.id;
                }
                if (typeof response.assessment_model.page === "object") {
                    assessmentModel.page.id = response.assessment_model.page.id;
                }
                if (typeof response.assessment_model.delivery === "object") {
                    assessmentModel.delivery.id = response.assessment_model.delivery.id;
                    assessmentModel.delivery.del_path = response.assessment_model.delivery.del_path;
                    assessmentModel.delivery.del_user_id = response.assessment_model.delivery.del_user_id;
                    assessmentModel.delivery.del_assessment_id = response.assessment_model.delivery.del_assessment_id;
                    $("#web-link").val(assessmentModel.delivery.del_path);
//                    alert('hi');
                }

                //consoleAssessmentModel();
            }

            if (response.warnings) {

                for (var i = 0; i < response.warnings.length; i++) {

                    if (response.warnings[i] == returnCode.EXISTS) {

                        $(".as-builds .as-build-1 .as-name .name").css("border-color", "brown");
                    }
                }
            }

            if (callback) {
                callback(true);
            }
            else if (callback) {
                callback(false);
            }

            if (!silent_flag) {

                alert(response.message);
            }
            if(type =='getAssessmentModel') {
                getAssessmentData(false);
            }
        });
    };





    $(document).ready(function () {

        if ($(".as-build-1 .type select").children().length > 0) {

            assessmentModel.details.typeID = $(".as-build-1 .type select").val().trim();
        }
    });

    showQuestionEditorAdvanced = function (axis, index) {
        var next = index+1;

        var header = "<span style='color:red'>Warning</span>";
        var body = "<label>Score must not be greater than 100</label>";
        var footer = "<input type='button' class='btn btn-default' value='Cancel' data-dismiss='modal'/>";

        var modal = new Modal(header,footer,body);
        console.log('total scores --'+totalScores(axis));
        if(totalScores(axis)>100) {
            modal.showModal("myModal");
            return;
        }
        if(next>assessmentModel.questions[axis].length && axis =="Y")  {
            if(!validateQuestionsScored("X") || !validateQuestionsScored("Y")) {
                selectASScoring(3);
                selectASAxisScores(1,'back');
                return;
            }
            else{
//            onAdvancedQuestionEditorSaveClicked(axis,index);
                selectASStep(6);
                return ;
            }
        }

        if(next>assessmentModel.questions[axis].length ) {
            axis = "Y";
            index = 0;
            next = index+1;
        }



        console.log('x axis length -- '+ assessmentModel.questions[axis].length);
        var asScoring = $(".as-builds .as-build.active .as-scoring.as-scoring-4");
        asScoring.find(".as-question-editor-advanced").attr("class", "col-xs-12 as-question-editor-advanced as-question-editor-advanced-" + axis + "-" + index);
        asScoring.find(".as-question-editor-advanced > .as-title").html("Score the " + axis + " Axis - Question #" + (index + 1));
        asScoring.find(".as-question-editor-advanced > .as-content > .as-question-edit .as-number").html("Question " + (index + 1) + ":");
        asScoring.find(".as-question-editor-advanced > .as-content > .as-question-edit .as-text").val(assessmentModel.questions[axis][index].text);
        asScoring.find(".as-question-editor-advanced > .as-content > .as-foot .as-points .as-min").val(assessmentModel.questions[axis][index].min);
        asScoring.find(".as-question-editor-advanced > .as-content > .as-foot .as-points .as-max").val(assessmentModel.questions[axis][index].max);
        asScoring.find(".as-question-editor-advanced .as-control .as-save").attr("onclick", "onAdvancedQuestionEditorSaveClicked('" + axis + "', " + index + ");");
        asScoring.find(".as-question-editor-advanced .as-control .as-next").attr("onclick", "showQuestionEditorAdvanced('"+axis+"', "+next +");");

        if(next >1){
//        $("#back_question").css("display", "none");
//        $("#back_question").css("display", "block");
            var back = index-1;
            $("#back_question").attr("onclick", "showQuestionEditorAdvanced('"+axis+"', "+back +");");
//        onAdvancedQuestionEditorSaveClicked(axis,index);


        }

        if(next == 1 && axis =='Y') {
            back = 0;
            $("#back_question").attr("onclick", "showQuestionEditorAdvanced('"+axis+"', "+back +");");
//        onAdvancedQuestionEditorSaveClicked(axis,index);

        }

        var asAnswers = asScoring.find(".as-question-editor-advanced > .as-content > .as-answers");
        asAnswers.html("");
        $(".as-steps.as-main .as-step-2 .title").popover("hide");

        var answersModel = assessmentModel.questions[axis][index].answers;

        /*
         * commented here..
         * */
        console.log('account type --'+$("#account_type").val());

        var acc_type = $("#account_type").val();

        for (var i = 0; i < answersModel.length; i++) {
            if(acc_type == 'free' || acc_type == 'Koalafy Joey') {
                asAnswers.append('<div class="as-answer-edit as-answer-edit-' + i + ' col-xs-12">' +
                    '<div class="col-md-2 as-score" style="padding-right: 0px;">' +
                    '<input class="col-xs-12" type="text" value="' + answersModel[i].score + '" onkeyup="validateMinMax(\'' + axis + '\', ' + index + ', this);" disabled/>' +
                    '</div>' +
                    '<div class="col-md-9 as-text">' +
                    '<input class="col-xs-12" type="text" value="' + answersModel[i].text + '" />' +
                    '</div>' +
                    '<div class="col-md-1" style="text-align: center; height: 26px;">' +
                    '<span class="glyphicon glyphicon-plus" style="vertical-align: middle; color: green; cursor: pointer; margin-right: 5px;"></span>' +
                    '<span class="glyphicon glyphicon-remove-circle" style="vertical-align: middle; color: red; cursor: pointer;"></span>' +
                    '</div>' +
                    '</div>');
            }
            else{
                asAnswers.append('<div class="as-answer-edit as-answer-edit-' + i + ' col-xs-12">' +
                    '<div class="col-md-2 as-score" style="padding-right: 0px;">' +
                    '<input class="col-xs-12" type="text" value="' + answersModel[i].score + '" onkeyup="validateMinMax(\'' + axis + '\', ' + index + ', this);" />' +
                    '</div>' +
                    '<div class="col-md-9 as-text">' +
                    '<input class="col-xs-12" type="text" value="' + answersModel[i].text + '" />' +
                    '</div>' +
                    '<div class="col-md-1" style="text-align: center; height: 26px;">' +
                    '<span class="glyphicon glyphicon-plus" style="vertical-align: middle; color: green; cursor: pointer; margin-right: 5px;"></span>' +
                    '<span class="glyphicon glyphicon-remove-circle" style="vertical-align: middle; color: red; cursor: pointer;"></span>' +
                    '</div>' +
                    '</div>');
            }

        }

        selectASScoring(4);
    }

    onAdvancedQuestionEditorSaveClicked = function (axis, index) {

        if (!validateMinMax(axis, index)) {

            alert("Please check data.");
            return;
        }

        var asScoring = $(".as-builds .as-build.active .as-scoring.as-scoring-4");
        assessmentModel.questions[axis][index].text = asScoring.find(".as-question-editor-advanced > .as-content > .as-question-edit .as-text").val().trim();

        var answersModel = assessmentModel.questions[axis][index].answers;
        var answers_arr = [];
        for (var i = 0; i < answersModel.length; i++) {
            var score = parseInt(asScoring.find(".as-question-editor-advanced > .as-content > .as-answers > .as-answer-edit-" + i + " .as-score input").val().trim());
            answersModel[i].score = score;
            answersModel[i].text = asScoring.find(".as-question-editor-advanced > .as-content > .as-answers > .as-answer-edit-" + i + " .as-text input").val().trim();
            answers_arr.push(score);
        }

        setQuestionScored(axis, index);

        drawAxisPotentialResults(axis);

        repaintQuestion(axis, index, true, true, true);
        var max = Math.max.apply(Math,answers_arr);
        var min = Math.min.apply(Math,answers_arr);

        assessmentModel.questions[axis][index].max = max;
        assessmentModel.questions[axis][index].min = min;
        redrawScores(axis);
        alert("Question has been saved.");
        saveAssessment(true,'none');
    }

    isQuestionScored = function (axis, q_index) {

        var result = false;

        var answersModel = assessmentModel.questions[axis][q_index].answers;

        for (var i = 0; i < answersModel.length; i++) {

            if (answersModel[i].score > 0) {

                result = true;
                break;
            }
        }

        return result;
    }

    validateQuestionsScored = function (axis) {
        var result = true;
        console.log('question length '+assessmentModel.questions[axis].length+" for "+axis);

        for(var i=0;i<assessmentModel.questions[axis].length;i++ ) {
            var questionModel = assessmentModel.questions[axis][i];
            var answers = questionModel.answers;
            console.log('answer length  '+answers.length+" for "+axis);

            for(var j=0;j<answers.length;j++){
                console.log('score for '+answers[j].score);

                if(answers[j].score>0){
                    result = true;
                }
                else{
                    result = false;
                }

            }
        }
        return result;
    }

    setQuestionScored = function (axis, q_index) {

        var asScoreQuestion = null;

        if (axis === "X") {
            asScoreQuestion = $(".as-builds .as-build-2 .as-scoring.as-scoring-3 .as-axis-scores-1 .as-score-questions .as-score-question-" + q_index + " .as-content");
        } else if (axis === "Y") {
            asScoreQuestion = $(".as-builds .as-build-2 .as-scoring.as-scoring-3 .as-axis-scores-2 .as-score-questions .as-score-question-" + q_index + " .as-content");
        }

        if (isQuestionScored(axis, q_index)) {

            asScoreQuestion.addClass("scored");
        } else {

            asScoreQuestion.removeClass("scored");
        }
    }

    redrawScores = function (axis) {

        var total = 0, scoredQuestions = 0,scores = 0;
        total = (100 - total) / (assessmentModel.questions[axis].length - scoredQuestions);
        var acc_type = $('#account_type').val();

        /*for (var i = 0; i < assessmentModel.questions[axis].length; i++) {

         if (isQuestionScored(axis, i)) {

         //            total += assessmentModel.questions[axis][i].max;
         //            scoredQuestions++;
         assessmentModel.questions[axis][i].max = Math.floor(total);

         }
         }*/

        if(acc_type =='free' || acc_type == 'Koalafy Joey') {
            for (var i = 0; i < assessmentModel.questions[axis].length; i++) {

                assessmentModel.questions[axis][i].max = Math.floor(total);
            }
        }
        else {
            var sub_total = 0;
            for (var i = 0; i < assessmentModel.questions[axis].length; i++) {
                if (!isQuestionScored(axis, i)) {
                    total = (100 - sub_total) / (assessmentModel.questions[axis].length - scoredQuestions);
                    assessmentModel.questions[axis][i].max = Math.floor(total);
                }
                else{
                    sub_total += assessmentModel.questions[axis][i].max;
                    scoredQuestions++;
                }
                console.log('sub total --'+sub_total)
            }
        }

        var asScoreQuestions = null;

        if (axis === "X") {
            asScoreQuestions = $(".as-builds .as-build-2 .as-scoring-3 .as-axis-scores-1 .as-score-questions");
        } else if (axis === "Y") {
            asScoreQuestions = $(".as-builds .as-build-2 .as-scoring-3 .as-axis-scores-2 .as-score-questions");
        }

        if(acc_type == 'free' || acc_type =='Koalafy Joey' ) {
            calculateScore(axis);
        }

        for (var i = 0; i < assessmentModel.questions[axis].length; i++) {

            asScoreQuestions.find(".as-score-question-" + i + " .as-points .as-min").val(assessmentModel.questions[axis][i].min);
            asScoreQuestions.find(".as-score-question-" + i + " .as-points .as-max").val(assessmentModel.questions[axis][i].max);
        }

        drawAxisMinMax(axis);
    }


    calculateScore  = function(axis) {
        for(var i=0;i<assessmentModel.questions[axis].length;i++) {
            var questionModel = assessmentModel.questions[axis][i];
            if(questionModel) {
                var tot_score = 0,score=0;
                for(var j=0;j<questionModel.answers.length;j++) {

                    var maxx = questionModel.max;

                    if(j == 1 && questionModel.answers.length == 2) {
                        tot_score = maxx;
                        questionModel.answers[j].score = Math.floor(tot_score);

                    }
                    else if( j==1 && questionModel.answers.length > 2 ) {
                        score = maxx/(questionModel.answers.length-1);
                        tot_score = tot_score+score;
//                alert(tot_score);
                        questionModel.answers[j].score = Math.floor(tot_score);

                    }
                    else{
                        tot_score = tot_score+score;
                        questionModel.answers[j].score = Math.ceil(tot_score);
                    }

                }
            }
        }
    }

    validateMax = function (axis, index, element) {

        var result = false;

        if ($(element).val().trim().length <= 0 || isNaN($(element).val().trim())) {
            $(element).css("border-color", "red");
            return result;
        } else {
            $(element).css("border-color", "black");
        }

        var total = 0;

        for (var i = 0; i < assessmentModel.questions[axis].length; i++) {

            if (index == i) {
                continue;
            }

            total += assessmentModel.questions[axis][i].max;
        }

        total += parseInt($(element).val().trim());

        if (total > 100) {
            $(element).css("border-color", "red");
        } else {
            $(element).css("border-color", "black");
            result = true;
            assessmentModel.questions[axis][index].max = parseInt($(element).val().trim());
        }

        return result;
    }

    validateMin = function (axis, index, element) {

        var result = false;

        if ($(element).val().trim().length <= 0 || isNaN($(element).val().trim())) {
            $(element).css("border-color", "red");
            return result;
        } else {
            $(element).css("border-color", "black");
        }

        var total = 0;

        for (var i = 0; i < assessmentModel.questions[axis].length; i++) {

            if (index == i) {
                continue;
            }

            total += assessmentModel.questions[axis][i].min;
        }

        total += parseInt($(element).val().trim());

        if (total < 1) {
            $(element).css("border-color", "red");
        } else {
            $(element).css("border-color", "black");
            result = true;
            assessmentModel.questions[axis][index].min = parseInt($(element).val().trim());
        }

        return result;
    }

    validateMinMax = function (axis, index, element) {

        var result = false;

        if (element) {
            if ($(element).val().trim().length <= 0 || isNaN($(element).val().trim())) {
                $(element).css("border-color", "red");
                return result;
            } else {
                $(element).css("border-color", "#bbb");
            }
        }

        var min = 0;
        var max = 0;

        $(".as-steps.as-main .as-step-2 .title").popover("hide");

        var asAnswers = $(".as-builds .as-build.active .as-scoring.as-scoring-4 .as-question-editor-advanced > .as-content > .as-answers");
        var answers_arr = [];
        var sub_total = 0;
        for (var i = 0; i < asAnswers.children().length; i++) {

            var score = asAnswers.find(".as-answer-edit-" + i + " .as-score input").val().trim();
            if (score.length <= 0 || isNaN(score)) {
                asAnswers.find(".as-answer-edit-" + i + " .as-score input").css("border-color", "red");
                if (!element) {
                    return false;
                }
            } else {
                asAnswers.find(".as-answer-edit-" + i + " .as-score input").css("border-color", "#bbb");
                sub_total = parseInt(score);
                sub_total = sub_total+parseInt(score);
                answers_arr.push(score);
                if (score < min) {
                    min = score;
                }
                if (score > max) {
                    max = score;
                }

            }
        }
        if(isQuestionScored(axis,index)) {
            sub_total = totalScores(axis)-parseInt(assessmentModel.questions[axis][index].max);
        }
        else{
            sub_total = totalScores(axis);
        }

        var total = sub_total+Math.max.apply(Math,answers_arr);
        console.log('total -- '+total);
        console.log('sub total -- '+sub_total);

        if ( total > 100) {
            if (element) {
                $(element).css("border-color", "red");
                var message = "Maximum score should not be greater than 100 ";
                $(".modal-title").html("<span style='color:red'>Warning</span>");
                $(".modal-body").html(message);
                $(".modal-footer").html("<input type='button' class='btn btn-default' value='Cancel' data-dismiss='modal'/>");
                $("#myModal").modal("show");

            }

        } else {
            if (element) {
                $(element).css("border-color", "#bbb");
            }

            result = true;
        }

        if(answers_arr.length>0 ) {
            $('.as-content .as-foot .as-points .as-min').val(Math.min.apply(Math,answers_arr));
            $('.as-content .as-foot .as-points .as-max').val(Math.max.apply(Math,answers_arr));

        }

        return result;
    }

    $(document).ready(function () {
        getAssessmentData(true);
    });

    getAssessmentData = function(isUpdate){
        var assessmentID = "";

        if ($(location).attr("href").lastIndexOf("assessment=") > -1) {
            assessmentID = $(location).attr("href").substr($(location).attr("href").lastIndexOf("=") + 1);
            assessmentID = assessmentID.endsWith("#") ? assessmentID.substr(0, assessmentID.length - 1) : assessmentID;
        }

        if (assessmentID.length > 0) {

            $.get("../../services/AssessmentService.php", {service_function: "getAssessmentModel", assessment_id: assessmentID}, function (response, status) {

                console.log(response);
                response = JSON.parse(response);

                if (response.success_flag) {
                    assessmentModel = response.assessment_model;
                    console.log('after respone getAssessment Model -- '+JSON.stringify(assessmentModel));

                    if (assessmentModel.questions) {

                        for (var axis in assessmentModel.questions) {

                            for (var i = 0; i < assessmentModel.questions[axis].length; i++) {

                                assessmentModel.questions[axis][i].jsToken = saveJSToken(25);

                                if (assessmentModel.questions[axis][i].answers) {

                                    for (var j = 0; j < assessmentModel.questions[axis][i].answers.length; j++) {

                                        assessmentModel.questions[axis][i].answers[j].jsToken = saveJSToken(25);
                                    }
                                }
                            }
                        }
                    }

                    if (assessmentModel.stages) {

                        for (var i = 0; i < assessmentModel.stages.length; i++) {

                            assessmentModel.stages[i].jsToken = saveJSToken(25);
                        }
                    }

                    if (assessmentModel.matrix_points) {

                        for (var axis in assessmentModel.matrix_points) {

                            for (var i = 0; i < assessmentModel.matrix_points[axis].length; i++) {

                                assessmentModel.matrix_points[axis][i].jsToken = saveJSToken(25);
                            }
                        }
                    }

                    if(assessmentModel.template) {
                        assessmentModel.template.jsToken = saveJSToken(25);
                    }

                    if(assessmentModel.setting) {
                        assessmentModel.setting.jsToken = saveJSToken(25);
                    }

                    if(assessmentModel.title) {
                        assessmentModel.title.jsToken = saveJSToken(25);
                    }

                    if(assessmentModel.logo) {
                        assessmentModel.logo.jsToken = saveJSToken(25);
                    }

                    if(assessmentModel.page) {
                        assessmentModel.page.jsToken = saveJSToken(25);
                    }

                    if(assessmentModel.footer) {
                        assessmentModel.footer.jsToken = saveJSToken(25);
                    }

                    if(assessmentModel.layout) {
                        assessmentModel.layout.jsToken = saveJSToken(25);
                    }



                    if(isUpdate) {
                        showVideo(1);
                        repaintAssessmentModel();
                    }
                    else{
//                        updateQuestionList();
                    }

                }
            });
        } else {

            insertNewStage(0);
        }
    }

    onDetailsNextClicked = function () {

        validateAssessmentDetails(function (valid) {
//        alert(valid);
            if (valid) {

                $(".as-steps .as-step").removeClass("selected");
                $(".as-steps .as-step-3").addClass("selected");
                $(".as-builds .as-build").removeClass("active");
                $(".as-builds .as-build-3").addClass("active");
                showVideo(2);
            }
        });
    }

    redrawQuestionIndexes = function (axis) {

        var asQuestions = null, asScoreQuestions = null;

        if (axis === "X") {

            asQuestions = $(".as-builds .as-build-3 .as-questions .as-question, .as-builds .as-build-3 .as-questions .as-question-builder");
            asScoreQuestions = $(".as-builds .as-build.as-build-2 .as-scoring.as-scoring-3 .as-axis-scores.as-axis-scores-1 .as-score-questions .as-score-question");
        } else if (axis === "Y") {

            asQuestions = $(".as-builds .as-build-4 .as-questions .as-question, .as-builds .as-build-4 .as-questions .as-question-builder");
            asScoreQuestions = $(".as-builds .as-build.as-build-2 .as-scoring.as-scoring-3 .as-axis-scores.as-axis-scores-2 .as-score-questions .as-score-question");
        }

        for (var i = 0; i < asQuestions.length; i++) {

            var asQuestion = asQuestions.eq(i);

            if (asQuestion.hasClass("as-question-builder")) {

                asQuestion.attr("class", "col-xs-12 as-question-builder as-question-editor as-edit as-edit-" + i + " as-question-builder-" + i);
                asQuestion.find(".as-question-edit .as-title").html("Question " + (i + 1) + ":");
                asQuestion.find(".as-control .as-save").attr("onclick", "onSaveQuestionClicked('" + axis + "', " + i + ");");
                asQuestion.find(".as-control .as-delete").attr("onclick", "onDeleteQuestionEditorClicked('" + axis + "', " + i + ");");
                asQuestion.find(".as-control .as-cancel").attr("onclick", "onCancelQuestionEditorClicked('" + axis + "', " + i + ");");
            } else if (asQuestion.hasClass("as-question")) {

                asQuestion.attr("class", "col-xs-12 as-question as-question-" + i);
                asQuestion.find(".as-text").html((i + 1) + ". " + assessmentModel.questions[axis][i].text);
                asQuestion.find(".as-edit").attr("onclick", "editQuestion('" + axis + "', " + i + ");");
            }

            asScoreQuestions.eq(i).attr("class", "col-xs-12 as-score-question as-score-question-" + i);
            asScoreQuestions.eq(i).find(".as-points .as-min").attr("onkeyup", "if(validateMin('" + axis + "', " + i + ", this)) { drawAxisMinMax(" + axis + "); };");
            asScoreQuestions.eq(i).find(".as-points .as-max").attr("onkeyup", "if(validateMax('" + axis + "', " + i + ", this)) { drawAxisMinMax(" + axis + "); };");
            asScoreQuestions.eq(i).find(".as-edit").attr("onclick", "if($(this).prop('checked')) {showQuestionEditorAdvanced('" + axis + "', " + i + ");}");
            asScoreQuestions.eq(i).find(".as-content p").html((i + 1) + ". " + assessmentModel.questions[axis][i].text);

            redrawAnswerIndexes(axis, i);
        }
    }

    repaintAssessmentModel = function () {

        $(".as-builds .as-build-1 .as-helper input[name='a-helper']").prop("checked", false);
        $(".as-builds .as-build-1 .as-custom-category input[type='checkbox']").prop("checked", true);
        $(".as-builds .as-build-1 .as-x-category input[type='text']").val(assessmentModel.details.x_category);
        $(".as-builds .as-build-1 .as-y-category input[type='text']").val(assessmentModel.details.y_category);
        switchCategoryControl(document.querySelector(".as-builds .as-build-1 .as-custom-category input[type='checkbox']"));

        if (assessmentModel.questions) {

            for (var axis in assessmentModel.questions) {

                for (var i = 0; i < assessmentModel.questions[axis].length; i++) {

                    insertQuestion(axis, i);

                    insertScoreQuestion(axis, i);

                    setQuestionScored(axis, i);
                }

                drawAxisMinMax(axis);
            }
        }

        if (assessmentModel.stages) {

            for (var i = 0; i < assessmentModel.stages.length; i++) {

                insertStage(i);
            }
        }

        if (assessmentModel.stages.length <= 0) {

            insertNewStage(0);
        }

        redrawMatrix();
    }

    selectASMatrix = function (no) {

        $(".as-builds .as-build-6 .as-matrixes .as-matrix").removeClass("active");
        $(".as-builds .as-build-6 .as-matrixes .as-matrix-" + no).addClass("active");
        if(no == 2) {
            drawAxisPotentialResults("Y");
            drawAxisPotentialResults("X");
        }
    }

    validateAssessmentDetails = function (callback)
    {
        /*if (assessmentModel.details.name.trim().length < 1 || assessmentModel.details.name.trim().length > 255) {

         $(".as-builds .as-build-1 .as-name .name").css("border-color", "red");
         //        $('.as-builds .as-build-1 .as-name .name').attr("data-original-title", "Error!");
         $('.as-builds .as-build-1 .as-name .name').attr("data-original-title", "Error!<button id='popovercloseid' type='button' onclick=onPopUpClosed('name','') class='close' >&times;</button>");
         $('.as-builds .as-build-1 .as-name .name').attr("data-content", "Assessment name should have length between 1 to 255 characters.");
         //        $('.popover-title').html('hello');
         //        $('.as-builds .as-build-1 .as-name .name').attr("data-original-title", "Error!<button id='popovercloseid' type='button' class='close'>&times;</button>");
         $('.as-builds .as-build-1 .as-name .name').popover("show");

         valid = false;
         }*/

        if (assessmentModel.details.x_category.trim().length < 1 || assessmentModel.details.x_category.trim().length > 255) {

            if ($(".as-build.active .as-helper input[name='a-helper']").prop("checked")) {
                var id = encodeURI('.x-category select');
                $(".as-build.active .x-category select").css("border-color", "red");
                $('.as-build.active .x-category select').attr("data-original-title", "Error! <button id='popovercloseid' type='button' onclick=onPopUpClosed('x-category','select') class='close' >&times;</button>");
                $('.as-build.active .x-category select').attr("data-content", "X category name should have length between 1 to 255 characters.");

                $(".as-build.active .x-category select").popover("show");
            } else {

                var id = encodeURI('.x-category input');
                $(".as-build.active .x-category input").css("border-color", "red");
                $('.as-build.active .x-category input').attr("data-original-title", "Error! <button id='popovercloseid' type='button' onclick=onPopUpClosed('x-category','input') class='close' >&times;</button>");
                $('.as-build.active .x-category input').attr("data-content", "X category name should have length between 1 to 255 characters.");
                $(".as-build.active .x-category input").popover("show");
            }

            valid = false;
        }

        else if (assessmentModel.details.y_category.trim().length < 1 || assessmentModel.details.y_category.trim().length > 255) {

            if ($(".as-build.active .as-helper input[name='a-helper']").prop("checked")) {
                var id = encodeURI('.y-category select');

                $(".as-build.active .y-category select").css("border-color", "red");
                $('.as-build.active .y-category select').attr("data-original-title", "Error!<button id='popovercloseid' type='button' onclick=onPopUpClosed('y-category','select') class='close' >&times;</button>");
                $('.as-build.active .y-category select').attr("data-content", "Y category name should have length between 1 to 255 characters.");
                $(".as-build.active .y-category select").popover("show");
            } else {
                var id = encodeURI('.y-category select');
                $(".as-build.active .y-category input").css("border-color", "red");
                $('.as-build.active .y-category select').attr("data-original-title", "Error!<button id='popovercloseid' type='button' onclick=onPopUpClosed('y-category','input') class='close' >&times;</button>");
                $('.as-build.active .y-category input').attr("data-content", "Y category name should have length between 1 to 255 characters.");
                $(".as-build.active .y-category input").popover("show");
            }

            valid = false;
        }
        else if (assessmentModel.details.x_category.trim().length > 0 && assessmentModel.details.x_category.trim().length <= 255 && assessmentModel.details.y_category.trim().length > 0 && assessmentModel.details.y_category.trim().length <= 255 && assessmentModel.details.x_category.trim() === assessmentModel.details.y_category.trim()) {

            if ($(".as-build.active .as-helper input[name='a-helper']").prop("checked")) {

                $(".as-build.active .x-category select").css("border-color", "brown");

                $(".as-build.active .y-category select").css("border-color", "brown");
                $('.as-build.active .y-category select').attr("data-original-title", "Warning!");
                $('.as-build.active .y-category select').attr("data-content", "X and Y category names can not be same.");
                $(".as-build.active .y-category select").popover("show");
            } else {

                $(".as-build.active .x-category input").css("border-color", "brown");

                $(".as-build.active .y-category input").css("border-color", "brown");
                $('.as-build.active .y-category input').attr("data-original-title", "Warning!");
                $('.as-build.active .y-category input').attr("data-content", "X and Y category names can not be same.");
                $(".as-build.active .y-category input").popover("show");
            }

            valid = false;
        }
        else
        {
            valid = true;
        }

        if (!valid) {

            callback(valid);
            return;
        }

        $.post("../../services/AssessmentService.php", {service_function: "validateAssessmentModel", assessment_model: assessmentModel}, function (response, status) {

            var valid = true;

            response = JSON.parse(response);

            if (response.warnings) {

                for (var i = 0; i < response.warnings.length; i++) {

                    if (response.warnings[i] == returnCode.EXISTS) {

                        valid = false;
                        $(".as-builds .as-build-1 .as-name .name").css("border-color", "brown");
//                    $('.as-builds .as-build-1 .as-name .name').attr("data-original-title", "Warning!");
//                    $('.as-builds .as-build-1 .as-name .name').attr("data-content", "An assessment with this name already exists.");
//                    $('.as-builds .as-build-1 .as-name .name').popover("show");
                    }
                }
            }
            if (response.errors) {

                for (var i = 0; i < response.errors.length; i++) {

                    if (response.errors[i] == returnCode.DUPLICATE) {

                        valid = false;

                        if ($(".as-build.active .as-helper input[name='a-helper']").prop("checked")) {

                            $(".as-build.active .x-category select").css("border-color", "brown");

                            $(".as-build.active .y-category select").css("border-color", "brown");
                            $('.as-build.active .y-category select').attr("data-original-title", "Warning!");
                            $('.as-build.active .y-category select').attr("data-content", "X and Y category names can not be same.");
                            $(".as-build.active .y-category select").popover("show");
                        } else {

                            $(".as-build.active .x-category input").css("border-color", "brown");

                            $(".as-build.active .y-category input").css("border-color", "brown");
                            $('.as-build.active .y-category input').attr("data-original-title", "Warning!");
                            $('.as-build.active .y-category input').attr("data-content", "X and Y category names can not be same.");
                            $(".as-build.active .y-category input").popover("show");
                        }
                    }
                }
            }

            callback(valid);
        });
    }
    var x='';
    var y='';
    axisCategoryChecker = function(axis,object)
    {
        if(axis == "x")
        {
            x = object;
            $("#xdata").val(object);
        }
        else if(axis == "y")
        {
            y = object;
            $("#ydata").val(object);
        }
        if(x == y)
        {
            $(".as-build.active .x-category select").css("border-color", "brown");
            $(".as-build.active .y-category select").css("border-color", "brown");
            $('.as-build.active .y-category select').attr("data-original-title", "Error!<button id='popovercloseid' type='button' onclick=onPopUpClosed('y-category','select') class='close' >&times;</button>");
            $('.as-build.active .y-category select').attr("data-content", "X and Y category names can not be same.");
            $(".as-build.active .y-category select").popover("show");

        }
        else
        {
            onPopUpClosed('y-category','select');
            $(".as-build.active .x-category select").css("border-color", "");
            $(".as-build.active .y-category select").css("border-color", "");

        }
    }
    redrawMatrix = function () {

        $(".as-builds .as-build-6 .as-matrixes .as-matrix-3 h5").html("MATRIX SIZE: " + (assessmentModel.matrix[0] ? assessmentModel.matrix[0].length : "0") + "x by " + assessmentModel.matrix.length + "y");

        var matrixTable = $(".as-builds .as-build-6 .as-matrixes .as-matrix-3 .as-table table");
        matrixTable.html("");

        for (var j = 0; j < assessmentModel.matrix.length; j++) {

            var row = "<td class='as-yy-"+j+"'><input type='text' style='width:40px' onkeyup=updateMatrixPoint(this,'Y','"+j+"','start') value='" + (assessmentModel.matrix_points["Y"][assessmentModel.matrix.length - j - 1].start)  + "' class='as-y-0' />" +
                "<input type='text' style='width:40px' id='yy-"+j+"' onkeyup=updateMatrixPoint(this,'Y','"+j+"','end') value='" + (assessmentModel.matrix_points["Y"][assessmentModel.matrix.length - j - 1].end)  + "' class='as-y-1' /></td>";

            for (var i = 0; i < assessmentModel.matrix[j].length; i++) {

                var abbr = "";

                if (assessmentModel.matrix[j][i].length > 0) {

                    for (var k = 0;k < assessmentModel.stages.length;k++) {

                        if(assessmentModel.matrix[j][i] === assessmentModel.stages[k].id) {

                            abbr = assessmentModel.stages[k].abbreviation;
                        }
                    }
                }

                row += '<td class="as-col" ondragover="event.preventDefault();" ondrop="event.preventDefault();jQuery(event.target).html(event.dataTransfer.getData(\'abbr\'));assessmentModel.matrix[' + j + '][' + i + '] = event.dataTransfer.getData(\'id\');">' + abbr + '</td>';
            }

            row = "<tr>" + row + "</tr>";

            matrixTable.append(row);
        }

        if (assessmentModel.matrix[0]) {

            var row = "<td></td>";

            for (var i = 0; i < assessmentModel.matrix[0].length; i++) {

                row += '<td class="as-x-'+i+'"><input style="width:40px" type="text" onkeyup=updateMatrixPoint(this,"X","'+i+'","start") value="' + (assessmentModel.matrix_points["X"][i].start) +'" class="as-x-0" />' +
                    '<input style="width:40px" type="text"  onkeyup=updateMatrixPoint(this,"X","'+i+'","end") value="' + (assessmentModel.matrix_points["X"][i].end) +'"  class="as-x-1" /></td>';
            }

            row = "<tr>" + row + "</tr>";

            matrixTable.append(row);
        }

        $(".as-builds .as-build-6 .as-matrixes .as-matrix-3 .as-table input[class='as-x']").inputmask("9{1,2}-9{1,3}", { "oncomplete": function() {

            var value = $(this).val();

            var start = parseInt(value.substr(0, value.indexOf("-")));
            var end = parseInt(value.substr(value.indexOf("-") + 1));

            if(end < 1 || end < start || end > 100) {
                $(this).css("border-color", "red");
                return;
            } else {
                $(this).css("border-color", "");
            }

            var index = $(".as-builds .as-build-6 .as-matrixes .as-matrix-3 .as-table table tr:last-child td").index($(this).parent("td")) - 1;

//        assessmentModel.matrix_points["X"][index].start = start;
//        assessmentModel.matrix_points["X"][index].end = end;
        }
        });

        $(".as-builds .as-build-6 .as-matrixes .as-matrix-3 .as-table input[class='as-y']").inputmask("9{1,2}-9{1,3}", { "oncomplete": function() {

            var value = $(this).val();

            var start = parseInt(value.substr(0, value.indexOf("-")));
            var end = parseInt(value.substr(value.indexOf("-") + 1));

            if(end < 1 || end < start || end > 100) {
                $(this).css("border-color", "red");
                return;
            } else {
                $(this).css("border-color", "");
            }

            var index = $(".as-builds .as-build-6 .as-matrixes .as-matrix-3 .as-table table tr").length - 2 - $(".as-builds .as-build-6 .as-matrixes .as-matrix-3 .as-table table tr").index($(this).parents("tr"));

//        assessmentModel.matrix_points["Y"][index].start = start;
//        assessmentModel.matrix_points["Y"][index].end = end;
        }
        });
//     alert('account type -- '+$('#account_type').val());

        if($('#account_type').val() == 'free' ||$('#account_type').val()=='Koalafy Joey' )
            $(".as-builds .as-build-6 .as-matrixes .as-matrix-3 .as-table input").attr("readonly", systemWorkMode !== "Development" && !transactionModel.weighting);
    }

    findAxisPotentialResults = function(axis) {

        var indexes = [];

        for(var i in assessmentModel.questions[axis]) {

            indexes.push(0);
        }

        var results = [];

        var sum = 0;

        for(var i = 0;i < indexes.length;i++) {
//        alert(indexes[i]);
            if(indexes[i] < assessmentModel.questions[axis][i].answers.length) {

                sum += assessmentModel.questions[axis][i].answers[indexes[i]].score;
            }

            if(i == indexes.length - 1) {

                if(results.indexOf(sum) == -1) {

                    results.push(sum);
                }

                for(var j = assessmentModel.questions[axis].length - 1;j >= 0;j--) {

                    if(indexes[j] == assessmentModel.questions[axis][j].answers.length - 1) {

                        indexes[j] = 0;
                    } else {

                        indexes[j]++;
                        i = -1;
                        sum = 0;
                        break;
                    }
                }
            }
        }

        results.sort( function(a,b){return a-b;});

        return  results;
    }

    themeSelected = function(index,isUpdate) {
//    alert(index);
        for(var i=0; i<templates.length;i++) {
            if(i == index) {
                if(!($("#selected-"+i).hasClass("active")))
                    $("#selected-"+i).addClass("active");
                var item = templates[i];
                //alert(item.temp_titleback_color);
                $(".assessment-page-title").css("background-color",item.temp_titleback_color);
                $("#ass-title").css("color",item.temp_titletext_color);
                $(".assessment-footer").css("background-color",item.temp_footerback_color);
                $("#page_footer").css("color",item.temp_footer_color);
//                console.log('hi -- '+item.temp_footer_color);
                $(".page-area-child").css("background-color",item.temp_pageback_color);

//            $(".page-area-child").css("margin-left",item.temp_pageback_left);
//            $(".page-area-child").css("margin-right",item.temp_pageback_right);
                $(".page-area-child").css("height","265px");
                $("#main-contentt").css("background-color",item.temp_back_color);
                $(".page-area").css("background-color",item.temp_back_color);
                $("#description_d").css("background-color",item.temp_back_color);

                $("#page-text").css("color",item.temp_pagetext_color);
                $(".question-text").css('color',item.temp_pagetext_color);
                $(".answer-text").css('color',item.temp_pagetext_color);
                $(".answer-text").css('background-color',item.temp_back_color);
                //$(".question-header").css('background-color',item.temp_back_color);


//                alert(item.temp_pagetext_color);

                if(item.temp_logo_align == 'left') {

//      $(".assessment-logo").addClass("pull-left");
                    /*
                     $('.assessment-page-title').css('flex-direction','row');
                     $('.assessment-page-title').css('-webkit-flex-direction','row');
                     $('#ass-logo-container').css('text-align','left');
                     $('#ass-title').css('text-align','right');
                     */
                    $('#ass-logo').removeClass('pull-right');
                    $('#ass-logo').removeClass('pull-left');
                    $('#ass-logo').addClass('pull-left');
                    $('#logo-position').val('left');

                    $('#ass-title').removeClass('pull-right');
                    $('#ass-title').removeClass('pull-left');
                    $('#ass-title').addClass('pull-right');
                    $('#font-align').val('right');
                    if(isUpdate) {
                        assessmentModel.title.title_alignment = 'right';
                        assessmentModel.logo.logo_position = 'left';
                    }
                }
                else if(item.temp_logo_align == 'right') {

//      $(".assessment-logo").addClass("pull-right");
                    /*$('.assessment-page-title').css('flex-direction','row-reverse');
                     $('.assessment-page-title').css('-webkit-flex-direction','row-reverse');
                     $('#ass-logo-container').css('text-align','right');
                     $('#ass-title').css('text-align','left');
                     */
                    $('#ass-logo').removeClass('pull-right');
                    $('#ass-logo').removeClass('pull-left');
                    $('#ass-logo').addClass('pull-right');
                    $('#logo-position').val('right');

                    $('#ass-title').removeClass('pull-right');
                    $('#ass-title').removeClass('pull-left');
                    $('#ass-title').addClass('pull-left');
                    $('#font-align').val('left');

                    if(isUpdate) {
                        assessmentModel.title.title_alignment = 'left';
                        assessmentModel.logo.logo_position = 'right';
                    }

                }
                //alert(JSON.stringify(item));
                if(isUpdate) {
                    assessmentModel.template.temp_id = item.temp_id;
                    assessmentModel.template.name = item.temp_name;

                    assessmentModel.title.title_color = item.temp_titletext_color;
                    assessmentModel.title.title_background = item.temp_titleback_color;
                    assessmentModel.footer.footer_back_color = item.temp_footerback_color;
                    assessmentModel.footer.footer_text_color = item.temp_footer_color;
                    assessmentModel.page.page_text_color = item.temp_pagetext_color;
                    assessmentModel.page.page_back_color = item.temp_back_color;
                    assessmentModel.background.color = item.temp_back_color;
                    assessmentModel.page.page_is_progress = 'A';
                    assessmentModel.page.page_is_top = 'D';
                    $('#progress').attr('checked',true);
//                    $('#progress_top').attr('checked',true);
                    progressChecked('progress_bottom');
                    saveAssessment(true,'none');

                }
//            alert(assessmentModel.design.template.temp_id);
            }
            else{
                $("#selected-"+i).removeClass("active");
            }
        }
        //    alert(index);
        //consoleAssessmentModel();
    }

    onPopUpClosed  = function(id,idd) {
        if(idd!='') {
            $('.'+id+' '+idd).popover('hide');

        }
        else{
            $('.'+id).popover('hide');
        }

        console.log('id here -- '+id+" idd "+idd);
//    alert('close is called');
    }


    onPopUpClosed1  = function(ref) {
//    alert('close is called');
      $('.as-steps.as-main .as-step-6 .title').popover('hide');
    }

    play_video = function() {
        console.log(videoModel.video_page_link);

        var id= getQueryString('v',videoModel.video_page_link);
        $(".modal-title").html("<span style='color:black'>Video Tutorial</span>");
        $(".modal-title").css("border-bottom","solid 1px lightgrey");
        $(".modal-body").html('<iframe width="560" height="315" src="https://www.youtube.com/embed/'+id+'"  frameborder="0" allowfullscreen></iframe>');
        $(".modal-body").css("height","50%");
        $(".modal-footer").css("display","block");
        if(videoModel.video_page_status) {
            $(".modal-footer").html("<div class='pull-left'><input type='checkbox' class='.checkbox-primary' value='Cancel' onchange='updateVideoView(this)'/>" +
                "&nbsp&nbspDon't show again</div><div class='pull-right'><input type='button' class='btn btn-primary pull-right' value='I am ready lets begin' onclick='onReadyBegin()'/></div>");

        }
        else{
            $(".modal-footer").html("<div class='pull-left'><input type='checkbox' class='.checkbox-primary' value='Cancel' onchange='updateVideoView(this)' checked/>" +
                "&nbsp&nbspDon't show again</div><div class='pull-right'><input type='button' class='btn btn-primary pull-right' value='I am ready lets begin' onclick='onReadyBegin()' /></div>");

        }
        $("#modal_header").css("background-color",'#ffffff');
        $("#myModal").modal("show");

    }

    onReadyBegin = function() {
        $('#myModal').modal('hide');
    }

    updateVideoView = function(ref){
//    alert($(ref).is(":checked"));
        console.log('videoId -- '+videoModel.video_id);
        console.log('user_id -- '+userModel.account_id);

        if($(ref).is(":checked")) {

            /*$.get(url,{'service_function':'updateVideoData','video_id':videoModel.video_id,'user_id':userModel.account_id,'video_view':'0'},function(data){
             console.log(data);
             });*/
            $.ajax({
                url: "../../services/PublicService.php",
                data: {
                    'service_function': "updateVideoData",
                    "video_id":videoModel.video_id,
                    "user_id":userModel.account_id,
                    "video_view":"0"
                },
                cache: false,
                type: "GET",
                dataType : "json"
            }).done(function(json) {
                console.log(json);
            }).fail(function(xhr, status, errorThrown) {
                alert("Sorry, there was a problem!");
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
            });
        }
        else{

            /*$.get(url,{'service_function':'updateVideoData','video_id':videoModel.video_id,'user_id':userModel.account_id,'video_view':'1'},function(data){
             console.log(data);
             });*/

            $.ajax({
                url: "../../services/PublicService.php",
                data: {
                    'service_function': "updateVideoData",
                    "video_id":videoModel.video_id,
                    "user_id":userModel.account_id,
                    "video_view":"1"
                },
                cache: false,
                type: "GET",
                dataType : "json"
            }).done(function(json) {
                console.log(json);
            }).fail(function(xhr, status, errorThrown) {
                alert("Sorry, there was a problem!");
                console.log("Error: " + errorThrown);
                console.log("Status: " + status);
                console.dir(xhr);
            });

        }
    }

    /*
     * find total scores for questions
     * */

    totalScores = function(axis) {
        var total_score = 0;
        for(var i=0;i< assessmentModel.questions[axis].length;i++) {
            var questionModel = assessmentModel.questions[axis][i];
            if(isQuestionScored(axis,i)) {
                total_score = total_score+parseInt(questionModel.max);
            }
        }
        return total_score;
    }

    /*
     * in this function query param will be return ...
     * */
    getQueryStringValue = function(key) {
        return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
    }
    var getQueryString = function ( field, url ) {
        var href = url ? url : window.location.href;
        var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
        var string = reg.exec(href);
        return string ? string[1] : null;
    };

    showVideo = function(id){
        var user_id = userModel.account_id;
        /*$.get("../../services/PublicService.php", {'service_function': "updateVideoData","video_id":id,"user_id":user_id,"video_view":""}, function (response, status) {
         response = JSON.parse(response);

         if (response.success_flag) {
         videoModel = response.video[0];

         //               alert(getQueryString('v',videoModel.video_page_link));
         //            alert(videoModel.video_page_status);
         if(videoModel.video_page_status)
         play_video();
         }
         });*/

        $.ajax({
            url: "../../services/PublicService.php",
            data: {
                'service_function': "updateVideoData",
                "video_id":id,
                "user_id":user_id,
                "video_view":""
            },
            cache: false,
            type: "GET",
            dataType : "json"
        }).done(function(response) {
//        var response = JSON.parse(json);

            if (response.success_flag) {
                videoModel = response.video[0];
                if(videoModel.video_page_status)
                    play_video();
            }}).fail(function(xhr, status, errorThrown) {
            alert("Sorry, there was a problem!");
            console.log("Error: " + errorThrown);
            console.log("Status: " + status);
            console.dir(xhr);
        });

    }

    category_select_x = function(ref) {
        assessmentModel.details.x_category = $(ref).find('option[value=\'' + $(ref).val().trim() + '\']').html();
        $('.as-build.active .y-category select').popover('hide');
        $(this).css('border-color','');
        $('.x-category select').css('border-color', '');
    }

    category_input_x = function(ref) {
        assessmentModel.details.x_category = $(ref).val().trim();
        $(ref).popover('hide');
        $(ref).css('border-color', '');
    }

    category_select_y = function(ref) {
        assessmentModel.details.y_category = $(ref).find('option[value=\'' + $(ref).val().trim() + '\']').html();
        $(ref).popover('hide');
        $(ref).css('border-color','');
        $('.y-category select').css('border-color', '');
    }

    category_input_y = function(ref) {
        assessmentModel.details.y_category = $(ref).val().trim();
        $(ref).popover('hide');
        $(this).css('border-color', '');
    }

    assessmentName = function(ref) {
        assessmentModel.details.name = $(ref).val().trim();
        $(ref).popover('hide');
        $(ref).css('border-color', '');
    }

    score_x = function(ref){
        if($(ref).prop('checked')) {
            selectASScoring(3);selectASAxisScores(1,'back');
        }
    }

    score_y = function(ref){
        if($(ref).prop('checked')) {
            selectASScoring(3);selectASAxisScores(2,'next');
        }
    }
    onDrop_code1 = function(event){
        event.preventDefault();
        $(event.target).html(assessmentModel.stages[parseInt(event.dataTransfer.getData('index'))].abbreviation);
    }

})(jQuery);
