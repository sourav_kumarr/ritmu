
<?php
//error_reporting(0);
session_start();
if(!isset($_SESSION['auth'])){
    header("Location: ../login/login.php");
}

if(isset($_COOKIE['FBID'])) {

    ?>

   <input type="hidden" id="fb_val" value="<?php echo $_COOKIE['FBID']?>"/>
<?php
}
else{
 ?>
    <input type="hidden" id="fb_val" value=""/>
  <?php
}

require_once("../../classes/MySQLiClass.php");
require_once("../../classes/data/AccountDataClass.php");
require_once("../../classes/AccountClass.php");
require_once("../../classes/AssessmentClass.php");
require_once("../../classes/System.php");
require_once("../../classes/ReturnCodeClass.php");
require_once("../../classes/data/AssessmentTypeDataClass.php");
require_once("../../classes/data/AssessmentDataClass.php");

$accountObject = new AccountClass();
$assessment_class = new AssessmentClass();
$userData = $accountObject->getUserDetail(0);
/* if ($accountObject->isLoggedIn() !== true) {

    //header("Location: ../login/login.php");
} */

$systemObject = new System();

$assessment_data_class = null;

if (isset($_REQUEST["assessment"]) && strlen(trim(strval($_REQUEST["assessment"]))) > 0) {

    $assessment_data_class = $assessment_class->getAssessmentData(trim(strval($_REQUEST["assessment"])));
}
$mysqli_obj = new MySQLiClass();
$assessment_class = new AssessmentClass();
$systemObject = new System();

$Id = $_SESSION['account_id'];

$mysqli_obj->connect();
$mysqli_obj->executeQuery("SELECT * FROM `tbl_account` WHERE `account_id` = '$Id'");

if ($row = mysqli_fetch_assoc($mysqli_obj->getResult())) {

    $accountType = $row['account_type'];
    $userName = $row['account_login_name'];
    echo "<input type='hidden' id='account_type' value='".$accountType."'>";
}
?>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html class="js" lang="en-US"><!--<![endif]-->
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="UTF-8">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">

<link rel="pingback" href="../../../xmlrpc.php">

<!--[if lt IE 9]>
<script src="../../../wp-content/themes/Divi/js/html5.js" type="text/javascript"></script>
<script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>
<![endif]-->

<script type="text/javascript">
    document.documentElement.className = 'js';
</script>

<title>Assessment | Koalafy</title>
<link rel="dns-prefetch" href="http://fonts.googleapis.com/">
<link rel="dns-prefetch" href="http://s.w.org/">
<link rel="alternate" type="application/rss+xml" title="Koalafy » Feed" href="../../../feed/">
<link rel="alternate" type="application/rss+xml" title="Koalafy » Comments Feed"
      href="../../../comments/feed/">
<script type="text/javascript">
    window._wpemojiSettings = {"baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/", "ext": ".png", "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/", "svgExt": ".svg", "source": {"concatemoji": "http:\/\/localhost\/xampp\/koalafy\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.6"}};
    !function (a, b, c) {
        function d(a) {
            var c, d, e, f, g, h = b.createElement("canvas"), i = h.getContext && h.getContext("2d"), j = String.fromCharCode;
            if (!i || !i.fillText)return!1;
            switch (i.textBaseline = "top", i.font = "600 32px Arial", a) {
                case"flag":
                    return i.fillText(j(55356, 56806, 55356, 56826), 0, 0), !(h.toDataURL().length < 3e3) && (i.clearRect(0, 0, h.width, h.height), i.fillText(j(55356, 57331, 65039, 8205, 55356, 57096), 0, 0), c = h.toDataURL(), i.clearRect(0, 0, h.width, h.height), i.fillText(j(55356, 57331, 55356, 57096), 0, 0), d = h.toDataURL(), c !== d);
                case"diversity":
                    return i.fillText(j(55356, 57221), 0, 0), e = i.getImageData(16, 16, 1, 1).data, f = e[0] + "," + e[1] + "," + e[2] + "," + e[3], i.fillText(j(55356, 57221, 55356, 57343), 0, 0), e = i.getImageData(16, 16, 1, 1).data, g = e[0] + "," + e[1] + "," + e[2] + "," + e[3], f !== g;
                case"simple":
                    return i.fillText(j(55357, 56835), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0];
                case"unicode8":
                    return i.fillText(j(55356, 57135), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0];
                case"unicode9":
                    return i.fillText(j(55358, 56631), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0]
            }
            return!1
        }

        function e(a) {
            var c = b.createElement("script");
            c.src = a, c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
        }

        var f, g, h, i;
        for (i = Array("simple", "flag", "unicode8", "diversity", "unicode9"), c.supports = {everything: !0, everythingExceptFlag: !0}, h = 0; h < i.length; h++)c.supports[i[h]] = d(i[h]), c.supports.everything = c.supports.everything && c.supports[i[h]], "flag" !== i[h] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[i[h]]);
        c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
            c.DOMReady = !0
        }, c.supports.everything || (g = function () {
            c.readyCallback()
        }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)) : (a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function () {
            "complete" === b.readyState && c.readyCallback()
        })), f = c.source || {}, f.concatemoji ? e(f.concatemoji) : f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))
    }(window, document, window._wpemojiSettings);
</script>
<script type="text/javascript" src="register_files/wp-emoji-release.js"></script>
<meta content="Divi v.2.7.10" name="generator">
<style type="text/css">
    img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
    }
</style>
<link rel="stylesheet" id="divi-fonts-css" href="register_files/css.css" type="text/css" media="all">
<link rel="stylesheet" id="divi-style-css" href="register_files/style.css" type="text/css" media="all">
<link rel="stylesheet" id="et-shortcodes-css-css" href="register_files/shortcodes.css" type="text/css" media="all">
<link rel="stylesheet" id="et-shortcodes-responsive-css-css" href="register_files/shortcodes_responsive.css"
      type="text/css" media="all">
<link rel="stylesheet" id="magnific-popup-css" href="register_files/magnific_popup.css" type="text/css" media="all">
<script type="text/javascript" src="register_files/jquery.js"></script>
<script type="text/javascript" src="register_files/jquery-migrate.js"></script>
<link rel="https://api.w.org/" href="../../../wp-json/">
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="../../../xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml"
      href="../../../wp-includes/wlwmanifest.xml">
<meta name="generator" content="WordPress 4.6">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<style id="theme-customizer-css">


    #main-footer {
        background-color: #1c023c;
    }

    .footer-widget {
        color: #beb8cc;
    }

    #main-footer .footer-widget h4 {
        color: #beb8cc;
    }

    .footer-widget li:before {
        border-color: #1c023c;
    }

    .footer-widget h4 {
        font-weight: bold;
        font-style: normal;
        text-transform: none;
        text-decoration: none;
    }

    @media only screen and ( min-width: 981px ) {
        .footer-widget h4 {
            font-size: 23px;
        }

        .et_header_style_centered.et_hide_primary_logo #main-header:not(.et-fixed-header) .logo_container, .et_header_style_centered.et_hide_fixed_logo #main-header.et-fixed-header .logo_container {
            height: 11.88px;
        }

    }

    @media only screen and ( min-width: 1350px) {
        .et_pb_row {
            padding: 27px 0;
        }

        .et_pb_section {
            padding: 54px 0;
        }

        .single.et_pb_pagebuilder_layout.et_full_width_page .et_post_meta_wrapper {
            padding-top: 81px;
        }

        .et_pb_section.et_pb_section_first {
            padding-top: inherit;
        }

        .et_pb_fullwidth_section {
            padding: 0;
        }
    }

    @media only screen and ( max-width: 980px ) {
    }

    @media only screen and ( max-width: 767px ) {
    }
</style>


<style id="module-customizer-css">
    .et_pb_fullwidth_section .et_pb_slide_content {
        font-size: 17px;
    }

    .et_pb_fullwidth_section .et_pb_slide_description {
        padding-top: 17%;
        padding-bottom: 17%
    }
</style>

<link rel="shortcut icon" href="register_files/Pearson.png">
<link rel="icon"
      href="../../wp-content/uploads/2016/08/cropped-koala-in-tree_Q1NegI_L-32x32.png"
      sizes="32x32">
<link rel="icon"
      href="../../wp-content/uploads/2016/08/cropped-koala-in-tree_Q1NegI_L-192x192.png"
      sizes="192x192">
<link rel="apple-touch-icon-precomposed"
      href="../../wp-content/uploads/2016/08/cropped-koala-in-tree_Q1NegI_L-180x180.png">
<meta name="msapplication-TileImage"
      content="../../wp-content/uploads/2016/08/cropped-koala-in-tree_Q1NegI_L-270x270.png">
<style type="text/css" id="et-custom-css">
    #logo {
        display: inline-block;
        float: none;
        margin-bottom: 0;
        max-height: 100%;
        transition: all 0.4s ease-in-out 0s;
        vertical-align: middle;
    }
</style>
<style id="fit-vids-style">.fluid-width-video-wrapper {
        width: 100%;
        position: relative;
        padding: 0;
    }

    .fluid-width-video-wrapper iframe, .fluid-width-video-wrapper object, .fluid-width-video-wrapper embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
</style>

<link rel="stylesheet" href="../../libraries/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../../libraries/AdminLTE-2.3.0/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../libraries/font-awesome-4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="../../libraries/AdminLTE-2.3.0/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="../../libraries/vs-lib/vs-lib.css">
<link rel="stylesheet" href="assessment.css"/>
<link rel="stylesheet" type="text/css" href="../../libraries/jquery-easyui-1.5/themes/default/combo.css">
<link rel="stylesheet" type="text/css" href="../../libraries/jquery-easyui-1.5/themes/default/tree.css">
<link rel="stylesheet" type="text/css" href="../../libraries/jquery-easyui-1.5/themes/default/textbox.css">
<link rel="stylesheet" type="text/css" href="../../libraries/jquery-easyui-1.5/themes/default/panel.css">
<link rel="stylesheet" type="text/css" href="../../libraries/jquery-easyui-1.5/themes/icon.css">
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="css/colorpicker.css">
<link rel="stylesheet" type="text/css" href="css/seekbar.css">
<link rel="stylesheet" type="text/css" href="css/nouislider.css">

<style>
    .et-show-dropdown {

    }
</style>

<script type="text/javascript" src="register_files/frontend-builder-global-functions.js"></script>
<script type="text/javascript" src="register_files/jquery_003.js"></script>
<script type="text/javascript" src="register_files/custom.js"></script>
<script type="text/javascript" src="register_files/smoothscroll.js"></script>
<script type="text/javascript" src="register_files/jquery_002.js"></script>
<script type="text/javascript" src="register_files/waypoints.js"></script>
<script type="text/javascript" src="register_files/jquery_004.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var et_pb_custom = {"ajaxurl": "http:\/\/localhost\/xampp\/koalafy\/wp-admin\/admin-ajax.php", "images_uri": "http:\/\/localhost\/xampp\/koalafy\/wp-content\/themes\/Divi\/images", "builder_images_uri": "http:\/\/localhost\/xampp\/koalafy\/wp-content\/themes\/Divi\/includes\/builder\/images", "et_frontend_nonce": "dc88e4a82a", "subscription_failed": "Please, check the fields below to make sure you entered the correct information.", "et_ab_log_nonce": "1dfe04b85d", "fill_message": "Please, fill in the following fields:", "contact_error_message": "Please, fix the following errors:", "invalid": "Invalid email", "captcha": "Captcha", "prev": "Prev", "previous": "Previous", "next": "Next", "wrong_captcha": "You entered the wrong number in captcha.", "is_builder_plugin_used": "", "is_divi_theme_used": "1", "widget_search_selector": ".widget_search", "is_ab_testing_active": "", "page_id": "1", "unique_test_id": "", "ab_bounce_rate": "5", "is_cache_plugin_active": "no", "is_shortcode_tracking": ""};
    /* ]]> */
</script>
<script type="text/javascript" src="register_files/frontend-builder-scripts.js"></script>
<script type="text/javascript" src="register_files/wp-embed.js"></script>
<script
    src="https://code.jquery.com/jquery-3.1.0.min.js"
    integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s="
    crossorigin="anonymous"></script>
<script src="../../libraries/jquery.inputmask-3.x/dist/min/jquery.inputmask.bundle.min.js"
        type="text/javascript"></script>
<script src="../../libraries/AdminLTE-2.3.0/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../libraries/bootstrap/bootstrap.min.js"></script>
<script src="../../libraries/AdminLTE-2.3.0/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="../../scripts/assessment-model.js"></script>
<script src="../../scripts/assessmentuser-model.js"></script>

<script src="../../scripts/user-model.js"></script>
<script src="../../scripts/return-code.js"></script>
<script src="../../scripts/transaction-model.js"></script>
<script>var systemWorkMode = '<?php echo $systemObject->getSystemWorkMode(); ?>';</script>
<script src="../../scripts/video-model.js"></script>

<script src="../../scripts/modal.js"></script>
<script src="register_files/jquery-ui.js"></script>
<script type="text/javascript" src="../../libraries/jquery-easyui-1.5/jquery.easyui.min.js"></script>
<script src="../../scripts/template-model.js"></script>
<script src="register_files/colorpicker.js"></script>
<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
<script src="register_files/seekbar.js"></script>
<script src="register_files/nouislider.js"></script>
<script src="../../scripts/custom-editor.js"></script>
<script src="assessment.js"></script>

<script src="../../libraries/vs-lib/vs-lib.js"></script>
</head>
<body
    class="blog et_pb_button_helper_class et_fixed_nav et_show_nav et_cover_background et_pb_gutter windows et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_full_width_page gecko">
<div class="et-animated-content" style="padding-top: 54px; " id="page-container">


<header class="" style="top: 0px;" data-height-loaded="true" id="main-header" data-height-onload="80">
    <div class="container clearfix et_menu_container" >
        <div class="logo_container">
            <span class="logo_helper"></span>
            <a href="../../../">
                <img data-actual-height="357" data-actual-width="920"
                     src="../../resources/development/site/site-logo.png" alt="Koalafy"
                     id="logo" data-height-percentage="54">
            </a>
        </div>
        <div style="padding-left: 236px;" id="et-top-navigation" data-height="66" data-fixed-height="40">
            <nav id="top-menu-nav">
                <ul id="top-menu" class="nav">
                    <li id="menu-item-161"
                        class="menu-item current-menu-item menu-item-type-custom menu-item-object-custom menu-item-161">
                        <a href="../assessments/assessments.php">Assessments</a></li>
                    <li id="menu-item-208"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-208">
                        <a href="#"><?php echo $userData->account_first_name." ".$userData->account_last_name; ?></a>
                        <ul class="sub-menu">
                            <li id="menu-item-205"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"><a
                                    href="../my-account/my-account.php">My Account</a></li>
                            <!--<li id="menu-item-205"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"><a
                                    href="#">Billing Info</a></li>
                            <li id="menu-item-205"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"><a
                                    href="#">My Contacts</a></li>-->
                            <li id="menu-item-205"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205">
                                <a
                                    href="../a-helper/a-helper.php">Assessment Helper</a></li>
                            <li id="menu-item-205"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205<?php if (!$accountObject->isAdministrator()) echo " vs-hidden"; ?>">
                                <a
                                    href="#">Administrator</a>
                                <ul class="sub-menu">
                                    <li id="menu-item-205"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205">
                                        <a
                                            href="../trials-report/trials-report.php">Koalafy Free Accounts</a></li>
                                    <li id="menu-item-205"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205">
                                        <a
                                            href="#">Financial Transactions</a></li>
                                </ul>
                            </li>
                            <li id="menu-item-205"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"
                                vs-hidden="<?php echo !$accountObject->hasSystemDeveloperAccess(); ?>"><a
                                    href="#">System Developer Access</a></li>
                            <li id="menu-item-205"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"><a
                                    href="#">Help</a></li>
                            <li id="menu-item-205"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"><a
                                    href="../integration/integration.php">Integrations</a></li>

                            <li id="menu-item-205"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"><a
                                    href="../logout/logout.php" onclick="logoutFromSystem();">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>


            <div id="et_top_search">
                <span id="et_search_icon"></span>
            </div>

            <div id="et_mobile_nav_menu">
                <div class="mobile_nav closed">
                    <span class="select_page">Select Page</span>
                    <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                    <ul id="mobile_menu" class="et_mobile_menu">
                        <li id="menu-item-161"
                            class="menu-item current-menu-item menu-item-type-custom menu-item-object-custom menu-item-161">
                            <a href="../assessments/assessments.php">Assessments </a></li>
                        <li id="menu-item-208"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-208">
                            <a href="#"><?php echo $accountObject->getLoggedInFullName(); ?></a>
                            <ul class="sub-menu">
                                <li id="menu-item-205"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"><a
                                        href="../my-account/my-account.php">My Account</a></li>
                                <li id="menu-item-205"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"><a
                                        href="#">Billing Info</a></li>
                                <li id="menu-item-205"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"><a
                                        href="#">My Contacts</a></li>
                                <li id="menu-item-205"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205">
                                    <a
                                        href="../a-helper/a-helper.php">Assessment Helper</a></li>
                                <li id="menu-item-205"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"><a
                                        href="#">Help</a></li>
                                <li id="menu-item-205"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"><a
                                        href="#" onclick="logoutFromSystem();">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #et-top-navigation -->
    </div>
    <!-- .container -->
    <div class="et_search_outer">
        <div class="container et_search_form_container">
            <form role="search" method="get" class="et-search-form" action="../../../">
                <input class="et-search-field" placeholder="Search …" name="s" title="Search for:" type="search">
            </form>
            <span class="et_close_search_field"></span>
        </div>
    </div>
</header>
<!-- #main-header -->

<div id="et-main-area">
<div id="main-content">
<div class="container" style="padding-top: 54px; " >
<div style="font-size: 16px; font-weight: bold;  margin-bottom: 35px;">Your Account Subscription is&nbsp;   <span style="color:steelblue">"<?php echo $accountType;?>"</span>
    <div class="pull-right play-icon" onclick="play_video()"><i class="fa fa-play" style="cursor: pointer"></i><label>HELP VIDEO</label></div>
</div>
<div id="content-area" class="clearfix">

<div class="row">
<div class="col-xs-12 as-steps as-main">
    <div class="selected as-step as-step-1">
        <div class="number" onclick="selectASStep(1);">1</div>
        <div class="title" onclick="selectASStep(1);">Assessment Setup</div>
        <!--<a class="glyphicon glyphicon-question-sign col-xs-1 help" href="#" title="Dismissible popover"
           data-toggle="popover" data-trigger="focus"
           data-content="Click anywhere in the document to close this popover"></a>-->
    </div>
    <div class="as-step as-step-3">
        <div class="number" onclick="selectASStep(3);">2</div>
        <div class="title as-title" onclick="selectASStep(3);" data-html="true" title="No Questions!<span style='float: right; cursor: pointer; font-weight: bold;' onclick='$(&quot;.as-steps.as-main .as-step-3 .as-title&quot;).popover(&quot;hide&quot;);'>X</span>"
             data-trigger="focus" data-content="Please make some questions here." data-placement="top" >X Axis
        </div>
        <!--<a class="glyphicon glyphicon-question-sign col-xs-1 help" href="#" title="Dismissible popover"
           data-toggle="popover" data-trigger="focus"
           data-content="Click anywhere in the document to close this popover"></a>-->
    </div>
    <div class="as-step as-step-4">
        <div class="number" onclick="selectASStep(4);">3</div>
        <div class="title as-title" onclick="selectASStep(4);" data-html="true" title="No Questions!<span style='float: right; cursor: pointer; font-weight: bold;' onclick='$(&quot;.as-steps.as-main .as-step-4 .as-title&quot;).popover(&quot;hide&quot;);'>X</span>"
             data-trigger="focus" data-content="Please make some questions here." data-placement="top" >Y Axis
        </div>
        <!--<a class="glyphicon glyphicon-question-sign col-xs-1 help" href="#" title="Dismissible popover"
           data-toggle="popover" data-trigger="focus"
           data-content="Click anywhere in the document to close this popover"></a>-->
    </div>
    <div class="as-step as-step-2">
        <div class="number" onclick="selectASStep(2);">4</div>
        <div class="title" onclick="selectASStep(2);" data-html="true"  data-placement="top"
             title="No Questions!<span style='float: right; cursor: pointer; font-weight: bold;' onclick='$(&quot;.as-steps.as-main .as-step-2 .title&quot;).popover(&quot;hide&quot;);'>X</span>">Start Scoring</div>
        <!--<a class="glyphicon glyphicon-question-sign col-xs-1 help" href="#" title="Dismissible popover"
           data-toggle="popover" data-trigger="focus"
           data-content="Click anywhere in the document to close this popover"></a>-->
    </div>
    <div class="as-step as-step-6">
        <div class="number" onclick="selectASStep(6);">5</div>
        <div class="title" onclick="selectASStep(6);" data-placement="bottom" data-trigger="manual"
             data-content="Invalid data found in matrix" data-html="true">Scoring Matrix</div>
        <!--<a class="glyphicon glyphicon-question-sign col-xs-1 help" href="#" title="Dismissible popover"
           data-toggle="popover" data-trigger="focus"
           data-content="Click anywhere in the document to close this popover"></a>-->

    </div>
    <div class="as-step as-step-5">
        <div class="number" onclick="selectASStep(5);">6</div>
        <div class="title" onclick="selectASStep(5);">Design</div>
        <!--<a class="glyphicon glyphicon-question-sign col-xs-1 help" href="#" title="Dismissible popover"
           data-toggle="popover" data-trigger="focus"
           data-content="Click anywhere in the document to close this popover"></a>-->
    </div>

    <div class="as-step as-step-7">
        <div class="number" onclick="selectASStep(7);">7</div>
        <div class="title" onclick="selectASStep(7);">Delivery</div>
        <!--<a class="glyphicon glyphicon-question-sign col-xs-1 help" href="#" title="Dismissible popover"
           data-toggle="popover" data-trigger="focus"
           data-content="Click anywhere in the document to close this popover"></a>-->
    </div>
</div>
<div class="col-md-12 as-builds" style="padding: 0;">
<div class="col-xs-12 assessment-details build-area as-build active as-build-1" style="padding: 0;">
   <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 " style="margin:0px;padding:25px 2px 25px 25px;background-color: #4471c4">
     <div class="form-group as-name" style="height: 34px;">
        <label for="4558015545" class="col-xs-12 col-sm-4 control-label" style="line-height: 34px;">Assessment Name:</label>

        <div class="col-xs-12 col-sm-8">
            <input title="" data-placement="top" data-trigger="manual" style="background-color: #4171c4;color:white"
                   data-content="An assessment with this name already exists." data-html="true" class="form-control name" id="4558015545"
                   type="text"
                   onkeyup="assessmentName(this);"
                   value="<?php echo $assessment_data_class != null ? $assessment_data_class->details["name"] : ""; ?>">
        </div>
     </div>
     <div class="form-group type" style="height: 34px;">
        <label for="9900624084" class="col-xs-12 col-sm-4 control-label" style="line-height: 34px;">Assessment
            Type:</label>

        <div class="col-xs-12 col-sm-8">
            <select class="form-control" id="9070624084"
                    onchange="updateAssessmentDetails();" style="background-color: #4171c4;color:white">
                <?php
                $temp = $assessment_class->getAssessmentTypes();
                foreach ($temp as $type) {
                    echo "<option value='$type->id'" . ($assessment_data_class != null && $assessment_data_class->details["typeID"] === $type->id ? " selected" : "") . ">$type->name</option>";
                }
                ?>
            </select>
        </div>
     </div>
   </div>

    <!--        <div class="form-group" style="height: 34px;">-->
    <!--            <label for="7476401035" class="col-xs-12 col-sm-4 control-label" style="line-height: 34px;">Results-->
    <!--                Type:</label>-->
    <!---->
    <!--            <div class="col-xs-12 col-sm-4 results-type">-->
    <!--                <input class="form-control type-table" id="7476401035" name="results-type"-->
    <!--                       type="radio"--><?php //echo $assessment_data_class != null && $assessment_data_class->details["resultType"] === "Chart" ? "" : " checked"; ?>
    <!--                       onchange="updateAssessmentDetails();">-->
    <!--                <span>Table</span>-->
    <!--            </div>-->
    <!--            <div class="col-xs-12 col-sm-4 results-type">-->
    <!--                <input class="form-control" id="7476401036" name="results-type"-->
    <!--                       type="radio"--><?php //echo $assessment_data_class != null && $assessment_data_class->details["resultType"] === "Chart" ? " checked" : ""; ?>
    <!--                       onchange="updateAssessmentDetails();">-->
    <!--                <span>Chart</span>-->
    <!--            </div>-->
    <!--        </div>-->
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 " style="padding: 0px;margin:0px;background-color: #c5c5c5">
      <p class="form-group"
       style="margin-top: 5px; text-align: justify; padding-left: 15px; padding-right: 15px;">Click
        checkbox if you wish to use the Assessment Helper or create your own assessment X & Y axis
        items</p>
    </div>
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 " style="padding: 0px;margin:0px;background-color: #bf9000;padding-top: 10px;padding-bottom: 10px;margin-bottom: 5px">
    <div class="form-group as-helper" style="height: 34px;">
        <div class="col-xs-12 col-sm-4">
            <input name="a-helper" type="checkbox" style="vertical-align: sub;" checked
                   onchange="switchCategoryControl(this);$('.assessment-details .as-helper input[type=\'text\']').combotree($(this).prop('checked') ? 'enable' : 'disable');"/>
            <label for="9900624384" class="control-label" style="line-height: 34px;">Assessment
                Helper:</label>
        </div>
        <div class="col-xs-12 col-sm-8">
            <input class="form-control" id="9070624384" type="text"/>
        </div>
    </div>
    <div class="form-group as-custom-category" style="height: 34px;">
        <div class="col-xs-12">
            <input name="a-helper" type="checkbox" style="vertical-align: sub;"
                   onchange="switchCategoryControl(this);$('.assessment-details .as-helper input[type=\'text\']').combotree($(this).prop('checked') ? 'disable' : 'enable');"/>
                <span class="control-label"
                      style="line-height: 34px; font-weight: bold;">Create your own category items</span>
        </div>
    </div>
    <div class="form-group x-category as-x-category " style="height: 34px;">
        <label for="9900624084" class="col-xs-12 col-md-4 col-lg-4 col-sm-4 control-label" style="line-height: 34px;">X
            Axis Category:</label>
        <div class="col-xs-12 col-sm-8">
            <select title="" data-placement="top" data-trigger="manual"
                    data-content="Categories on the axis can not be same." data-html="true" class="form-control" id=""
                    onchange="category_select_x(this)" >
            </select>
            <!--<input type="hidden" id="xdata" value="Timeline" />-->
            <input title="" data-placement="top" data-trigger="manual"
                   data-content="Categories on the axis can not be same." data-html="true"
                   class="form-control vs-hidden" id="3895608817" type="text"
                   onkeyup="category_input_x(this)">
        </div>
    </div>
    <div class="form-group y-category as-y-category col-md-12 col-lg-12 col-xs-12 col-sm-12 " style="height: 34px;margin:0px;padding:0px">
        <label for="9900624084" class="col-xs-12 col-md-4 col-lg-4 col-sm-4 control-label" style="line-height: 34px;">Y
            Axis Category:</label>
        <div class="col-xs-12 col-sm-8">
            <select title="" data-placement="top" data-trigger="manual"
                    data-content="Categories on the axis can not be same." data-html = "true" class="form-control" id=""
                    onchange="category_select_y(this)">
            </select>
            <!--<input type="hidden" id="ydata" value="Timeline" />-->
            <input title="" data-placement="top" data-trigger="manual"
                   data-content="Categories on the axis can not be same." data-html="true" class="form-control vs-hidden" id="3895608817"
                   type="text" onkeyup="category_input_y(this)">
        </div>
    </div>

        <div class="col-xs-12" style="text-align: right; margin-bottom: 10px;margin-top: 5px">
            <button id='nextStep' class="btn btn-info" onclick="onDetailsNextClicked();">Next</button>
        </div>
    </div>
</div>
<div class="col-xs-12 build-area as-build as-build-2">
    <!-- <div class="as-scoring as-scoring-1 ">
         <div class="as-review col-xs-12">
             <h3 class="as-title col-xs-12">Start Scoring </h3>

             <div class="as-go col-xs-12">
                 <span class="col-md-9">I’m Ready to Begin Scoring</span>
                 <button class="btn btn-info" onclick="selectASScoring(2);">Begin Scoring</button>
             </div>
             <div class="as-go col-xs-12">
                 <span class="col-md-9">I’m Ready to Begin Design & Delivery</span>
                 <button class="btn btn-info">Begin Design & Delivery</button>
             </div>
             <div class="as-go col-xs-12">
                 <span class="col-md-9">I Want to Review My X Axis Questions & Answers</span>
                 <button class="btn btn-info" onclick="selectASStep(3);">X Axis Review</button>
             </div>
             <div class="as-go col-xs-12">
                 <span class="col-md-9">I Want to Review My Y Axis Questions & Answers</span>
                 <button class="btn btn-info" onclick="selectASStep(4);">Y Axis Review</button>
             </div>
         </div>
     </div>-->
    <div class="as-scoring as-scoring-1 active">
        <div class="as-scores col-xs-12">
            <h3 class="as-title col-xs-12">Start Scoring</h3>

            <div class="as-table col-xs-12 container">
                <div class="row as-head">
                    <span class="col-xs-4">Totals</span>
                    <span class="col-xs-4">Minimum</span>
                    <span class="col-xs-4">Maximum</span>
                </div>
                <div class="row as-row vs-x">
                    <div class="col-xs-4">
                        <input type="checkbox" name="axis-edit"
                               onchange="score_x(this)"/>
                        <span>Score the X axis</span>
                    </div>
                    <span class="col-xs-4 vs-min">0</span>
                    <span class="col-xs-4 vs-max">100</span>
                </div>
                <div class="row as-row vs-y">
                    <div class="col-xs-4">
                        <input type="checkbox" name="axis-edit"
                               onchange="score_y(this)"/>
                        <span>Score the Y axis</span>
                    </div>
                    <span class="col-xs-4 vs-min">0</span>
                    <span class="col-xs-4 vs-max">100</span>
                </div>
            </div>
            <div class="col-xs-12" style="text-align: right; margin-top: 10px; padding: 0px; margin-bottom: 10px;">
                <button class="btn btn-info" style="min-width: 100px;" onclick="selectASScoring(1);">Back</button>
            </div>
        </div>
    </div>
    <div class="as-scoring as-scoring-3">
        <div class="as-axis-scores as-axis-scores-1 active col-xs-12">
            <div class="as-head col-xs-12">
                <h3 class="col-md-9">Review Total Scores for X Axis</h3>

                <div class="col-md-3 as-points"
                     style="text-align: center; border: 1px solid; background-color: whitesmoke; padding: 4px 10px;">
                    <span class="col-xs-12">Total Points for X Axis:</span>
                    <span class="col-xs-6 as-min" style="border: 1px solid; background-color: white;">0</span>
                    <span class="col-xs-6 as-max" style="border: 1px solid; background-color: white;">100</span>
                </div>
            </div>
            <div class="col-xs-12 as-header">
                <div class="col-xs-2">
                    <span class="col-xs-5" style="border: 1px solid; padding: 0px;">Min</span>
                    <span class="col-xs-5" style="border: 1px solid; padding: 0px;">Max</span>
                </div>
                <span class="col-xs-2" style="border: 1px solid;">Score Question</span>
            </div>
            <div class="col-xs-12 as-score-questions">
            </div>
            <div class="col-xs-12" style="text-align: right; margin-top: 10px; padding: 0px; margin-bottom: 10px;">
                <button class="btn btn-info" style="min-width: 100px;" onclick="selectASScoring(1,'back');">Back</button>
                <button class="btn btn-info" style="min-width: 100px;" onclick="selectASAxisScores(2,'next');">Next</button>
            </div>
        </div>
        <div class="as-axis-scores as-axis-scores-2 col-xs-12">
            <div class="as-head col-xs-12">
                <h3 class="col-md-9">Review Total Scores for Y Axis</h3>

                <div class="col-md-3 as-points"
                     style="text-align: center; border: 1px solid; background-color: whitesmoke; padding: 4px 10px;">
                    <span class="col-xs-12">Total Points for Y Axis:</span>
                    <span class="col-xs-6 as-min" style="border: 1px solid; background-color: white;">0</span>
                    <span class="col-xs-6 as-max" style="border: 1px solid; background-color: white;">100</span>
                </div>
            </div>
            <div class="col-xs-12 as-header">
                <div class="col-xs-2">
                    <span class="col-xs-5" style="border: 1px solid; padding: 0px;">Min</span>
                    <span class="col-xs-5" style="border: 1px solid; padding: 0px;">Max</span>
                </div>
                <span class="col-xs-2" style="border: 1px solid;">Score Question</span>
            </div>
            <div class="col-xs-12 as-score-questions">
            </div>
            <div class="col-xs-12" style="text-align: right; margin-top: 10px; padding: 0px; margin-bottom: 10px;">
                <button class="btn btn-info" style="min-width: 100px;" onclick="selectASAxisScores(1,'back');">Back</button>
                <button class="btn btn-info" style="min-width: 100px;" onclick="selectASStep(6);">Next</button>
            </div>
        </div>
    </div>
    <div class="as-scoring as-scoring-4">
        <div class="col-xs-12 as-question-editor-advanced">
            <h3 class="col-xs-12 as-title">Score the X Axis - Question #1</h3>
            <div class="col-xs-12 as-content">
                <div class="col-xs-12 as-question-edit">
                    <span class="col-md-2 as-number">Question 1:</span>
                    <input class="col-md-10 as-text" type="text"/>
                </div>
                <div class="as-head col-xs-12" style="margin-bottom: 3px;">
                    <span class="col-md-2">Enter Score</span>
                </div>
                <div class="as-answers col-xs-12">
                </div>
                <div class="col-xs-12 as-foot">
                    <div class="col-md-3 as-points" style="padding: 0px; text-align: center;margin-bottom: 5px">
                        <!--<span class="col-xs-12">Total For Question</span>
                        <div class="col-xs-12">
                            <span class="col-xs-6" style="border: 1px solid; background-color: white;">Min</span>
                            <span class="col-xs-6" style="border: 1px solid; background-color: white;">Max</span>
                        </div>-->
                        <div class="col-xs-12">
                            <input class="col-xs-6 as-min"
                                   style="border: 1px solid; background-color: white; text-align: center;" id="as_min"/>
                            <input class="col-xs-6 as-max"
                                   style="border: 1px solid; background-color: white; text-align: center;" id="as_max"/>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <?php
                        $accountType =  $accountObject->getUserDetail(0)->account_type;
                        if($accountType =='Koalafy Eucalyptus' || $accountType =='Koalafy King' ){
                            ?>
                            <p class="col-xs-12">* Remember your total maximum for ALL questions on this axis cannot exceed
                                100 points.</p>
                            <p class="col-xs-12">TIP: You may want to keep your total minimums so that someone can land in
                                the first lowest scoring possibility.</p>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 as-control" style="padding: 0px; text-align: right; margin: 10px 0px; border: none;">
                <button class="btn btn-info" onclick="selectASScoring(3);selectASAxisScores(2,'review');">
                    <!--                        onclick="selectASScoring(3);$('.as-builds .as-build.active .as-scoring .as-axis-scores').removeClass('active');$('.as-builds .as-build.active .as-scoring .as-axis-scores.as-axis-scores-1').addClass('active');">-->
                    Review Axis
                </button>
                <button class="btn btn-info as-save">Save</button>
                <button class="btn btn-info " id="back_question">Previous Question</button>
                <button class="btn btn-info as-next" >Next Question</button>

            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 assessment-save build-area as-build as-build-5" id="design-content" style="padding: 5px;margin-top:-24px;background:whitesmoke;margin-bottom:10px">
<!--<p class="col-xs-12 message">What do you want to do with the changes you made in the
    assessment?</p>

<div class="col-xs-12" style="text-align: right;">
    <button class="btn btn-info" onclick="saveAssessment();">Save Changes</button>
    <button class="btn btn-default">Undo Changes</button>
</div>-->

<ul class="col-xs-12 col-sm-12 col-md-5 col-lg-5 list-unstyled" style="overflow:scroll;overflow-x: hidden;height:100%" id="design-ul">
<li class="firstLevel">Question Order<label  class="pull-right"  >+</label>
    <ul class='list-unstyled collapse' id="first" >
        <li ><span class="col-md-5" style="padding: 0px">Order</span> <span class="col-md-7" style="padding: 0px">Question</span></li>
    </ul>
</li>
<li class="firstLevel">Themes<label  class="pull-right">+</label>
    <ul class='list-unstyled collapse' id="second">
        <li style="padding: 0px;margin:0px;">
            <div class="theme-box">

                <!--<div class="theme-item ">
                    <div class="selected-item active"><i class="fa fa-check"></i></div>
                    <div class="theme-body ">
                        <div class="theme-body-contentt "></div>
                        <div class="theme-body-contentt "></div>
                        <div class="theme-body-contentt theme-body-title"></div>
                        <div class="theme-body-contentt theme-body-page"></div>
                        <div class="theme-body-contentt theme-body-page"></div>
                        <div class="theme-body-contentt theme-body-page"></div>

                    </div>
                    <div class="theme-footer">

                        <p style="padding-bottom:5px">Name</p>
                        <p style="margin-left: auto;padding-bottom:5px;padding-right:5px"><i class="fa fa-caret-down"></i></p>

                    </div>
                </div>-->
            </div>
        </li>

    </ul>
</li>
<li >Setting & Description<label  class="pull-right">+</label>
    <ul class='list-unstyled collapse' id="third">
        <li style="padding: 0px;margin:0px;">
            <div class="assessment-setting-container">


                <div class="assessment-setting-header col-md-12 col-lg-12" style="margin: 0px;padding:0px">
                    <p class="col-md-3 col-lg-3 pull-left" style="padding-left: 11px">BUTTON</p>
                    <p class="col-md-4 col-lg-4 pull-left" >BUTTON TEXT</p>
                    <p class="col-md-4 col-lg-4 pull-left" style="padding:0px">#Questions Per Page</p>

                </div>
                <div class="col-md-12 col-lg-12" style="margin: 0px;padding:0px">
                    <select class="pull-right" id="question-range" onchange="onQuestionRange(this.id)">
                        <option value="3">3</option>
                        <option value="5">5</option>
                        <option value="7">7</option>
                        <option value="10">10</option>

                    </select>
                    <p class="pull-right" style="padding-right:17px">#(Min 3/Max 10)</p>

                </div>
                <div class="assessment-setting-body col-md-12 col-lg-12" style="margin: 0px;padding:0px">
                    <div class="col-md-3 col-lg-3" style="display:flex">
                        <p >BACK  :</p>
                        <input type="checkbox" style="margin-left: 12px" id="back" onchange ="onBackPressed(this)" checked/>
                    </div>

                    <div class="col-md-4 col-lg-4" >
                        <input type="text" placeholder="back" class="form-control" style="width:90%" id="back_text" onkeyup="onBackType(this)"/>
                    </div>

                    <div class="col-md-4 col-lg-4" >
                        <p >Show Question Numbers</p>
                    </div>
                    <div class="col-md-1 col-lg-1" >
                        <input type="checkbox" id="s_question" checked  onchange="onQuestionChecked();"/>
                    </div>

                </div>
                <div class="assessment-setting-body col-md-12 col-lg-12" style="margin: 0px;padding:0px">
                    <div class="col-md-3 col-lg-3" style="display:flex">
                        <p >NEXT  :</p>
                    </div>

                    <div class="col-md-4 col-lg-4" >
                        <input type="text" placeholder="Next" class="form-control" style="width:90%" id="next_text" onkeyup="onNextType(this)"/>
                    </div>

                    <div class="col-md-4 col-lg-4" >
                        <p >Show Header Title</p>
                    </div>
                    <div class="col-md-1 col-lg-1" >
                        <input type="checkbox" id="s_title" onchange="show_title(this)" checked/>
                    </div>
                </div>

                <div class="assessment-setting-body col-md-12 col-lg-12" style="margin: 0px;padding:0px">

                    <div class="col-md-6 col-lg-6" >
                    </div>

                    <div class="col-md-5 col-lg-5" >
                        <p style="text-align:right" >Show Description</p>
                    </div>
                    <div class="col-md-1 col-lg-1" >
                        <input type="checkbox" checked id="s_desc" onchange="show_description(this)"/>
                    </div>
                </div>
                <div class="assessment-setting-body col-md-12 col-lg-12" style="margin: 0px;padding:0px">
                    <input type="text" placeholder="Type max of 50 characters description here" id="desc"
                           style="width:100%;margin-top:5px" onkeyup="typeDescription()"/>
                </div>
                <div class="assessment-setting-body col-md-12 col-lg-12" style="margin:0px;padding:0px;">

                    <div class="col-md-6 col-lg-6" style="background-color: #FFFFFF;border-radius:5px;padding:7px">
                        <div class="col-md-8 col-lg-8" style="margin: 0px;padding:0px">
                            <select id="font-style-desc" style="width:100%" onchange="updateFontStyle('description','font-style-desc')">
                            </select>
                        </div>

                        <div class="col-md-4 col-lg-4" style="margin: 0px;padding:0px">
                            <select id="font-size-desc" style="width:100%" onchange="updateFontSize('description','font-size-desc')">
                                <option value="10">10</option>
                                <option value="12">12</option>
                                <option value="14">14</option>
                                <option value="16">16</option>
                                <option value="18">18</option>
                                <option value="20">20</option>
                                <option value="22">22</option>
                                <option value="24">24</option>
                                <option value="26">26</option>
                                <option value="28">28</option>
                                <option value="36">36</option>
                                <option value="48">48</option>
                            </select>
                        </div>

                    </div>

                    <div class="col-md-6 col-lg-6" style="background-color: #FFFFFF;border-radius:5px;padding:2px;margin-left: 5px">
                        <div class="col-md-4 col-lg-4" style="margin: 0px;padding:0px">
                            <!--<select id="font-style" style="width:100%;margin: 0px;padding:0px">
                                <option value="font-0">Font 0</option>
                                <option value="font-1">Font 1</option>
                                <option value="font-2">Font 2</option>
                            </select>-->
                            <div id="font-color-desc" class="assessment-font-color"></div>
                        </div>

                        <div class="col-md-8 col-lg-8" style="margin:0px;padding-top:7px">
                            <select id="font-align-desc" style="width:100%" onchange="updateTextStyle('description','font-align-desc')">
                                <option value="center">Center</option>
                                <option value="left">Left</option>
                                <option value="right">Right</option>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
        </li>
    </ul>
</li>
<li >Title Area<label  class="pull-right">+</label>
    <ul class='list-unstyled collapse' id="fourth" onclick="showCollapsed(this)">
        <li style="padding: 0px;margin:0px;">
            <div class="assessment-title-container ">
                <div class="assessment-setting-body col-md-12 col-lg-12" style="margin:0px;padding:0px;">

                    <div class="col-md-6 col-lg-6" style="background-color: #FFFFFF;border-radius:5px;padding:7px">
                        <div class="col-md-8 col-lg-8" style="margin: 0px;padding:0px">
                            <select id="font-style" style="width:100%" onchange="updateFontStyle('ass-title','font-style')">
                            </select>
                        </div>

                        <div class="col-md-4 col-lg-4" style="margin: 0px;padding:0px">
                            <select id="font-size" style="width:100%" onchange="updateFontSize('ass-title','font-size')">
                                <option value="10">10</option>
                                <option value="12">12</option>
                                <option value="14">14</option>
                                <option value="16">16</option>
                                <option value="18">18</option>
                                <option value="20">20</option>
                                <option value="22">22</option>
                                <option value="24">24</option>
                                <option value="26">26</option>
                                <option value="28">28</option>
                                <option value="30">36</option>
                                <option value="32">48</option>
                            </select>
                        </div>

                    </div>

                    <div class="col-md-6 col-lg-6" style="background-color: #FFFFFF;border-radius:5px;padding:2px;margin-left: 5px">
                        <div class="col-md-4 col-lg-4" style="margin: 0px;padding:0px">
                            <!--<select id="font-style" style="width:100%;margin: 0px;padding:0px">
                                <option value="font-0">Font 0</option>
                                <option value="font-1">Font 1</option>
                                <option value="font-2">Font 2</option>
                            </select>-->
                            <div id="font-color" class="assessment-font-color"></div>
                        </div>

                        <div class="col-md-8 col-lg-8" style="margin:0px;padding-top:7px">
                            <select id="font-align" style="width:100%" onchange="updateTextStyle('ass-title','font-align')">
                                <option value="center">Center</option>
                                <option value="left">Left</option>
                                <option value="right" selected="selected">Right</option>
                            </select>
                        </div>

                    </div>
                </div>

                <div class="col-md-12 col-lg-12" style="margin: 0px;padding:0px">
                    <div class="col-md-9 col-lg-9" style="margin:0px;padding:0px">
                        <p >Background Color</p>
                        <p id="colorpickerHolder"></p>
                    </div>
                    <div class="col-md-3 col-lg-3" style="margin:0px;padding:0px">
                        <p style="text-align: left;margin-bottom:20px">Height</p>
                        <div id="seekbar-container-vertical-blue" style="height:168px"></div>

                    </div>
                </div>

            </div>
        </li>
    </ul>
</li>
<li >Logo<label  class="pull-right">+</label>
    <ul class='list-unstyled collapse' id="fifth">
        <li style="padding: 0px;margin:0px;">
            <div class="assessment-setting-logo">
                <div class="col-md-12 col-lg-12">
                    <img src="images/profile-pic.png" alt="Logo Image" style="border-radius: 18px;width:100px;height:100px" id="logo_img">
                    <img src="images/profile-pic.png" alt="Logo Image" style="display:none" id="logo_img_hide" >

                </div>
                <div class="col-md-12 col-lg-12 assessment-setting-logo-btn" style="margin: 0px;padding:5px">
                    <button class="btn btn-sm btn-primary col-md-3 col-lg-3" style="margin-right: 5px" onclick="addAssessmentLogo()">  Add</button>
                    <button class="btn btn-sm btn-danger col-md-3 col-lg-3" onclick="removeAssessmentLogo()">  Remove</button>
                    <INPUT type='file' id='file1' name='file1' style="display:none" onchange="selectedFiles(this)"/>
                </div>
                <div class="col-md-12 col-lg-12" style="text-align: left; margin: 0px; padding: 0px;">
                    <div class='col-md-3 col-lg-3' style="margin: 0px;padding:0px;text-align:right;"><span style="padding-right: 10px">Position :</span></div>
                    <div class='col-md-6 col-lg-6' style="margin: 0px;padding:0px">
                        <select style="width: 100%" id="logo-position" onchange="logo_position(this.id);" >
                            <option value="left">Left</option>
                            <option value="right">Right</option>
                            <!--                            <option value="center">Center</option>-->
                        </select>
                    </div>

                    <div class="col-md-12 col-lg-12" style="text-align: left; margin: 0px; padding: 0px;margin-top: 5px">
                        <div class='col-md-3 col-lg-3' style="margin: 0px;padding:0px;text-align:right;padding-left:2px"><span style="padding-right: 10px">Size :</span></div>
                        <div class='col-md-6 col-lg-6' style="margin: 0px;padding:0px">
                            <select style="width: 100%" id="logo-size" onchange="logo_scale(this.id)">
                                <option value="small">Small</option>
                                <option value="medium">Medium</option>
                                <option value="large">Large</option>
                            </select>
                        </div>
                    </div>
                </div>
        </li>
    </ul>
</li>
<li >Background<label  class="pull-right">+</label>
    <ul class='list-unstyled collapse' id="sixth">
        <li style="padding: 0px;margin:0px;">
            <div class="assessment-title-container ">
                <div class="col-md-12 col-lg-12" style="margin: 0px;padding:0px">
                    <p >Background Color</p>
                    <p id="colorpickerBack"></p>
                </div>
            </div>
        </li>
    </ul>
</li>
<li >Page <label  class="pull-right">+</label>
    <ul class='list-unstyled collapse' id="seventh">
        <li style="padding: 0px;margin:0px;">
            <div class="assessment-title-container ">
                <div class="col-md-12 col-lg-12" style="margin: 0px;padding:0px;text-align:center">
                    <label class="radio-inline" style="color:black"><input type="radio" name="page_font" id="page_font" onchange="pageChecked(this,event);" checked >Font</label>
                    <label class="radio-inline" style="color:black"><input type="radio" name="page_background" id="page_background" onchange="pageChecked(this,event);" >Background</label>
                </div>

                <div class="col-md-12 col-lg-12" style="margin: 0px;padding:0px">
                    <div class="col-md-12 col-lg-12" style="margin:0px;padding:0px">
                        <p>Background Color</p>
                        <p id="page-colorpicker"></p>
                    </div>

                </div>
                <div class="col-md-12 col-lg-12" style="margin: 0px;padding:0px">
                    <div class="col-md-4 col-lg-4" style="margin: 0px;padding:0px">
                        <p>Show Progress Bar</p>
                    </div>
                    <div class="col-md-2 col-lg-2" style="margin: 0px;padding:0px">
                        <!--<label class="switch">
                            <input type="checkbox" checked>
                            <div class="slider round"></div>
                        </label>-->
                        <input type="checkbox" id="progress" onchange="showProgress(this,event)" onclick="javascript:void(0)" checked/>
                    </div>
                    <div class="col-md-6 col-lg-6" style="margin: 0px;padding:0px">
                        <label class="radio-inline" style="color:black"><input type="radio" name="progress_top" id="progress_top" onchange="progressChecked(this.id)" onclick="javascript:void(0)">Top</label>
                        <label class="radio-inline" style="color:black"><input type="radio" name="progress_bottom" id="progress_bottom" onchange="progressChecked(this.id)" onclick="javascript:void(0)" checked>Bottom</label>
                    </div>
                </div>

            </div>
        </li>
    </ul>
</li>
<li >Footer<label  class="pull-right">+</label>
    <ul class='list-unstyled collapse' id="eighth" onclick="javascript:void(0);">
        <li style="padding: 0px;margin:0px;">
            <div class="assessment-title-container ">

                <!--<div class="col-md-12 col-lg-12" style="margin: 0px;padding:0px;text-align:center">
                    <label class="radio-inline" style="color:black"><input type="radio" name="font" id="font" onchange="footerChecked(this)" checked>Font</label>
                    <label class="radio-inline" style="color:black"><input type="radio" name="background" id="background" onchange="footerChecked(this)">Background</label>
                </div>-->
                <div class="col-md-12 col-lg-12" style="margin:0px;padding:0px">
                    <input type="text" id="page_footerr" style="width:100%;margin-bottom: 5px;" onkeyup="onFooterTyped();" placeholder="Footer goes here"/>
                </div>
                <div class="assessment-setting-body col-md-12 col-lg-12" style="margin:0px;padding:0px;">

                    <div class="col-md-6 col-lg-6" style="background-color: #FFFFFF;border-radius:5px;padding:7px">
                        <div class="col-md-8 col-lg-8" style="margin: 0px;padding:0px">
                            <select id="font-stylee" style="width:100%" onchange="updateFontStyle('page_footer','font-stylee')">
                            </select>
                        </div>

                        <div class="col-md-4 col-lg-4" style="margin: 0px;padding:0px">
                            <select id="font-sizee" style="width:100%" onchange="updateFontSize('page_footer','font-sizee')">
                                <option value="10">10</option>
                                <option value="12">12</option>
                                <option value="14">14</option>
                                <option value="16">16</option>
                                <option value="18">18</option>
                                <option value="20">20</option>
                                <option value="22">22</option>
                                <option value="24">24</option>
                                <option value="26">26</option>
                                <option value="28">28</option>
                                <option value="36">36</option>
                                <option value="48">48</option>
                            </select>
                        </div>

                    </div>

                    <div class="col-md-6 col-lg-6" style="background-color: #FFFFFF;border-radius:5px;padding:2px;margin-left: 5px">
                        <div class="col-md-4 col-lg-4" style="margin: 0px;padding:0px">
                            <!--<select id="font-style" style="width:100%;margin: 0px;padding:0px">
                                <option value="font-0">Font 0</option>
                                <option value="font-1">Font 1</option>
                                <option value="font-2">Font 2</option>
                            </select>-->
                            <div id="font-colorr" class="assessment-font-color"></div>
                        </div>

                        <div class="col-md-8 col-lg-8" style="margin:0px;padding-top:7px">
                            <select id="font-alignn" style="width:100%" onchange="updateTextStyle('assessment-footer','font-alignn')">
                                <option value="center">Center</option>
                                <option value="left">Left</option>
                                <option value="right">Right</option>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="col-md-12 col-lg-12" style="margin: 0px;padding:0px">
                    <div class="col-md-9 col-lg-9" style="margin:0px;padding:0px">
                        <p >Background Color</p>
                        <p id="footer-colorpicker"></p>
                    </div>
                    <div class="col-md-3 col-lg-3" style="margin:0px;padding:0px">
                        <p style="text-align: center;margin-bottom:20px">Height</p>
                        <div id="footer-seekbar" style="height:168px"></div>

                    </div>
                </div>

            </div>
        </li>
    </ul>
</li>
<li onclick="updatePageQuestion(this)">Layout<label  class="pull-right">+</label>
    <ul class='list-unstyled collapse' id="nineth" >
        <li style="padding: 0px;margin:0px;">
            <div class="assessment-layout-container ">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 assessment-layout-header" style="margin:0px;padding:0px">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 assessment-layout-main" style="margin :0px;padding:0px">
                        <i class="fa fa-desktop fa-5x"></i>
                        <div class="layout-checkbox">
                            <label >Desktop</label>
                            <input type="radio" name="layout_1" value="desktop" id="desktop" class="layout_" onchange="layout_checked(this,event)" style="margin-left: 5px;margin-top: 6px;" checked/>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 assessment-layout-main" style="padding:0px">
                        <i class="fa fa-tablet fa-4x"></i>
                        <div class="layout-checkbox">
                            <label >Tablet</label>
                            <input type="radio" name="layout_1" class="layout_" id="tab" value="tab" onchange="layout_checked(this,event)" style="margin-left: 5px;margin-top: 6px;" />
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 assessment-layout-main" style="margin: :0px;padding:0px">
                        <i class="fa fa-mobile-phone fa-3x"></i>
                        <div class="layout-checkbox">
                            <label >Smartphone</label>
                            <input type="radio" name="layout_1" class="layout_" id="smartphone" value="smartphone" onchange="layout_checked(this,event)" style="margin-left: 5px;margin-top: 6px;" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin:0px;padding:0px" >
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="text-align:center" >
                        <label >Page Width</label>
                        <div class="float-left">
                            <input type="number" id="page_width" value="80" style="width:50%" onkeyup="pagewidth_typed(this);"/>
                            <label>%</label>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="text-align:center" >
                        <label >Page Content Width</label>
                        <div class="float-left">
                            <input type="number" id="page_content_width" value="50" style="width:50%" onkeyup="page_content_typed(this);"/>
                            <label>%</label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align:center;margin-top:10px" >
                    <button class="btn btn-primary" id="preview">Preview Assessment</button>
                </div>
            </div>
        </li>
    </ul>
</li>
<li onclick="updatePageQuestion(this)">Popup Form<label  class="pull-right">+</label>
    <ul class='list-unstyled collapse' id="ten" >
        <li style="padding: 0px;margin:0px;">
            <div class="assessment-layout-container ">
                <div class="assessment-layout-header col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-4">
                        <p>Popup form ?</p>
                    </div>
                    <div class="assessment-layout-header col-md-3">
                        <input type="radio" id="yes" class="confirm" style="margin-right: 5%" checked onchange="onPopupVisible(this.id)"/>
                        <p>Yes</p>

                    </div>
                    <div class="assessment-layout-header col-md-3">
                        <input type="radio" id="no" class="confirm" style="margin-right: 5%" onchange="onPopupVisible(this.id)"/>
                        <p>No</p>
                    </div>

                </div>

                <div class="assessment-layout-header col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="assessment-layout-header col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p >Popup Title</p>
                    </div>
                    <div class="assessment-layout-header col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <input type="text" placeholder="Get your assessment results ! " style="width:100%" id="popup-title" onkeyup="onAssessmentTyped(this.id)"/>
                    </div>
                </div>

                <div class="assessment-layout-header col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="assessment-layout-header col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p>FIELD</p>
                    </div>
                    <div class="assessment-layout-header col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p>LABEL</p>
                    </div>
                    <div class="assessment-layout-header col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p>REQ</p>
                    </div>
                </div>

                <div class="assessment-layout-header col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="assessment-layout-header col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p style="margin-right: 5%;width:70px">First Name</p>
                        <input type="checkbox" id="first_name_chk" onchange="onFieldUpdate(this.id)" checked/>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <input type="text" id="first_name" placeholder="First name" style="width: 100%" onkeyup="onAssessmentTyped(this.id)"/>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="margin-top: 2px">
                        <input type="checkbox"  id="first_name_req" onchange="onRequiredUpdate(this.id)" checked/>
                    </div>
                </div>

                <div class="assessment-layout-header col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="assessment-layout-header col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p style="margin-right: 5%;width:70px">Last Name</p>
                        <input type="checkbox" id="last_name_chk" onchange="onFieldUpdate(this.id)" checked />
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <input type="text" id="last_name" placeholder="Last name" style="width: 100%" onkeyup="onAssessmentTyped(this.id)"/>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="margin-top: 2px">
                        <input type="checkbox" id="last_name_req" onchange="onRequiredUpdate(this.id)" checked/>
                    </div>
                </div>

                <div class="assessment-layout-header col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="assessment-layout-header col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p style="margin-right: 5%;width:70px">Full Name</p>
                        <input type="checkbox" id="full_name_chk" onchange="onFieldUpdate(this.id)" />
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <input type="text" id="full_name" placeholder="Full name" style="width: 100%" onkeyup="onAssessmentTyped(this.id)"/>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="margin-top: 2px">
                        <input type="checkbox" id="full_name_req" onchange="onRequiredUpdate(this.id)" />
                    </div>
                </div>


                <div class="assessment-layout-header col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="assessment-layout-header col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p style="margin-right: 5%;width:70px">Email</p>
                        <input type="checkbox" id="email_chk" onchange="onFieldUpdate(this.id)" checked/>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <input type="text" id="email" placeholder="Email" style="width: 100%" onkeyup="onAssessmentTyped(this.id)" />
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="margin-top: 2px">
                        <input type="checkbox" id="email_req" onchange="onRequiredUpdate(this.id)" checked/>
                    </div>
                </div>


                <div class="assessment-layout-header col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="assessment-layout-header col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p style="margin-right: 5%;width:70px">Phone</p>
                        <input type="checkbox" id="phone_chk" onchange="onFieldUpdate(this.id)"/>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <input type="text" id="phone" placeholder="Phone" style="width: 100%" onkeyup="onAssessmentTyped(this.id)" />
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="margin-top: 2px">
                        <input type="checkbox" id="phone_req" onchange="onRequiredUpdate(this.id)" />
                    </div>
                </div>

                <div class="assessment-layout-header col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="assessment-layout-header col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <p style="margin-right: 5%;width:70px">Company</p>
                        <input type="checkbox" id="company_chk" onchange="onFieldUpdate(this.id)" checked/>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <input type="text" id="company" placeholder="Company" style="width: 100%" onkeyup="onAssessmentTyped(this.id)" />
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="margin-top: 2px">
                        <input type="checkbox" id="company_req" onchange="onRequiredUpdate(this.id)" />
                    </div>
                </div>

                <div class="assessment-layout-header col-lg-12 col-md-12 col-sm-12 col-xs-12" style="flex-direction: column;-webkit-flex-direction: column;align-items: center;-webkit-align-items: center;margin-top: 10px">
                    <button class="btn btn-primary" style="width:50%" onclick="onSubmitAssessment()">Preview</button>
                </div>
        </li>
    </ul>
</li>

<li onclick="updatePageQuestion(this)">Results<label  class="pull-right">+</label>
    <ul class='list-unstyled collapse' id="eleven" style="height:30%;background-color:grey">
        <li style="padding: 0px;margin:0px;">
            <div class="as-resultss as-resultss-1 active">
              <div class="assessment-layout-header col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">Redirect</div>
                  <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9"><input type="radio" id="redirect" class="result_type" onchange="onResultTypeChecked(this.id)" checked  /></div>
              </div>

                <div class="assessment-layout-header col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">Message</div>
                    <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9"><input type="radio" id="message" class="result_type" onchange="onResultTypeChecked(this.id)" /></div>
                </div>

                <div class="assessment-layout-header col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3"><p style="font-weight: bold;font-size:17px;text-decoration:underline">Stage</p></div>
                    <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9" style="text-align: center"><p style="font-weight: bold;font-size:17px;text-decoration:underline">Enter url below</p></div>
                </div>
                <div class="result_row col-md-12 col-lg-12 col-sm-12 col-xs-12"></div>
                <!--<div class="assessment-layout-header col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3"><p >TOP</p></div>
                    <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9"><input type="text" style="width:100%" id="top"/></div>
                </div>

                <div class="assessment-layout-header col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3"><p >MID</p></div>
                    <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9"><input type="text" style="width:100%" id="mid"/></div>
                </div>

                <div class="assessment-layout-header col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3"><p >BAD</p></div>
                    <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9"><input type="text" style="width:100%" id="bad"/></div>
                </div>-->


            </div>

            <div class="assessment-layout-container as-resultss as-resultss-2">
                <div class="assessment-layout-header col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4" style="padding: 0px;margin:0px">
                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2" ><input type="radio" id="redirectt" class="result_type" onchange="onResultTypeChecked(this.id)" /></div>
                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2" ><p>Redirect</p></div>
                    </div>

                    <div class="assessment-layout-header col-md-8 col-lg-8 col-sm-8 col-xs-8" style="padding: 0px;margin:0px">
                       <p style="width:50px">Result</p> <input type="text" placeholder="Here are your results" id="results_s" style="width:80%"/>
                    </div>

                </div>
                <div class="assessment-layout-header col-md-12 col-lg-12 col-sm-12 col-xs-12">
                   <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4" style="padding: 0px;margin:0px">
                       <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2" ><input type="radio" id="messagee" class="result_type" onchange="onResultTypeChecked(this.id)" checked="checked"/></div>
                       <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2" ><p>Message</p></div>

                   </div>
                    <div class="assessment-layout-header col-md-8 col-lg-8 col-sm-8 col-xs-8" style="padding: 0px;margin:0px">
                        <p style="width:50px">Stage</p>
                        <select style="width:80%" id="stage_options" onclick="onStageSelected(event)" onchange="onStageChanged()">
                            <option>First</option>
                            <option>Second</option>
                            <option>Third</option>
                        </select>
                    </div>
                </div>

                <div class="assessment-layout-header col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <textarea name="editor1">&lt;p&gt;Initial value.&lt;/p&gt;</textarea>
                </div>

                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="text-align: right;margin-top:2%">
                    <button class="btn btn-primary" onclick="onPreviewClicked()" id="preview_button">Preview</button>
                    <button class="btn btn-default" onclick="onSaveResults()" id="save_button">Save</button>
                </div>
            </div>
        </li>
    </ul>
</li>
</ul>
<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 design-innerr"  id="design-inner">
    <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12" style="padding: 0px;border:1px solid black" id="main-contentt">
        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 assessment-page-title" style="padding:5px">
            <div style="margin:0px;padding:0px;" id="ass-logo-container">
                <img src="images/profile-pic.png" class="assessment-logo"  id="ass-logo"/>
            </div>
            <p class="pull-left" style="text-align:right;padding:5px;margin-top:17px;font-weight:bolder;white-space: nowrap" id="ass-title" ></p>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 assessment-desc" id="description_d">
            <!--               <input type="text" value="" placeholder="Enter description here" class="col-md-9 col-xs-12 col-sm-12 col-lg-12" style="height: 8%;width: 100%;"/>-->
            <p id="description">Description goes here</p>
        </div>

        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 assessment-desc page-area" style="margin: 0px;padding:0px">
            <div class="page-area-child page-area col-md-12 col-xs-12 col-sm-12 col-lg-12">
                <label id="page-text">Page Area here</label>
            </div>

            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                    50%
                </div>
            </div>

        </div>


        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 assessment-footer">
            <label id="page_footer">Page Footer</label>
        </div>
    </div>
</div>


</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 build-area as-build as-build-7">
    <div class="as-delivery as-delivery-1 active">
        <div class="as-delivery-icons col-xs-4 col-sm-4 col-md-4 col-lg-4 " onclick="share_selected('social')">
            <img src="images/web_link.jpg" style="width: 100px;height:100px"/>
            <label style="margin-top: 3%;cursor: pointer;">Web Link</label>
            <p>For email , social , other sharing</p>
            <input type ='radio' id="social" class="share" name='share' value='web_link' onchange="share_selected(this.id)" checked/>
        </div>

        <div class="as-delivery-icons col-xs-4 col-sm-4 col-md-4 col-lg-4 " onclick="share_selected('social_1')">
            <img src="images/social_sharing.png" width="100px" height="100px"/>
            <label style="margin-top: 3%;cursor: pointer;">Social Sharing</label>
            <p>Facebook , LinkedIn , Or Twitter</p>
            <input type ='radio' id="social_1" class="share" name='share' value='social' onchange="share_selected(this.id)"/>
        </div>

        <div class="as-delivery-icons col-xs-4 col-sm-4 col-md-4 col-lg-4 " onclick="share_selected('social_2')">
            <img src="images/websitee.png" width="100px" height="100px"/>
            <label style="margin-top: 3%;cursor: pointer;">Website</label>
            <p>Embed Assessment onto your website</p>
            <input type ='radio' id="social_2" class="share" name='share' value='web' onchange="share_selected(this.id)"/>
        </div>
        <div class="as-delivery-icons col-xs-12 col-sm-12 col-md-12 col-lg-12 " style="margin-top:3%">
            <button class="btn btn-primary btn-lg" onclick="share_submit()">Generate</button>
        </div>
    </div>

    <div class="as-delivery as-delivery-2">
        <div class="as-delivery-icons col-xs-12 col-sm-12 col-md-12 col-lg-12 " style="margin-top:3%">
            <div class="as-web-link">
                <img src="images/web_link.jpg" style="width:130px;height:100px"/>
                <div class="as-delivery-icons">
                    <label>Web Link  </label>
                    <p>For Email,social and other sharing</p>
                </div>
            </div>
        </div>

        <div class="as-delivery-icons col-xs-12 col-sm-12 col-md-12 col-lg-12 " style="margin-top:3%">
            <div class="as-web-pops col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                <label class="pull-left" style="color:white;font-size:18px">WEB LINK  <i class="fa fa-question-circle" aria-hidden="true" style="color:black"></i></label>
                <label class="pull-right" style="color:white;padding:5px;border:1px solid white;cursor:pointer" onclick="selectASDelivery(1);"><i class="fa fa-remove"></i></label>


                <div class="box-content col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <i class="fa fa-paperclip fa-2x" style="margin-left: 2%"></i>
                    <input type="text" class="form-control" id="web-link" style="margin-right: 2%;width: 85%;margin-left: 2%;border-radius: 5px" disabled="disabled" />
                    <button type="button" class="btn btn-default"  onclick="onTextCopy()">Copy</button>

                </div>
            </div>

        </div>
    </div>

    <div class="as-delivery as-delivery-3">
        <div class="as-delivery-icons col-xs-12 col-sm-12 col-md-12 col-lg-12 " style="margin-top:3%">
            <div class="as-web-link">
                <img src="images/social_sharing.png" style="width:122px;height:100px;padding-right:7px"/>
                <div class="as-delivery-icons">
                    <label>Social Sharing  </label>
                    <p>Facebook . LinkedIn , or Twitter</p>
                </div>
            </div>
        </div>

        <div class="as-delivery-icons col-xs-12 col-sm-12 col-md-12 col-lg-12 " style="margin-top:3%;">
            <div class="as-web-pops col-xs-12 col-sm-12 col-md-12 col-lg-12 " style="background-color: black;">
                <label class="pull-left" style="color:white;font-size:18px">Social Media Links  <i class="fa fa-question-circle" aria-hidden="true" style="color:white"></i></label>
                <label class="pull-right" style="color:white;padding:5px;border:1px solid white;cursor:pointer" onclick="selectASDelivery(1);"><i class="fa fa-remove"></i></label>


                <div class="box-content col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <img src="images/facebook-square.png" style="width:53px;height: 48px;margin-left: 10px"/>
                        <input type="radio" class="social_share" name="social_share" id="facebook" style="margin-top:22px;margin-left: 7%;" onchange="onSocialShare(this.id)" value="facebook" checked/>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <img src="images/linkedin-square.png" style="width:53px;height: 48px;margin-left: 10px"/>
                        <input type="radio" class="social_share" name="social_share" id="linkedin" style="margin-top:22px;margin-left: 7%;" onchange="onSocialShare(this.id)"  value="linkedin" />
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <img src="images/twitter-square.png" style="width:53px;height: 63px;margin-left: 10px" />
                        <input type="radio" class="social_share" id="twitter" name="social_share" style="margin-top:22px;margin-left: 7%;" onchange="onSocialShare(this.id)" value="twitter" />
                    </div>

                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <button type="button" class="btn btn-primary" style="margin-top:12px;" onclick="onSocialSubmit()">Generate</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

<div class="col-xs-12 as-build as-build-6">
    <div class="col-xs-12 as-matrixes vs-padding">
        <div class="col-xs-12 as-matrix as-matrix-1 vs-padding active">
            <div class="col-xs-12 vs-padding">
                <div class="col-md-7 vs-padding">
                    <button class="btn btn-primary" onclick="selectASMatrix(2);">All Potential Scores Table</button>
                </div>
                <div class="col-md-5 vs-padding as-points" style="background-color: #92d050">
                    <div class="col-xs-12 vs-padding as-head">
                        <span class="col-xs-4">Totals</span>
                        <span class="col-xs-4">Minimum</span>
                        <span class="col-xs-4">Maximum</span>
                    </div>
                    <div class="col-xs-12 vs-padding vs-x">
                        <span class="col-xs-4 vs-padding">Score The X Axis</span>
                        <span class="col-xs-4 vs-min">3</span>
                        <span class="col-xs-4 vs-max">100</span>
                    </div>
                    <div class="col-xs-12 vs-padding vs-y">
                        <span class="col-xs-4 vs-padding">Score The Y Axis</span>
                        <span class="col-xs-4 vs-min">0</span>
                        <span class="col-xs-4 vs-max">100</span>
                    </div>
                </div>
            </div>
            <h5 class="col-xs-12">Create Stages For Your Matrix</h5>
            <input type="hidden" id='stagescount' value='0' >

            <div class="col-xs-12 vs-padding">
                <span class="col-xs-3 vs-padding">Abbreviation</span>
                <span class="col-xs-9 vs-padding">Description</span>
            </div>
            <div class="col-xs-12 as-abbreviations vs-padding">
            </div>
            <div class="col-xs-12 vs-padding as-control">
                <button class="btn btn-info" onclick="selectASStep(2);selectASScoring(3);">Back</button>
                <button class="btn btn-info"
                        onclick="saveAssessment(true,'none', function(saved) { if(saved) { selectASMatrix(3); } else { /*alert('Failed to save assessment');*/ } });">
                    Next
                </button>
            </div>
        </div>
        <div class="col-xs-12 as-matrix as-matrix-2 vs-padding">
            <h4 class="col-xs-12">All Potential Scores Table</h4>

            <div class="col-xs-12 as-block vs-x">
                <!-- <div class="form-group">
                     <label>X Axis Possibilities</label>
                     <select class="form-control"></select>
                 </div>-->
                <div class="form-group">
                    <label>X Axis Results</label>
                </div>
                <div class="col-xs-12 vs-padding as-results assessment-axis-result" ></div>
            </div>
            <div class="col-xs-12 as-block vs-y">
                <!--  <div class="form-group">
                      <label>Y Axis Possibilities</label>
                      <select class="form-control"></select>
                  </div>-->
                <div class="form-group">
                    <label>Y Axis Results</label>
                </div>
                <div class="col-xs-12 vs-padding as-results assessment-axis-result"></div>
            </div>
            <div class="col-xs-12 as-control" style="border: none; text-align: right; padding: 0;">
                <button class="btn btn-info" onclick="selectASMatrix(1);">Back To Stages For Matrix</button>
            </div>
        </div>
        <div class="col-xs-12 as-matrix as-matrix-3 vs-padding">
            <h4 class="col-xs-12">Assign Stages to Matrix</h4>

            <div class="col-xs-12 vs-padding">
                <div class="col-xs-12 vs-padding">
                    <div class="col-md-7 vs-padding">
                        <button class="btn btn-primary" onclick="selectASMatrix(2);">All Potential Scores Table</button>
                    </div>
                    <div class="col-md-5 vs-padding as-points">
                        <div class="col-xs-12 vs-padding as-head">
                            <span class="col-xs-4">Totals</span>
                            <span class="col-xs-4">Minimum</span>
                            <span class="col-xs-4">Maximum</span>
                        </div>
                        <div class="col-xs-12 vs-padding vs-x">
                            <span class="col-xs-4 vs-padding">Score The X Axis</span>
                            <span class="col-xs-4 vs-min">3</span>
                            <span class="col-xs-4 vs-max">100</span>
                        </div>
                        <div class="col-xs-12 vs-padding vs-y">
                            <span class="col-xs-4 vs-padding">Score The Y Axis</span>
                            <span class="col-xs-4 vs-min">0</span>
                            <span class="col-xs-4 vs-max">100</span>
                        </div>
                    </div>
                </div>
            </div>
            <h5 class="col-xs-12">MATRIX SIZE: 2x by 2y</h5>

            <div class="col-xs-12 vs-padding">
                <div class="col-md-10 as-table">
                    <table>
                        <tr>
                            <td><input type="text" value="51-100"/></td>
                            <td class="as-col" ondragover="event.preventDefault();"
                                ondrop="onDrop_code1(event);">
                                CODE1
                            </td>
                            <td class="as-col" ondragover="event.preventDefault();"
                                ondrop="onDrop_code1(event);">
                                CODE2
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" value="0-50"/></td>
                            <td class="as-col" ondragover="event.preventDefault();"
                                ondrop="onDrop_code1(event)">
                                CODE3
                            </td>
                            <td class="as-col" ondragover="event.preventDefault();"
                                ondrop="onDrop_code1(event)">
                                CODE4
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="text" value="0-50"/></td>
                            <td><input type="text" value="51-100"/></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-2">
                    <span class="col-xs-12">Abbreviations</span>

                    <div class="col-xs-12 as-abbreviations"></div>
                </div>
            </div>
            <div class="col-xs-12 vs-padding as-control">
                <button class="btn btn-info" onclick="selectASMatrix(1);">Back</button>
                <button class="btn btn-info" onclick="saveAssessment(true,'none', function(saved) { if(saved) { selectASStep(5); } else { /*alert('Failed to save assessment');*/ } });">Next</button>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 as-build as-build-3">
    <div class="col-xs-12 as-control">
        <div class="btn-group col-md-4" style="padding: 0;">
            <button type="button" class="btn btn-info">Add Questions</button>
            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#" onclick="addNewQuestion('X', 'Dropdown');">New Dropdown Question</a></li>
                <li><a href="#" onclick="addNewQuestion('X', 'Button');">New Button Question</a></li>
            </ul>
        </div>
        <div class="col-md-8"></div>
    </div>
    <div class="col-xs-12 as-questions">
    </div>
</div>
<div class="col-xs-12 as-build as-build-4">
    <div class="col-xs-12 as-control">
        <div class="btn-group col-md-4" style="padding: 0;">
            <button type="button" class="btn btn-info">Add Questions</button>
            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#" onclick="addNewQuestion('Y', 'Dropdown');">New Dropdown Question</a></li>
                <li><a href="#" onclick="addNewQuestion('Y', 'Button');">New Button Question</a></li>
            </ul>
        </div>
        <div class="col-md-8"></div>
    </div>
    <div class="col-xs-12 as-questions">
    </div>
</div>
</div>

<!--<div class="col-xs-12 col-md-2 help-area">
    <div class="col-xs-12" style="text-align: center; padding: 0px;">
        <span style="font-weight: bold;">Help</span>
                        <span class="glyphicon glyphicon-remove-sign close-help" aria-hidden="true"
                              onclick="closeHelp();"></span>
    </div>
    <p class="col-xs-12 help-text">
        As of v3.1.0, we've deprecated .pull-right on dropdown menus. To right-align a menu, use
        .dropdown-menu-right. Right-aligned nav components in the navbar use a mixin version of this
        class to automatically align the menu. To override it, use .dropdown-menu-left.
    </p>
</div>-->

<span onclick="openHelp();" class="glyphicon glyphicon-info-sign open-help" aria-hidden="true" style="display: none;"></span>
</div>

</div>
<!-- #content-area -->
</div>
<!-- .container -->
</div>
<!-- #main-content -->

<span class="et_pb_scroll_top et-pb-icon et-hidden"></span>

<!--<footer id="main-footer" style="clear: both;">

    <div class="container">
        <div id="footer-widgets" class="clearfix">
            <div class="footer-widget">
                <div id="pages-2" class="fwidget et_pb_widget widget_pages"><h4 class="title">Quick Link</h4>
                    <ul>
                        <li class="page_item page-item-132"><a href="../../../about/">About</a></li>
                        <li class="page_item page-item-85 current_page_item"><a href="../../../about-us/">About
                                Us</a></li>
                        <li class="page_item page-item-120"><a href="../../../contact-us/">Contact us</a></li>
                        <li class="page_item page-item-41"><a href="../../../home/">Home</a></li>
                        <li class="page_item page-item-49"><a href="../../../">Pricing</a></li>
                    </ul>
                </div>
            </div>

            <div class="footer-widget">
                <div id="text-2" class="fwidget et_pb_widget widget_text"><h4 class="title">About Us</h4>

                    <div class="textwidget"><p style="text-align:justify;">We
                            offer no-questions-asked refunds to all customers within 30 days of
                            your purchase. If you are not satisfied with our product, then simply
                            send us an email and we will refund your purchase right away.</p></div>
                </div>
            </div>
            <div class="footer-widget">
                <div id="calendar-2" class="fwidget et_pb_widget widget_calendar">
                    <div id="calendar_wrap" class="calendar_wrap">
                        <table id="wp-calendar">
                            <caption>September 2016</caption>
                            <thead>
                            <tr>
                                <th scope="col" title="Sunday">S</th>
                                <th scope="col" title="Monday">M</th>
                                <th scope="col" title="Tuesday">T</th>
                                <th scope="col" title="Wednesday">W</th>
                                <th scope="col" title="Thursday">T</th>
                                <th scope="col" title="Friday">F</th>
                                <th scope="col" title="Saturday">S</th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <td colspan="3" id="prev"><a href="../../../2016/08/">« Aug</a></td>
                                <td class="pad">&nbsp;</td>
                                <td colspan="3" id="next" class="pad">&nbsp;</td>
                            </tr>
                            </tfoot>

                            <tbody>
                            <tr>
                                <td colspan="4" class="pad">&nbsp;</td>
                                <td>1</td>
                                <td id="today">2</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>5</td>
                                <td>6</td>
                                <td>7</td>
                                <td>8</td>
                                <td>9</td>
                                <td>10</td>
                            </tr>
                            <tr>
                                <td>11</td>
                                <td>12</td>
                                <td>13</td>
                                <td>14</td>
                                <td>15</td>
                                <td>16</td>
                                <td>17</td>
                            </tr>
                            <tr>
                                <td>18</td>
                                <td>19</td>
                                <td>20</td>
                                <td>21</td>
                                <td>22</td>
                                <td>23</td>
                                <td>24</td>
                            </tr>
                            <tr>
                                <td>25</td>
                                <td>26</td>
                                <td>27</td>
                                <td>28</td>
                                <td>29</td>
                                <td>30</td>
                                <td class="pad" colspan="1">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="footer-widget last">
                <div id="text-3" class="fwidget et_pb_widget widget_text"><h4 class="title">Contact Us</h4>

                    <div class="textwidget">Location : We offer no-questions-asked refunds to all.<br>
                        Phone :12345-67890.<br>
                        Email : Demo@gmail.com.
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="footer-bottom">
        <div class="container clearfix">
            <ul class="et-social-icons">

                <li class="et-social-icon et-social-facebook">
                    <a href="#" class="icon">
                        <span>Facebook</span>
                    </a>
                </li>
                <li class="et-social-icon et-social-twitter">
                    <a href="#" class="icon">
                        <span>Twitter</span>
                    </a>
                </li>
                <li class="et-social-icon et-social-google-plus">
                    <a href="#" class="icon">
                        <span>Google</span>
                    </a>
                </li>
                <li class="et-social-icon et-social-rss">
                    <a href="../../../feed/" class="icon">
                        <span>RSS</span>
                    </a>
                </li>

            </ul>
            <p id="footer-info">Designed by <a href="http://www.elegantthemes.com/"
                                               title="Premium WordPress Themes">Elegant Themes</a> | Powered by
                <a href="http://www.wordpress.org/">WordPress</a></p>
        </div>

    </div>
</footer>-->
<!-- #main-footer -->
</div>
<!-- #et-main-area -->


</div>
<!-- #page-container -->

<style type="text/css" id="et-builder-page-custom-style">
    .et_pb_section {
        background-color:;
    }
</style>
<div id="myModal" style="z-index:99999" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" id="modal_content">
            <div class="modal-header" id="modal_header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_title">Modal Header</h4>
            </div>
            <div class="modal-body" id="modal_body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer" id="modal_footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>