<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 8/14/2017
 * Time: 4:21 PM
 */
 include 'header.php';
?>

<div class="right_col" role="main">
    <div class="row tile_count">
    </div>

    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>All Messages <small></small></h2>
                <?php

                //                            echo $_SESSION['timezone'];
                ?>
                <!--<input type="text" class="form-control" value="" id="datetimepicker"/><br><br>-->
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <button class="btn btn-info btn-sm" onclick="desc_message()">
                            Add New Message
                        </button>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p class="text-muted font-13 m-b-30">
                    View the Details of All Messages
                </p>
                <table id="datatable-buttons1" class="table table-striped table-bordered">

                    <tbody id="tbl_body">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<?php
include 'footer.php';
?>
<script>
    window.onload = function () {
        get_all_messages();
        $('#datatable-buttons1').dataTable({});
        $('.messageStatus').bootstrapToggle();
        $('#datatable-buttons1').on('draw.dt', function() {
            $('.messageStatus').bootstrapToggle();
            console.log('table drawn');
        });
    };

</script>
