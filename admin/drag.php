<?php
include('header.php');
?>
<style>
    #sortable tr td {
        color: white;
    }
</style>
<div class="clearfix"></div>
<table class="table table-bordered">
    <thead>
        <tr><th>Sorting Elements</th></tr>
    </thead>
    <tbody id="sortable">
        <tr>
            <td>Item 1</td>
        </tr>
        <tr>
            <td>Item 2</td>
        </tr>
        <tr>
            <td>Item 3</td>
        </tr>
        <tr>
            <td>Item 4</td>
        </tr>
        <tr>
            <td>Item 5</td>
        </tr>
    </tbody>
</table>
<?php
include('footer.php');
?>
<script>
    $( function() {
        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();
    });
</script>
