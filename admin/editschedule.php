<?php
$sch_id = '';
if(isset($_REQUEST['_id'])){
    $sch_id = trim($_REQUEST['_id']);
    echo "<input type='hidden' value='".$sch_id."' id='sch_id' />";
}else{
    header("Location:schedule");
}
include('header.php');
include('api/Classes/CONNECT.php');
include('api/Classes/VIDEO.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
$conn = new \Classes\CONNECT();
$video = new VIDEO();
?>

<style>
    #overlay{
        position:fixed;
        z-index:99999;
        top:0;
        left:0;
        bottom:0;
        right:0;
        background:rgba(0,0,0,0.6);
        transition: 1s 0.4s;
    }
    #progress{
        height:1px;
        background:#fff;
        position:absolute;
        width:0;
        top:50%;
    }
    #progstat{
        font-size:13px;
        letter-spacing: 2px;
        position:absolute;
        top:50%;
        margin-top:-40px;
        width:100%;
        text-align:center;
        color:#fff;
    }
</style>

<div id="overlay" style="display: none">
    <div id="progstat">....Please Wait....<br>Loading</div>
    <div id="progress"></div>
</div>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="row tile_count">
            <!--<a href="users">
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
                    <div class="count" id="userCount"></div>
                    <span class="count_bottom"><i class="green">Click </i>to Expand</span>
                </div>
            </a>-->
            <!--<a href="books"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-volume-up"></i> Total Audio Books</span>
                    <div class="count" id="booksCount"></div>
                    <span class="count_bottom"><i class="green"></i> in All Categories</span>
                </div></a>
            <a href="index"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-book"></i> Total Categories</span>
                    <div class="count green" id="catCount"></div>
                    <span class="count_bottom"><i class="green"></i> Click to Expand</span>
                </div></a>
            <a href="membership"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-users"></i> MemberShip Types</span>
                    <div class="count" id="membershipCount"></div>
                    <span class="count_bottom"> Click to View</span>
                </div></a>
            <a href="discount"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-cc-discover"></i> Discount Coupons</span>
                    <div class="count" id="allCoupon"></div>
                    <span class="count_bottom"><i class="green" id="activeCoupon"></i> is Still Active</span>
                </div></a>
            <a href="orders"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-first-order"></i> Orders</span>
                    <div class="count" id="orderCount"></div>
                    <span class="count_bottom"><i class="green"></i>Click to Expand</span>
                </div></a>-->
        </div>
        <div class="">
            <div class="row">
                <?php
                $sch_resp = $video->getParticularScheduleData($sch_id);
                $schedule_data = $sch_resp['schedule_data'];

                ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Schedule
                                <small></small>
                            </h2>
                            <!--<input type="text" class="form-control" value="" id="datetimepicker"/><br><br>-->
                            <ul class="nav navbar-right panel_toolbox"></ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                <label class="label label-danger">Please Fill all the Required Fields</label>
                            </p>
                            <div class="row">
                                <div class="col-md-7 border-right">
                                    <div class="form-group col-md-6">
                                        <label>Schedule Name </label>
                                        <input type="text" class="form-control" id="schedule_name" value="<?php echo $schedule_data['schedule_name']?>"
                                        placeholder="Enter Schedule Name"/>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Schedule Start Time</label>
                                        <input type="text" class="form-control"  id="datetimepicker"
                                        value="<?php echo date("d M Y h:i:s A",$schedule_data['schedule_start_time'])?>" placeholder="Select Schedule Start Time" onchange="onTimeChange()"/>
                                    </div>
                                    <input type="hidden" disabled value="<?php echo date("d M Y h:i:s A",$schedule_data['schedule_end_time'])?>" id="schedule_end_time" />
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-6">
                                        <label>Schedule Priority </label>
                                        <select class="form-control" id="priority" onchange="getTime()">
                                            <option value="na">--Select Priority--</option>
                                            <?php
                                             if($schedule_data['schedule_priority'] =='normal'){
                                                 ?>
                                                 <option value="normal" selected="selected">Normal</option>
                                                 <option value="high">High</option>
                                                 <?php
                                             }
                                             else{
                                                 ?>
                                                 <option value="normal" >Normal</option>
                                                 <option value="high" selected="selected">High</option>
                                                 <?php
                                             }
                                            ?>

                                        </select>
                                    </div>
                                    <!--<div class="form-group col-md-6">
                                        <label>Schedule Description </label>
                                        <textarea class="form-control" id="schedule_desc" placeholder="Enter Description Here"><?php /*echo $schedule_data['schedule_desc']*/?></textarea>
                                    </div>-->
                                    <div class="clearfix"></div>
                                    <p class="paraheading">List of Selected Videos<span onclick="clearList()" class="clearlist">Clear List</span></p>
                                    <hr/>
                                    <div style="height: 300px;overflow: auto">
                                        <table class="table table-bordered" >
                                            <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Video Name</th>
                                                <th>Video Duration</th>
                                                <th>Video Started at</th>
                                                <th>Video Ends at</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody id="selectiontablebody"></tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-5" style="height:527px;overflow:auto">
                                    <div class="folders folders-1 active">
                                        <table class="table table-striped" id="table1">

                                        </table>
                                    </div>
                                    <div class="folders folders-2">
                                        <label class="label label-danger" style="line-height:20px">Click on Select Button to Select Video for
                                            Schedule</label>
                                        <hr/>
                                        <h2 style="cursor:pointer" onclick="onBack();"><i class="fa fa-arrow-circle-left"></i> Back</h2>

                                        <table class="table table-striped" id="table2">

                                        </table>
                                    </div>
                                    <div class="folders folders-3">
                                        <label class="label label-danger" style="line-height:20px">Click on Select Button to Select Video for
                                            Schedule</label>
                                        <hr/>
                                        <h2 style="cursor:pointer" onclick="onBack();"><i class="fa fa-arrow-circle-left"></i> Back</h2>

                                        <table class="table table-striped" id="table3">

                                        </table>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-12 text-right" style="margin-top:20px">
                                <input type="button" class="btn btn-default" value="Cancel"/>
                                <input type="button" class="btn btn-Info" onclick="updateSchedule('<?php echo $sch_id;?>')"
                                       value="Update Schedule"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
<?php
include('footer.php');
?>
<script>
    function getData(){
        var sch_id = $("#sch_id").val();
        if(sch_id != "") {
            showSelection(sch_id);
//            startSelection = true;
        }
    }
    getData();
    $("#datetimepicker").attr("disabled",false);
</script>
<script type="text/javascript" src="js/schedule.js"></script>