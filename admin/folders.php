<?php
include('header.php');
include('api/Classes/CONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
$conn = new \Classes\CONNECT();
?>
<!-- page content -->
<div class="right_col" role="main">

    <!-- first level loaded here----- !-->

    <div class="folders folders-1 active">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Folders <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <label class="percentprog"></label>
                            </li>
                            <li><label id="messagee" style="color: red;margin-right: 30px;margin-top: 4%;"></label></li>
                            <li><div class="form-inline">
                                    <input class="form-control btn-sm" type="text" id="folder_name"  placeholder="Please enter folder name"/>
                                    <button onclick="createFolder()" id="uploadbtn" class="btn btn-info">
                                        + Add
                                    </button>
                                </div>
                            </li>
                            <!--<li>
                                <label>Add New Videos</label>
                                <div class="form-inline">
                                <input class="form-control btn-sm" type="file" id="video_file" name="files[]" multiple="" onchange="videoOperations(this)" />
                                <button onclick="uploadData()" id="uploadbtn" class="btn btn-info">
                                    Upload
                                </button>
                                </div>
                            </li>-->

                        </ul>
                        <div class="clearfix"></div>

                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            View the Details of All Folders
                        </p>
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Folder Name</th>
                                <th>Folder Path</th>
                                <th>Added On</th>
                                <!--<th>Video Duration</th>-->
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $link = $conn->connect();
                            if ($link) {
                                $query = "select * from folders where folder_parent_id='0' order by folder_id DESC";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($folderData = mysqli_fetch_array($result)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <td data-title='Folder Name' style="width:20%">
                                                    <?php
                                                        $folder_name = $folderData['folder_name'];
                                                        $folderPath = $folderData['folder_path'];

                                                        $folderPath = substr($folderPath,strpos($folderPath,'video'),strlen($folderPath));
                                                        echo "<span style='cursor: pointer' onclick=onLevelLoaded('2','".$folderData['folder_id']."','".urlencode($folderData['folder_path'])."');loadFolderData()><i class='fa fa-folder'></i> ".$folder_name."</span>";
                                                    ?>
                                                </td>
                                                <td data-title='Folder Path'><?php echo $folderPath; ?></td>
                                                <td data-title='Added On'><?php echo date("d-M-Y h:i:s A",$folderData['folder_created_at']) ?></td>
                                                <td data-title='Action'>
                                                   <i class='fa fa-trash' onclick=deleteFolder('<?php echo $folderData['folder_id']; ?>','<?php echo urlencode($folderData['folder_path']); ?>') style='color:#D05E61;cursor: pointer'></i>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- second level loaded here----- !-->

    <div class="folders folders-2">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <ul class="nav navbar-left panel_toolbox">
                            <li><h2 style="cursor:pointer" onclick="onLevelLoaded(1,0);"><i class="fa fa-arrow-circle-left"></i> </h2> </li>
                            <li><h2>&nbsp;All Folders <small></small></h2></li>
                        </ul>

                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <label class="percentprog"></label>
                            </li>
                            <li><label id="messagee" style="color: red;margin-right: 30px;margin-top: 4%;"></label></li>
                            <li>

                                <div class="form-inline" style="margin-right:5px">
                                    <input class="form-control btn-sm" type="file" id="video_file" name="files[]" multiple="" onchange="videoOperations(this)" />
                                    <button onclick="uploadData()" id="uploadbtn" class="btn btn-info">
                                        <i class="fa fa-video-camera"></i>  Upload
                                    </button>
                                </div>
                            </li>

                            <li><div class="form-inline">
                                    <button onclick="addFolder()" id="uploadbtn" class="btn btn-info">
                                        + Add Folder
                                    </button>
                                </div>
                            </li>
                        </ul>
                        <div class="clearfix"></div>

                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            View the Details of All Folders
                        </p>
                        <table id="datatable-buttons1" class="table table-striped table-bordered">

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- third level loaded here----- !-->

    <div class="folders folders-3">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <ul class="nav navbar-left panel_toolbox">
                            <li><h2 style="cursor:pointer" onclick="onLevelLoaded(2,0);"><i class="fa fa-arrow-circle-left"></i> </h2> </i></li>
                            <li><h2>&nbsp;All Videos <small></small></h2></li>
                        </ul>

                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <label class="percentprog" id="percentprog1"></label>
                            </li>
                            <li><label id="messagee" style="color: red;margin-right: 30px;margin-top: 4%;"></label></li>
                            <li>

                                <div class="form-inline" style="margin-right:5px">
                                    <input class="form-control btn-sm" type="file" id="video_file1" name="files[]" multiple="" onchange="videoOperations(this)" />
                                    <button onclick="uploadData()" id="uploadbtn1" class="btn btn-info">
                                        <i class="fa fa-video-camera"></i>  Upload
                                    </button>
                                </div>
                            </li>
                        </ul>
                        <div class="clearfix"></div>

                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            View the Details of All Videos
                        </p>
                        <table id="datatable-buttons2" class="table table-striped table-bordered">

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- /page content -->
<?php
include('footer.php');
?>

<script type="text/javascript" src="js/schedule.js"></script>
