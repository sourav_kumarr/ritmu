<!-- footer content -->
<footer>
    <div class="pull-right">
        Ritmu - Developed by <a href="http://sachtechsolution.com"><b>SachTech Solution Pvt. Ltd.</b></a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>
<div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="https://www.gstatic.com/firebasejs/4.1.5/firebase.js"></script>

<script type="text/javascript" src="js/db.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/bootstrap-toggle.min.js"></script>
<script src="js/jquery.datetimepicker.full.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/myscript.js"></script>

</body>
</html>