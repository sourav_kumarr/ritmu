<?php
session_start();
if(isset($_SESSION['timezone']))
    date_default_timezone_set($_SESSION['timezone']);

if(!(isset($_SESSION['RITAdminId']) && isset($_SESSION['RITAdminName']))){
    ?>
    <script>
        window.location = "login.php";
    </script>
    <?php
}
else{
    echo "<input type='hidden' id='user_id' value='".$_SESSION['RITAdminId']."'>";
}
?>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ritmu | Admin </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/jquery.datetimepicker.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>


    <style>
        .logo_img{
            height: 50px;
            width: 50px;
        }
    </style>
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index" class="site_title"><img class="logo_img" src="images/logo.png"> <span>Ritmu</span></a>
                </div>
                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="images/img.png" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome</span>
                        <h2>Administrator</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>Admin</h3>
                        <ul class="nav side-menu">
                            <!--<li><a href="index" id="index"><i class="fa fa-book"></i> Categories</a></li>
                            <li><a href="books" id="books"><i class="fa fa-volume-up"></i> Audio Books</a></li>-->
                            <!--<li><a href="users" id="users"><i class="fa fa-user"></i> Users</a></li>-->
                            <li><a href="folders.php" id="folders"><i class="fa fa-folder"></i> Folders </a></li>
                            <!--                            <li><a href="videos" id="videos"><i class="fa fa-file-video-o"></i> Videos </a></li>-->
                            <li><a href="schedule.php" id="schedule"><i class="fa fa-calendar"></i> Schedule </a></li>
                            <li ><a href="description.php" id="description"><i class="fa fa-cc-discover"></i> Description</a></li>
                            <!--<li><a href="orders" id="orders"><i class="fa fa-first-order"></i>Order Management</a></li>-->
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->
            </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="images/img.png" alt="">Admin
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                <li><a onclick=changeAdminPassword('<?php echo $_SESSION['RITAdminId'] ?>')><i class="fa fa-lock pull-right"></i> Change Password</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->