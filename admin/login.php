<?php
session_start();
?>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ritmu | Admin Login</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/loginStyle.css" rel="stylesheet">
</head>

<?php

if(isset($_REQUEST["_"])){
    if($_REQUEST["_"] == "cpd"){
        $email = $_SESSION['capd'];
        $capdd =$_SESSION['capd'];
        $tokn = $_REQUEST['tokn'];
        echo "<input type='hidden' value='cpd' id='cpd' />
            <input type='hidden' value='$email' id='email' />
            <input type='hidden' value='$tokn' id='tokn' />";
    }
}
?>
<body class="login">
<div class="loader">
    <img src="images/default.gif" class="loaderImg" />
</div>
<div class="main">
    <div class="user">
        <img src="images/user2.png" alt="">
    </div>
    <div class="login">
        <div class="inset">
            <!-----start-main---->
            <form>
                <div>
                    <span><label>Username</label></span>
                    <span><input type="text" class="textbox" id="username"></span>
                </div>
                <div>
                    <span><label>Password</label></span>
                    <span><input type="password" class="password" id="password"></span>
                </div>
                <div class="sign">
                    <div class="submit">
                        <a onclick="loginNow()"> LOGIN  AS ADMIN</a>
                    </div>
                    <span class="forget-pass">
							<a onclick="forgotPassword()">Forgot Password?</a>
						</span>
                    <div class="clear"> </div>
                </div>
            </form>
        </div>
    </div>
    <!-----//end-main---->
</div>
<!-----start-copyright---->
<div class="copy-right">
    <p>©2017 All Rights Reserved. <b><u>Ritmu</u></b>, <a href="#">Privacy and Terms</a></p>
</div>
<!-----//end-copyright---->
<div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script src="js/jquery.min.js" ></script>
<script type="text/javascript" src="js/timezone.js"></script>
<script src="js/bootstrap.min.js" ></script>
<script src="js/myscript.js" ></script>
<script>
    var tz = jstz.determine(); // Determines the time zone of the browser client
    var timezone = tz.name();
    var url = 'api/userProcess.php';
    $.post(url,{'type':'updateTimeZone','timezone':timezone},function(data){
        console.log('response -- '+JSON.stringify(data));
    });

</script>
<script>
    function loginNow(){
        var userName = $("#username").val();
        var Password = $("#password").val();
        if(validateEmail(userName))
        {
            var url = "api/admin_login.php";
            $.post(url, {"type": "admin_login","email":userName,"password":Password}, function (data) {
                var status = data.Status;
                if (status == "Success") {
                    showMessage("Login Success !!! Please Wait.....", 'green');
                    setTimeout(function(){
                        window.location="folders.php";
                    },1500);
                }
                else {
                    showMessage(data.Message, 'red');
                }
            }).fail(function(){
                showMessage("Server Error !!! Please Try After Some Time....", 'red');
            });
        }
        else{
            showMessage("Invalid Email Address Entered",'red');
        }
    }
    function forgotPassword(){
        $(".loader").show();
        var url = "api/admin_login.php";
        $.post(url, {"type": "forgotPassword"}, function (data) {
            var status = data.Status;
            var message = data.Message;
            $(".loader").hide();
            if (status == "Success") {
                showMessage(message, 'green');
            }
            else {
                showMessage(message, 'red');
            }
        }).fail(function(){
            $(".loader").hide();
            showMessage("Server Error !!! Please Try After Some Time....", 'red');
        });
    }
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    function showCPDForm(){
        $(".modal-header").css({"display":"block"});
        $(".modal-title").html('Forgot Password');
        $(".modal-body").html("<div class='row'><div " +
        "class='col-md-3'></div><div class='col-md-6'><div class='form-group'><label>" +
        "New Password</label><input type='password' class='form-control' id='newPassword' /></div><div class='form-group'><label>" +
        "Confirm Password</label><input type='password' class='form-control' id='confirmNewPassword' /></div><input type='button' " +
        "class='btn btn-info pull-right' data-dismiss='modal' onclick=changeForgotPassword() value='Change Password'/>" +
        "</div></div><div class='col-md-3'></div>");
        $(".modal-footer").css({"display":"none"});
        $("#myModal").modal("show");
    }
    function changeForgotPassword() {
        var url = "api/admin_login.php";
        var newPassword = $("#newPassword").val();
        var confirmNewPassword = $("#confirmNewPassword").val();
        var email = $("#email").val();
        var tokn = $("#tokn").val();
        if (newPassword == confirmNewPassword) {
            setTimeout(function () {
                $.post(url, {
                    "type": "changeForgotPassword",
                    "new_password": newPassword,
                    "email": email,
                    "tokn": tokn
                }, function (data) {
                    var status = data.Status;
                    if (status == "Success") {
                        showMessage(data.Message, "green");
                    }
                    else {
                        showMessage(data.Message, "red");
                    }
                }).fail(function () {
                    showMessage("Server Error!!! Please Try After Some Time", "red")
                });
            }, 1000);
        }
        else {
            showMessage("New Password and Confirm Password Should Be Same", "red");
        }
    }

    function showCPDForm2() {
        if(document.getElementById('cpd').value == "cpd"){
            showCPDForm();
        }
    }
    showCPDForm2();
</script>
