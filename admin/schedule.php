<?php
include('header.php');
include('api/Classes/CONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
$conn = new \Classes\CONNECT();


?>

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="row tile_count">
         </div>
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>All Schedule <small></small></h2>
                            <?php

//                            echo $_SESSION['timezone'];
                            ?>
                            <!--<input type="text" class="form-control" value="" id="datetimepicker"/><br><br>-->
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <button onclick="addSchedule()" class="btn btn-info btn-sm">
                                        Add New Schedule
                                    </button>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                View the Details of All Schedules
                            </p>
                            <table id="datatable-buttons" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Schedule Name</th>
                                    <th>Added On</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
<!--                                    <th>Description</th>-->
                                    <th>Priority</th>
                                    <th>Schedule Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $link = $conn->connect();
                                if ($link) {

                                    $query = "select * from schedule order by sch_id DESC";
                                    $result = mysqli_query($link, $query);
                                    if ($result) {
                                        $num = mysqli_num_rows($result);
                                        if ($num > 0) {
                                            $j = 0;
                                            while ($schData = mysqli_fetch_array($result)) {
                                                $j++;
                                                ?>
                                                <tr>
                                                    <td data-title='#'><?php echo $j ?></td>
                                                    <td data-title='Schedule Name'>
                                                        <!--<a href=' '--><!--udet?_=--><?php /*echo $userData['user_id'] */?>
                                                        <?php echo $schData['sch_name'] ?><!--</a>-->
                                                    </td>
                                                    <td data-title='Added On'><?php echo date("d-M-Y",$schData['sch_date']) ?></td>
                                                    <td data-title='Start Time'><?php echo date("d-M-Y h:i:s A",$schData['sch_start_time']) ?></td>
                                                    <td data-title='End Time'><?php echo date("d-M-Y h:i:s A",$schData['sch_end_time'])?> </td>
<!--                                                    <td data-title='Description' title="--><?php //echo $schData['sch_desc'] ?><!--">--><?php //echo substr($schData['sch_desc'],0,25)."..."?><!--</td>-->
                                                    <td data-title='Priority' title="<?php echo $schData['sch_priority']; ?>"><?php echo $schData['sch_priority'];?></td>
                                                    <td class='buttonsTd' data-title='Schedule Status'>
                                                        <?php
                                                        $checked = "";
                                                        if ($schData['sch_status'] == "1") {
                                                            $checked  = "checked";
                                                        }
                                                        ?>
                                                        <input data-onstyle='info' class='scheduleStatus' <?php echo $checked ?>
                                                        data-toggle='toggle' value="<?php echo $schData['sch_status'] ?>"
                                                        id="<?php echo $schData['sch_id']?>" data-size='mini' data-style='ios'
                                                        type='checkbox' />
                                                    </td>
                                                    <td data-title='Action'>
                                                        <i class='fa fa-edit' onclick=editSchedule('<?php echo $schData['sch_id'] ?>') style='color:#D05E61;cursor: pointer;padding-right:12px'></i>
                                                        <i class='fa fa-trash' onclick=delete_sch('<?php echo $schData['sch_id'] ?>') style='color:#D05E61;cursor: pointer;'></i>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
<?php
include('footer.php');
?>