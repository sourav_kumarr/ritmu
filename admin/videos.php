<?php
include('header.php');
include('api/Classes/CONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
$conn = new \Classes\CONNECT();
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
        <!--<a href="users"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
                <div class="count" id="userCount"></div>
                <span class="count_bottom"><i class="green">Click </i>to Expand</span>
            </div></a>-->
        <!--<a href="books"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-volume-up"></i> Total Audio Books</span>
                <div class="count" id="booksCount"></div>
                <span class="count_bottom"><i class="green"></i> in All Categories</span>
            </div></a>
        <a href="index"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-book"></i> Total Categories</span>
                <div class="count green" id="catCount"></div>
                <span class="count_bottom"><i class="green"></i> Click to Expand</span>
            </div></a>
        <a href="membership"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-users"></i> MemberShip Types</span>
                <div class="count" id="membershipCount"></div>
                <span class="count_bottom"> Click to View</span>
            </div></a>
        <a href="discount"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cc-discover"></i> Discount Coupons</span>
                <div class="count" id="allCoupon"></div>
                <span class="count_bottom"><i class="green" id="activeCoupon"></i> is Still Active</span>
            </div></a>
        <a href="orders"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-first-order"></i> Orders</span>
                <div class="count" id="orderCount"></div>
                <span class="count_bottom"><i class="green"></i>Click to Expand</span>
            </div></a>-->
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Videos <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <label class="percentprog"></label>
                            </li>
                            <li>
                                <label>Add New Videos</label>
                                <div class="form-inline">
                                <input class="form-control btn-sm" type="file" id="video_file" name="files[]" multiple="" onchange="videoOperations(this)" />
                                <button onclick="uploadData()" id="uploadbtn" class="btn btn-info">
                                    Upload
                                </button>
                                </div>
                            </li>

                        </ul>
                        <div class="clearfix"></div>

                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            View the Details of All Videos
                        </p>
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Thumb</th>
                                <th>Video Name</th>
                                <th>Added On</th>
                                <!--<th>Video Duration</th>-->
                                <th>Video Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $link = $conn->connect();
                            if ($link) {
                                $query = "select * from videos order by video_id DESC";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($videoData = mysqli_fetch_array($result)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <td data-title='Thumb' style="text-align: center">
                                                    <i class='fa fa-video-camera fa-2x' style="color:#31B0D5" ></i>
                                                </td>
                                                <td data-title='Video Name' style="width:20%">
                                                    <?php
                                                        $video_name = explode(";",$videoData['video_name'])[1];
                                                        echo explode(".mp4",$video_name)[0];
                                                    ?>
                                                </td>
                                                <td data-title='Added On'><?php echo date("d-M-Y h:i:s A",$videoData['added_on']) ?></td>
                                               <!-- <?php /*$mins = $videoData['video_duration']/60;
                                                $secs = $videoData['video_duration']%60;
                                                $mins = (int)$mins;
                                                $secs = (int)$secs;
                                                if($mins<10){
                                                    $mins = "0".$mins;
                                                }
                                                if($secs<10){
                                                    $secs = "0".$secs;
                                                }
                                                */?>
                                                <td data-title='Video Duration'><?php /*echo $mins.":".$secs." Seconds" */?></td>-->
                                                <td class='buttonsTd' data-title='Video Status'>
                                                    <?php
                                                    $checked = "";
                                                    if ($videoData['video_status'] == "1") {
                                                        $checked  = "checked";
                                                    }
                                                    ?>
                                                    <input data-onstyle='info' class='videoStatus' <?php echo $checked ?>
                                                    data-toggle='toggle' value="<?php echo $videoData['video_status'] ?>"
                                                    id="<?php echo $videoData['video_id']?>" data-size='mini' data-style='ios'
                                                    type='checkbox' />
                                                </td>
                                                <td data-title='Action'>
                                                   <i class='fa fa-edit' onclick=editVideo('<?php echo $videoData['video_id'] ?>','<?php echo urlencode($videoData['video_name']) ?>') style='color:#D05E61;cursor: pointer'></i>
                                                   <i class='fa fa-trash' onclick=deleteVideo('<?php echo $videoData['video_id'] ?>') style='color:#D05E61;cursor: pointer'></i>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>