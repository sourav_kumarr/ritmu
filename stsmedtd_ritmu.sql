-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2017 at 03:56 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stsmedtd_ritmu`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `admin_id` int(100) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_password` varchar(500) NOT NULL,
  `admin_token` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`admin_id`, `admin_name`, `admin_email`, `admin_password`, `admin_token`) VALUES
(1, 'Admin', 'sbsunilbhatia9@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'edd88f948868a7de531a5ca81aab6cbf99d881cf8c7b7894be410dfadde5ec');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `sch_id` int(100) NOT NULL,
  `sch_name` varchar(100) NOT NULL,
  `sch_start_time` varchar(100) NOT NULL,
  `sch_end_time` varchar(100) NOT NULL,
  `sch_status` varchar(100) NOT NULL,
  `sch_desc` text NOT NULL,
  `sch_date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`sch_id`, `sch_name`, `sch_start_time`, `sch_end_time`, `sch_status`, `sch_desc`, `sch_date`) VALUES
(8, 'sourav', '1500385255', '1500385646', '1', 'asdfgfhj', '1500328800');

-- --------------------------------------------------------

--
-- Table structure for table `sch_detail`
--

CREATE TABLE `sch_detail` (
  `detail_id` int(100) NOT NULL,
  `video_id` varchar(100) NOT NULL,
  `start_time` varchar(100) NOT NULL,
  `end_time` varchar(100) NOT NULL,
  `sch_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sch_detail`
--

INSERT INTO `sch_detail` (`detail_id`, `video_id`, `start_time`, `end_time`, `sch_id`) VALUES
(42, '137', '1500373516', '1500373579', 'Guest917078'),
(43, '138', '1500289316', '1500289439', 'Guest917078'),
(55, '138', '1500385255', '1500385378', '8'),
(56, '139', '1500385378', '1500385501', '8'),
(57, '134', '1500385501', '1500385604', '8'),
(58, '135', '1500385604', '1500385646', '8');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_contact` varchar(100) NOT NULL,
  `active_plan` varchar(100) NOT NULL,
  `activation_date` varchar(100) NOT NULL,
  `plan_expiry_date` varchar(100) NOT NULL,
  `user_status` enum('0','1') NOT NULL,
  `user_token` text NOT NULL,
  `user_address` text NOT NULL,
  `register_source` varchar(100) NOT NULL,
  `renewal_type` varchar(100) NOT NULL,
  `auto_renewal` varchar(100) NOT NULL,
  `email_verified` enum('Yes','No') NOT NULL,
  `user_profile` varchar(500) NOT NULL,
  `subscribed` varchar(100) NOT NULL,
  `subscribed_for` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_contact`, `active_plan`, `activation_date`, `plan_expiry_date`, `user_status`, `user_token`, `user_address`, `register_source`, `renewal_type`, `auto_renewal`, `email_verified`, `user_profile`, `subscribed`, `subscribed_for`) VALUES
(19, 'Sunil Bhatia', 'sbsunilbhatia9@gmail.com', '8872292478', '6', '1498203568', '1529653168', '1', '05e328d0616037c76a7f329cadcaf50c071e2be3422792fd0bc1f4d62eb80f', 'majitha', 'Facebook', 'Yearly', 'Yes', 'No', 'images url', 'Yes', '1501055124'),
(20, 'SachTech Solution', 'sachtech4016@gmail.com', 'dasddas', '8', '1498483664', '1500989264', '1', '9295da85b9a05b0e67013aa26964412f7a763f415602edc2bae4095071b94e', 'dads', 'Google', 'Monthly', '0', 'No', 'Google', 'Yes', ''),
(21, 'SachTech Solution', 'sachtech40126@gmail.com', '7874545456', '5', '1498542488', '1501048088', '1', 'ffb85c8470fff07a291ea33dc9d1216b09f537d0c7258056ae4ee88c8bfa2c', 'sad', 'Google', 'Monthly', '1', 'No', 'Google', 'No', '');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `video_id` int(11) NOT NULL,
  `video_name` text NOT NULL,
  `video_file` text NOT NULL,
  `added_on` varchar(100) NOT NULL,
  `video_status` varchar(100) NOT NULL,
  `video_duration` varchar(100) NOT NULL,
  `video_thumb` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`video_id`, `video_name`, `video_file`, `added_on`, `video_status`, `video_duration`, `video_thumb`) VALUES
(128, '427429;Ki Banu Duniya Da - Gurdas Maan feat. Diljit Dosanjh & Jatinder Shah - Coke Studio @ MTV Season 4.mp4', '427429;Ki Banu Duniya Da - Gurdas Maan feat. Diljit Dosanjh & Jatinder Shah - Coke Studio @ MTV Season 4.mp4', '1500199038', '1', '207', ''),
(129, '872772;Bholeynath_-_Millind_Gaba.mp4', '872772;Bholeynath_-_Millind_Gaba.mp4', '1500199038', '1', '207', ''),
(130, '24536;PATAKE (Full Video) __ SUNANDA SHARMA __ Latest Punjabi Songs 2016 __ AMAR A.mp4', '24536;PATAKE (Full Video) __ SUNANDA SHARMA __ Latest Punjabi Songs 2016 __ AMAR A.mp4', '1500199038', '1', '207', ''),
(131, '988677;Makhna (à¨®à©±à¨–à¨£à¨¾)- Gurdas Maan Jatinder Shah R.Swami Punjab The Album - New Punjabi Song Saga Music.mp4', '988677;Makhna (à¨®à©±à¨–à¨£à¨¾)- Gurdas Maan Jatinder Shah R.Swami Punjab The Album - New Punjabi Song Saga Music.mp4', '1500199038', '1', '457', ''),
(132, '64514;Gurdas Maan - PUNJAB - Jatinder Shah - Gurickk G Maan - New Punjabi Songs 2017 - Saga Music.mp4', '64514;Gurdas Maan - PUNJAB - Jatinder Shah - Gurickk G Maan - New Punjabi Songs 2017 - Saga Music.mp4', '1500199038', '1', '482', ''),
(133, '627532;akshay.mp4', '627532;akshay.mp4', '1500211959', '1', '180', ''),
(134, '355957;dabang3.mp4', '355957;dabang3.mp4', '1500211959', '1', '103', ''),
(135, '926055;jio.mp4', '926055;jio.mp4', '1500211959', '1', '42', ''),
(136, '801208;krish.mp4', '801208;krish.mp4', '1500211959', '1', '134', ''),
(137, '239715;parmish.mp4', '239715;parmish.mp4', '1500211959', '1', '63', ''),
(138, '146362;robot.mp4', '146362;robot.mp4', '1500211959', '1', '123', ''),
(139, '892730;runout2.mp4', '892730;runout.mp4', '1500211959', '1', '123', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`sch_id`);

--
-- Indexes for table `sch_detail`
--
ALTER TABLE `sch_detail`
  ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`video_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `admin_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `sch_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sch_detail`
--
ALTER TABLE `sch_detail`
  MODIFY `detail_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
